package com.bcx.wind.workflow.test.parser;

import com.bcx.wind.workflow.core.flow.node.ProcessModel;
import com.bcx.wind.workflow.errorcontext.ProcessBuilderError;
import com.bcx.wind.workflow.errorcontext.WindLocalError;
import com.bcx.wind.workflow.parser.ProcessBuilder;
import com.bcx.wind.workflow.parser.ProcessBuilderFactory;
import com.bcx.wind.workflow.support.ResourceHelper;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * 流程定义对象构建测试
 *
 * @author zhanglei
 */
public class ProcessBuilderTest extends BaseTest {

    @Test
    public void parserTest() throws IOException {
        engine().managerService().language("zh");
        ProcessBuilder builder = null;
        try {
            InputStream stream = ResourceHelper.getResourceAsStream("process/Context.xml");
            builder = ProcessBuilderFactory.getBuilder(stream);
            builder.buildProcess("12345");
            ProcessModel model = builder.getProcessModel();
            System.out.println(model);
        }catch (Exception e){
            String errorMsg = WindLocalError.get().errorMsg();
            System.out.println(errorMsg);
        }
    }


    @Test
    public void parserTest1() throws IOException {
        try {
            InputStream stream = ResourceHelper.getResourceAsStream("process/Context2.xml");
            ProcessBuilder builder = ProcessBuilderFactory.getBuilder(stream);
            builder.buildProcess("12345");
            ProcessModel model = builder.getProcessModel();
            System.out.println(model);
        }catch (Exception e){
            String errorMsg = WindLocalError.get().errorMsg();
            System.out.println(errorMsg);
        }
    }

}
