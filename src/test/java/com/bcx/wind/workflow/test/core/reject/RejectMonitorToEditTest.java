package com.bcx.wind.workflow.test.core.reject;

import com.bcx.wind.workflow.pojo.Wind;
import com.bcx.wind.workflow.pojo.WindUser;
import com.bcx.wind.workflow.pojo.variable.RejectVariable;
import com.bcx.wind.workflow.test.core.submit.SubmitBase;
import org.junit.Test;

public class RejectMonitorToEditTest extends SubmitBase {

    @Test
    public void rejectTest(){
        //首先提交到monitor
        monitor();
        //退回到eidt节点
        Wind wind = reject(new WindUser().setUserId("2004").setUserName("张无忌"));
        assert wind !=null;
        Wind wind1 = reject(new WindUser().setUserId("2005").setUserName("赵敏"));
        assert wind1 !=null;
        Wind wind2 = reject(new WindUser().setUserId("2006").setUserName("周芷若"));
        assert wind2 !=null;
        commit();
    }


    private Wind reject(WindUser user){
        RejectVariable rejectVariable = new RejectVariable()
                .setUser(user)
                .setOrderId(orderId)
                .setReturnNode("edit")
                .setSuggest("退回到起草节点");
        return engine.windCoreService().reject(rejectVariable);
    }

}
