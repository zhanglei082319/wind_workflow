package com.bcx.wind.workflow.test.impl.cmd.managercmd;

import com.bcx.wind.workflow.access.TaskFilter;
import com.bcx.wind.workflow.access.WindPage;
import com.bcx.wind.workflow.entity.WindTaskInstance;
import com.bcx.wind.workflow.pojo.ProcessModule;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Test;

import java.util.List;

public class ManagerCmdTest extends BaseTest {

    @Test
    public void queryToDoProcessCommand(){

        ProcessModule processModule = engine().managerService().queryToDoProcess("2009");
        System.out.println(processModule);
        commit();
    }


    @Test
    public void queryTaskInstanceTest(){
        TaskFilter filter = new TaskFilter()
                .setProcessName("holiday");
        WindPage page = new WindPage();
        page.setPageSize(10);
        List<WindTaskInstance> taskInstanceList = engine().managerService().queryTaskInstance(filter,page);
        System.out.println(taskInstanceList.size());
    }


}
