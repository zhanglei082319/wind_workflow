package com.bcx.wind.workflow.test.impl.cmd.repositorycmd.processmodulecmd;

import com.bcx.wind.workflow.entity.WindProcessModule;
import com.bcx.wind.workflow.pojo.ProcessModule;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Test;

import static com.bcx.wind.workflow.core.constant.Constant.ROOT_MODULE;

public class ProcessModuleCmdTest extends BaseTest {


    private WindProcessModule insert(){
        WindProcessModule module = new WindProcessModule()
                .setId(ObjectHelper.primaryKey())
                .setModuleName(ObjectHelper.primaryKey())
                .setParentId(ROOT_MODULE);
        access().insertProcessModule(module);
        commit();
        return module;
    }


    @Test
    public void  addTest(){
        this.engine().repositoryService().selectWindProcessModule();

        WindProcessModule module = new WindProcessModule()
                .setId(ObjectHelper.primaryKey())
                .setModuleName(ObjectHelper.primaryKey())
                .setParentId(ROOT_MODULE);
        int ret = engine().repositoryService().addProcessModule(module);
        assert ret > 0;
        commit();
    }


    @Test
    public void updateTest(){
        this.engine().repositoryService().selectWindProcessModule();
        WindProcessModule module = insert();
        module.setSystem("eqms");
        int ret = this.engine().repositoryService().updateProcessModule(module);
        assert ret > 0;
        commit();
    }


    @Test
    public void deleteTest(){
        WindProcessModule module = insert();
        int ret = this.engine().repositoryService().deleteProcessModule(module.getId());
        //根模块不可删除，故返回0
        assert ret == 1;
        commit();
    }


    @Test
    public void selectTest(){
        ProcessModule module = this.engine().repositoryService().selectWindProcessModule();
        System.out.println(module);
    }
}
