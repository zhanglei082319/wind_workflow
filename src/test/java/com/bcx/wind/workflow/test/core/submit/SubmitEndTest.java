package com.bcx.wind.workflow.test.core.submit;

import com.bcx.wind.workflow.pojo.Wind;
import org.junit.Test;

public class SubmitEndTest extends SubmitBase {

    @Test
    public void submitTest(){
        Wind wind = endSubmit();
        assert wind != null;
        commit();
    }
}
