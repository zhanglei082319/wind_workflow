package com.bcx.wind.workflow.test.access;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.access.WindPage;
import com.bcx.wind.workflow.entity.WindProcess;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.ResourceHelper;
import com.bcx.wind.workflow.support.StreamHelper;
import com.bcx.wind.workflow.support.TimeHelper;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.List;


/**
 * 流程定义持久层测试
 *
 * @author zhanglei
 */
public class ProcessDefinitionTest extends BaseTest {

    private WindProcess insertProcess() throws IOException {
        InputStream stream = ResourceHelper.getResourceAsStream("process/Context.xml");
        WindProcess process = new WindProcess()
                .setId(ObjectHelper.primaryKey())
                .setProcessName("template")
                .setDisplayName("模板提交")
                .setStatus("0")
                .setCreateTime(TimeHelper.nowDate())
                .setContent(StreamHelper.getByte(stream))
                .setParentId(ObjectHelper.primaryKey())
                .setSystem("DMS")
                .setVersion(1);
        access().addProcess(process);
        return process;
    }

    @Test
    public void insertTest() throws IOException {
        InputStream stream = ResourceHelper.getResourceAsStream("process/Context.xml");
        WindProcess process = new WindProcess()
                .setId(ObjectHelper.primaryKey())
                .setProcessName("template")
                .setDisplayName("模板提交")
                .setStatus("0")
                .setCreateTime(TimeHelper.nowDate())
                .setContent(StreamHelper.getByte(stream))
                .setParentId(ObjectHelper.primaryKey())
                .setSystem("DMS")
                .setVersion(1);

        int result = access().addProcess(process);
        commit();
        assert result == 1;
    }

    @Test
    public void deleteProcessTest() throws IOException {
        WindProcess process = insertProcess();
        String id = process.getId();
        int ret = access().removeProcessById(id);
        assert ret == 1;
        commit();
    }


    @Test
    public void  updateProcessTest() throws IOException {
        WindProcess process = insertProcess();
        process.setProcessName("测试更新");
        int ret = access().updateProcess(process);
        assert ret == 1;
        commit();
    }

    @Test
    public void  queryTest(){
        QueryFilter filter = new QueryFilter()
                .setCreateTimeStart(new Timestamp(TimeHelper.getTime("2019-03-12 15:38:11.86","yyyy-MM-dd HH:mm:ss")))
                .setProcessName("template");

        List<WindProcess> processes = access().selectProcessList(filter);
        System.out.println(processes.size());
        commit();
    }

    @Test
    public void queryPageTest(){
        QueryFilter filter = new QueryFilter()
                .setProcessName("template");

        WindPage<WindProcess>  page = new WindPage<>();
        page.setPageSize(4);
        List<WindProcess> processes = access().selectProcessList(filter,page);
        System.out.println(page.getCount());
        System.out.println(processes.size());
        commit();
    }

    @Test
    public void getMaxVersionTest(){
        int ret = access().queryMaxVersionProcess("approve");
        System.out.println(ret);
        commit();
    }

    @Test
    public void queryOneTest() throws IOException {
        WindProcess process = insertProcess();
        WindProcess pr = access().getProcessDefinitionById(process.getId());
        assert pr != null;
        commit();
    }
}
