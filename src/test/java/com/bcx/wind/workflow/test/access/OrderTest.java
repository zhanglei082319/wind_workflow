package com.bcx.wind.workflow.test.access;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.access.WindPage;
import com.bcx.wind.workflow.entity.WindOrder;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.TimeHelper;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * 流程实例测试
 *
 * @author zhanglei
 */
public class OrderTest extends BaseTest {

    private WindOrder insert(){
        WindOrder order = new WindOrder()
                .setId(ObjectHelper.primaryKey())
                .setProcessId(ObjectHelper.primaryKey())
                .setStatus("run")
                .setCreateUser("10001")
                .setCreateTime(TimeHelper.nowDate())
                .setExpireTime(TimeHelper.nowDate())
                .setParentId(ObjectHelper.primaryKey())
                .setVersion(1)
                .setVariable("{}")
                .setData("请假相关流程呢")
                .setSystem("DMS")
                .setFinishTime(null)
                .setBusinessId(ObjectHelper.primaryKey());
        int ret = access().insertOrderInstance(order);
        assert ret == 1;
        return order;
    }

    @Test
    public void insertTest(){
        insert();
        commit();
    }

    @Test
    public void updateOrderTest(){
        WindOrder order = insert();
        order.setFinishTime(TimeHelper.nowDate());
        int ret = access().updateOrderInstance(order);
        assert ret == 1;
        commit();
    }

    @Test
    public void queryOne(){
        WindOrder order = insert();
        order  = access().getOrderInstanceById(order.getId());
        assert order != null;
        commit();
    }

    @Test
    public void removeOne(){
        WindOrder order = insert();
        int ret = access().removeOrderInstanceById(order.getId());
        assert ret == 1;
        commit();
    }

    @Test
    public void removeByParentId(){
        WindOrder order = insert();
        int ret = access().removeOrderInstanceByParentId(order.getParentId());
        assert ret == 1;
        commit();
    }

    @Test
    public void queryTest(){
        QueryFilter filter = new QueryFilter()
                .setVersion(1)
                .setOverTime(true);

        List<WindOrder> orders = access().selectOrderInstanceList(filter);
        System.out.println(orders.size());
        commit();
    }

    @Test
    public void queryPage(){
        QueryFilter filter = new QueryFilter()
                .setVersion(1);

        WindPage<WindOrder> page = new WindPage<>();
        page.setPageSize(5);

        List<WindOrder> result = access().selectOrderInstanceList(filter,page);
        System.out.println(result.size());
        System.out.println(page.getCount());
        commit();
    }

}
