package com.bcx.wind.workflow.test.access;

import com.bcx.wind.workflow.entity.WindProcessModule;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Test;

import java.util.List;

public class ProcessModuleTest extends BaseTest {

    private WindProcessModule insert(){
        WindProcessModule module = new WindProcessModule()
                .setId(ObjectHelper.primaryKey())
                .setModuleName(ObjectHelper.primaryKey());
        int ret = access().insertProcessModule(module);
        assert  ret > 0;
        return module;
    }

    @Test
    public void addTest(){
        insert();
        commit();
    }

    @Test
    public void updateTest(){
        WindProcessModule module = insert();
        module.setModuleName(ObjectHelper.primaryKey())
                .setSystem("dms");

        int ret = access().updateProcessModule(module);
        assert ret > 0;
        commit();
    }

    @Test
    public void deleteTest(){
        WindProcessModule module = insert();

        int ret = access().deleteProcessModule(module.getId());
        assert ret > 0;
        commit();
    }


    @Test
    public void  queryOneTest(){
        WindProcessModule module = insert();

        module = access().selectProcessModuleById(module.getId());
        assert  module != null;
        commit();
    }


    @Test
    public void queryByParentIdTest(){
        WindProcessModule module = insert();
        WindProcessModule child = new WindProcessModule()
                .setId(ObjectHelper.primaryKey())
                .setModuleName(ObjectHelper.primaryKey())
                .setParentId(module.getId());
        access().insertProcessModule(child);
        List<WindProcessModule> result = access().selectProcessModuleByParentId(module.getId());
        assert result != null;
        commit();
    }


    @Test
    public void query(){
        insert();
        List<WindProcessModule> processModules = access().selectProcessModule();
        assert  processModules != null;
        commit();
    }




}
