package com.bcx.wind.workflow.test.access;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.access.TaskFilter;
import com.bcx.wind.workflow.access.WindPage;
import com.bcx.wind.workflow.core.constant.TaskLevel;
import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.entity.WindTaskActor;
import com.bcx.wind.workflow.entity.WindTaskInstance;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.TimeHelper;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * 任务测试
 *
 * @author zhanglei
 */
public class TaskTest extends BaseTest {

    private WindTask insert(){
        WindTask task = new WindTask()
                .setId(ObjectHelper.primaryKey())
                .setTaskName("edit")
                .setDisplayName("编辑")
                .setTaskType("any")
                .setCreateTime(TimeHelper.nowDate())
                .setExpireTime(TimeHelper.nowDate())
                .setApproveCount(1)
                .setStatus("run")
                .setOrderId(ObjectHelper.primaryKey())
                .setProcessId(ObjectHelper.primaryKey())
                .setVariable("{}")
                .setParentId(ObjectHelper.primaryKey())
                .setVersion(1)
                .setPosition("/abc/dsf")
                .setTaskLevel(TaskLevel.main.name());
        int ret = access().insertTaskInstance(task);
        assert ret == 1;
        return task;
    }

    @Test
    public void insertTest(){
        insert();
        commit();
    }

    @Test
    public void  updateTest(){
        WindTask task = insert();
        task.setStatus("stop");
        int ret = access().updateTaskInstance(task);
        assert ret == 1;
        commit();
    }

    @Test
    public void  queryOne(){
        WindTask task = insert();
        task = access().getTaskInstanceById(task.getId());
        assert task != null;
        commit();;
    }

    @Test
    public void removeOne(){
        WindTask task = insert();
        int ret = access().removeTaskInstanceById(task.getId());
        assert ret == 1;
        commit();
    }

    @Test
    public void removeByOrderId(){
        WindTask task = insert();
        int ret = access().removeTaskByOrderId(task.getOrderId());
        assert  ret == 1;
        commit();
    }

    @Test
    public void removeByTaskIds(){
        WindTask task = insert();
        WindTask task1 = insert();
        List<String> taskIds = new LinkedList<>();
        taskIds.add(task.getId());
        taskIds.add(task1.getId());

        int ret = access().removeTaskByTaskIds(taskIds);
        assert ret == 2;
        commit();
    }

    @Test
    public void query(){
        QueryFilter filter = new QueryFilter()
                .setTaskName("edit")
                .setOverTime(true)
                .setTaskActorId(new String[]{"10001","10002"});

        List<WindTask> tasks = access().selectTaskInstanceList(filter);
        System.out.println(tasks.size());
    }

    @Test
    public void  queryList(){
        QueryFilter filter = new QueryFilter()
                .setTaskName("edit")
                .setOverTime(true);

        WindPage<WindTask> page = new WindPage<>();
        page.setPageSize(3);

        List<WindTask> tasks = access().selectTaskInstanceList(filter,page);
        System.out.println(tasks.size());
        System.out.println(page.getCount());
    }


    private WindTaskActor insertActor(){
        WindTaskActor actor = new WindTaskActor()
                .setTaskId(ObjectHelper.primaryKey())
                .setActorId("1003");

        int ret = access().insertTaskActor(actor);
        assert  ret == 1;
        return actor;
    }

    @Test
    public void insertActorTest(){
        insertActor();
        commit();
    }


    @Test
    public void removeActor(){
        WindTaskActor actor = insertActor();
        int ret = access().removeTaskActorByTaskId(actor.getTaskId());
        assert  ret == 1;
        commit();;
    }

    @Test
    public void removeActorByTaskIds(){
        WindTaskActor actor = insertActor();
        WindTaskActor actor1 = insertActor();

        List<String> args = new LinkedList<>();
        args.add(actor.getTaskId());
        args.add(actor1.getTaskId());

        int ret = access().removeTaskActorByTaskIds(args);
        assert ret == 2;
        commit();;
    }

    @Test
    public void removeActorByTaskOrActor(){
        WindTaskActor actor = insertActor();
        int ret = access().removeTaskActor(actor.getTaskId(),actor.getActorId());
        assert ret == 1;
        commit();
    }

    @Test
    public void queryActorTest(){
        QueryFilter filter = new QueryFilter()
                .setTaskActorId(new String[]{"10001"});

        List<WindTaskActor> actors = access().selectTaskActorList(filter);
        System.out.println(actors.size());
        commit();
    }

    @Test
    public void queryTaskInstanceTest(){
        TaskFilter filter = new TaskFilter()
                .setProcessName("holiday")
                .setActor("2009")
                .setOrderBy(true);
        WindPage<WindTaskInstance> page = new WindPage();
        page.setPageSize(2);
        List<WindTaskInstance> taskInstances = access().selectTaskInstance(filter,page);
        System.out.println();
        commit();
    }

}
