package com.bcx.wind.workflow.test.access;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.access.WindPage;
import com.bcx.wind.workflow.core.constant.TaskLevel;
import com.bcx.wind.workflow.entity.WindActHistory;
import com.bcx.wind.workflow.entity.WindEndHistory;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.TimeHelper;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

/**
 * 完毕履历测试
 *
 * @author zhanglei
 */
public class EndHistoryTest extends BaseTest {

    private WindEndHistory insert(){
        WindEndHistory history = new WindEndHistory()
                .setId(ObjectHelper.primaryKey())
                .setTaskId(ObjectHelper.primaryKey())
                .setTaskName("approve")
                .setTaskDisplayName("审核")
                .setProcessId(ObjectHelper.primaryKey())
                .setOrderId(ObjectHelper.primaryKey())
                .setProcessName("holiday_apply")
                .setProcessDisplayName("请假申请")
                .setOperate("提交")
                .setSuggest("同意审核")
                .setApproveTime(TimeHelper.nowDate())
                .setActorId("10002")
                .setActorName("李四")
                .setCreateTime(TimeHelper.nowDate())
                .setApproveUserVariable("[]")
                .setTaskType("all")
                .setSystem("DMS")
                .setSubmitUserVariable("{}")
                .setTaskLevel(TaskLevel.main.name());
        int ret = access().insertCompleteHistory(history);
        assert ret == 1;
        return history;
    }


    @Test
    public void insertTest(){
        insert();
        commit();
    }

    @Test
    public void updateTest(){
        WindEndHistory history =  insert();
        history.setProcessName("abcd");
        int ret = access().updateCompleteHistory(history);
        assert ret == 1;
        commit();
    }

    @Test
    public void queryOne(){
        WindEndHistory history = insert();
        history = access().getCompleteHistoryById(history.getId());
        assert history != null;
        commit();
    }

    @Test
    public void query(){
        QueryFilter filter = new QueryFilter()
                .setProcessName("holiday_apply")
                .setTaskActorId(new String[]{"admin","10001","10002"})
                .setCreateTimeStart(new Timestamp(TimeHelper.getTime("2019-03-13 13:31:17")));

        List<WindEndHistory> histories = access().selectCompleteHistoryList(filter);
        System.out.println(histories.size());
        commit();
    }

    @Test
    public void queryPage(){
        WindPage<WindEndHistory> page = new WindPage<>();
        page.setPageSize(3);

        QueryFilter filter = new QueryFilter()
                .setProcessName("holiday_apply")
                .setTaskActorId(new String[]{"admin","10001","10002"})
                .setCreateTimeStart(new Timestamp(TimeHelper.getTime("2019-03-13 13:31:17")));

        List<WindEndHistory> result = access().selectCompleteHistoryList(filter,page);
        System.out.println(result.size());
        System.out.println(page.getCount());
        commit();
    }

    @Test
    public void deleteOne(){
        WindEndHistory actHistory = insert();

        int ret = access().removeCompleteHistoryById(actHistory.getId());
        assert  ret == 1;
        commit();
    }

    @Test
    public void deleteList(){
        WindEndHistory actHistory = insert();
        WindEndHistory actHistory1 = insert();

        List<String> ids = new LinkedList<>();
        ids.add(actHistory.getId());
        ids.add(actHistory1.getId());

        int ret = access().removeCompleteHistoryIds(ids);
        assert ret == 2;
        commit();
    }

    @Test
    public void insertList(){
        List<WindActHistory> histories = new LinkedList<>();
        histories.add(buildActHistory());
        histories.add(buildActHistory());
        histories.add(buildActHistory());
        int ret = access().insertCompleteHistoryList(histories);
        assert  ret == 3;
        commit();
    }


    private WindActHistory buildActHistory(){
        return  new WindActHistory()
                .setId(ObjectHelper.primaryKey())
                .setTaskId(ObjectHelper.primaryKey())
                .setTaskName("approve")
                .setTaskDisplayName("审核")
                .setProcessId(ObjectHelper.primaryKey())
                .setOrderId(ObjectHelper.primaryKey())
                .setProcessName("holiday_apply")
                .setProcessDisplayName("请假申请")
                .setOperate("提交")
                .setSuggest("同意审核")
                .setApproveTime(TimeHelper.nowDate())
                .setActorId("10002")
                .setActorName("李四")
                .setCreateTime(TimeHelper.nowDate())
                .setApproveUserVariable("[]")
                .setTaskType("all")
                .setSystem("DMS")
                .setSubmitUserVariable("{}");
    }
}
