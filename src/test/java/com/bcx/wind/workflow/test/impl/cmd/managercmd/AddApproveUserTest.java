package com.bcx.wind.workflow.test.impl.cmd.managercmd;

import com.bcx.wind.workflow.pojo.Task;
import com.bcx.wind.workflow.pojo.Wind;
import com.bcx.wind.workflow.pojo.WindUser;
import com.bcx.wind.workflow.test.core.submit.SubmitBase;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class AddApproveUserTest extends SubmitBase {

    @Test
    public  void  addApproveUsers(){
        Wind wind = monitor();

        //获取当前任务
        Task curTask = wind.getCurNode().get(0);

        //获取新任务 即班长任务
        List<Task> newTasks  = curTask.getSubmitTasks();

        //查询其中一个任务的审批人
        Task newTask = newTasks.get(0);
        String taskId = newTask.getWindTask().getId();

        WindUser windUser = new WindUser()
                .setUserId("100001")
                .setUserName("测试用户1");
        WindUser windUser1 = new WindUser()
                .setUserId("100002")
                .setUserName("测试用户2");
        List<WindUser> users = new LinkedList<>();


        users.add(windUser);
        users.add(windUser1);
        engine().managerService().addApproveUsers(taskId,users,null);
        commit();
    }
}
