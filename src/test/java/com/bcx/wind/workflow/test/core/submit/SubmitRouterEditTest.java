package com.bcx.wind.workflow.test.core.submit;

import com.bcx.wind.workflow.entity.WindProcess;
import com.bcx.wind.workflow.pojo.*;
import com.bcx.wind.workflow.pojo.variable.BuildVariable;
import com.bcx.wind.workflow.pojo.variable.SubmitVariable;
import com.bcx.wind.workflow.support.Assert;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.ResourceHelper;
import com.bcx.wind.workflow.support.TimeHelper;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

/**
 * 提交到Context.xml模型中的子节点的第一个节点 routerStart
 *
 * @author zhanglei
 */
public class SubmitRouterEditTest extends SubmitBase {


    @Test
    public void routerEditTest(){
        Wind wind = routerEdit();
        assert wind != null;
        commit();
        System.out.println(wind);
    }
}

