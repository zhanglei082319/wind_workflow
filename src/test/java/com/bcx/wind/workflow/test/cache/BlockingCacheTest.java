package com.bcx.wind.workflow.test.cache;

import com.bcx.wind.workflow.cache.Cache;
import com.bcx.wind.workflow.cache.WindCache;
import com.bcx.wind.workflow.cache.imp.BlockingCache;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;

/**
 * 阻塞缓存测试类
 *
 * @author zhanglei
 */
public class BlockingCacheTest extends BaseTest {


    @Test
    public  void  blockingCacheTest(){
        Cache cache = new BlockingCache(new WindCache());

        for(int i=0 ; i<100 ; i++){
            //设置20个相同的键和80个不同的键，使用阻塞缓存，实际的长度应该为81
            String key = null;
            if(i<20){
                key = "10001";
            }else {
                key = Integer.toString(i);
            }

            final String k = key;
            new Thread(()->{
                cache.put(k,"james");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finishTest();
            }).start();

            System.out.println(cache.size());
        }
        waitTest(100);
        assert cache.size()==81;
    }
}
