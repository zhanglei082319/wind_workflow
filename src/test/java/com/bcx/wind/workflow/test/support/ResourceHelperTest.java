package com.bcx.wind.workflow.test.support;

import com.bcx.wind.workflow.support.ResourceHelper;
import org.junit.Test;

import java.io.IOException;

public class ResourceHelperTest {

    @Test
    public void resourceHelperTest() throws IOException {

        ResourceHelper.getResourceAsStream("logback.xml");

        ResourceHelper.getResourceAsFile("logback.xml");

        ResourceHelper.getResourceURL("logback.xml");

    }
}
