package com.bcx.wind.workflow.test.impl.cmd.managercmd;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.pojo.Wind;
import com.bcx.wind.workflow.pojo.WindUser;
import com.bcx.wind.workflow.test.core.submit.SubmitBase;
import org.junit.Test;

import java.util.List;

public class CloseTaskCmdTest extends SubmitBase {

    @Test
    public void  closeTaskTest(){
        Wind wind = approveA();
        assert  wind != null;


        String orderId = wind.getWindOrder().getId();
        QueryFilter filter = new QueryFilter()
                .setOrderId(orderId);
        List<WindTask> list = this.engine().getConfiguration().getAccess().selectTaskInstanceList(filter);
        String taskId = list.get(0).getId();

        int ret = this.engine().managerService().closeTask(taskId,null);
        assert  ret == 1;
        commit();
    }


    @Test
    public void approveATest(){
        approveA();
        commit();;
    }
}
