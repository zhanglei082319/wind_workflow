package com.bcx.wind.workflow.test.core.complete;

import com.bcx.wind.workflow.pojo.Wind;
import com.bcx.wind.workflow.pojo.variable.CompleteVariable;
import com.bcx.wind.workflow.test.core.submit.SubmitBase;
import org.junit.Test;

public class CompleteTest  extends SubmitBase {

    @Test
    public void completeTest(){
        routerApprove();

        CompleteVariable variable = new CompleteVariable()
                .setOrderId( orderId)
                .setSuggest("完结操作");
        Wind wind = engine.windCoreService().complete(variable);
        assert  wind != null;
        commit();
    }
}
