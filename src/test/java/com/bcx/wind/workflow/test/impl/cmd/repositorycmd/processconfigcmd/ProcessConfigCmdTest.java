package com.bcx.wind.workflow.test.impl.cmd.repositorycmd.processconfigcmd;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.core.constant.ConditionType;
import com.bcx.wind.workflow.entity.WindProcess;
import com.bcx.wind.workflow.entity.WindProcessConfig;
import com.bcx.wind.workflow.pojo.Condition;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.test.BaseTest;
import com.bcx.wind.workflow.test.impl.cmd.repositorycmd.processcmd.AddProcessCmdTest;
import org.junit.Test;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 流程配置相关测试
 *
 * @author zhanglei
 */
public class ProcessConfigCmdTest extends AddProcessCmdTest {

    private WindProcessConfig addConfig(WindProcess process){
        return new WindProcessConfig()
                .setId(ObjectHelper.primaryKey())
                .setProcessId(process.getId())
                .setConfigName("配置名称")
                .setProcessName(process.getProcessName())
                .setNodeId(process.getProcessName());
    }

    @Test
    public void addProcessConfigTest(){
        WindProcess process = addProcess();
        //生效
        engine().repositoryService().release(process.getId());
        //添加配置
        WindProcessConfig config = addConfig(process);
        //添加配置
        int ret = engine().repositoryService().addProcessConfig(config);
        assert  ret == 1;
        commit();
    }

    @Test
    public void updateProcessConfigTest(){
        WindProcess process = addProcess();
        //生效
        engine().repositoryService().release(process.getId());
        //添加配置
        WindProcessConfig config = addConfig(process);
        //添加配置
        int ret = engine().repositoryService().addProcessConfig(config);
        assert ret == 1;
        config.setCondition("\n" +
                "[{\"key\":\"userId\",\"condition\":\"eq\",\"value\":\"10001\",\"logic\":\"AND\"},{\"key\":\"userId\",\"condition\":\"eq\",\"value\":\"10002\",\"logic\":\"OR\"}]");
        int update = engine().repositoryService().updateProcessConfig(config);
        assert  update == 1;
        commit();
    }

    @Test
    public void  removeProcessConfigTest(){
        WindProcess process = addProcess();
        //生效
        engine().repositoryService().release(process.getId());
        //删除
        int ret = engine().repositoryService().removeProcessConfigByProcessId(process.getId());
        System.out.println(ret);
        commit();
    }

    @Test
    public void removeProcessConfigById(){
        WindProcess process = addProcess();
        //生效
        engine().repositoryService().release(process.getId());
        //添加配置
        WindProcessConfig config = addConfig(process);
        //添加配置
        engine().repositoryService().addProcessConfig(config);
        //删除
        int remove = engine().repositoryService().removeProcessConfigById(config.getId());
        assert remove == 1;
        commit();
    }

    @Test
    public void  queryProcessConfig(){
        WindProcess process = addProcess();
        //生效
        engine().repositoryService().release(process.getId());
        QueryFilter filter = new QueryFilter()
                .setProcessId(process.getId());
        List<WindProcessConfig> configs = engine().repositoryService().queryProcessConfig(filter);
        System.out.println(configs.size());
        commit();
    }

    @Test
    public void queryById() {
        WindProcess process = addProcess();
        //生效
        engine().repositoryService().release(process.getId());
        //添加配置
        WindProcessConfig config = addConfig(process);
        //添加配置
        engine().repositoryService().addProcessConfig(config);

    }

    @Test
    public void  addNodeConfig(){
        WindProcess process = addProcess();
        //生效
        engine().repositoryService().release(process.getId());
        //添加配置
        WindProcessConfig config = addConfig(process);
        //添加配置
        engine().repositoryService().addProcessConfig(config);
        Map<String,Object> nodeConfig = new HashMap<>();
        nodeConfig.put("content","审批代办");
        nodeConfig.put("link","任务链接");
        int ret = engine().repositoryService().addNodeConfig(config.getId(),nodeConfig);
        assert  ret == 1;
        commit();
    }

    @Test
    public void removeNodeConfig(){
        WindProcess process = addProcess();
        //生效
        engine().repositoryService().release(process.getId());
        //添加配置
        WindProcessConfig config = addConfig(process);
        //添加配置
        engine().repositoryService().addProcessConfig(config);
        Map<String,Object> nodeConfig = new HashMap<>();
        nodeConfig.put("content","审批代办");
        nodeConfig.put("link","任务链接");
        engine().repositoryService().addNodeConfig(config.getId(),nodeConfig);
        engine().repositoryService().removeNodeConfig(config.getId(),"link");
        commit();
    }

    @Test
    public void  addBusinessConfig(){
        WindProcess process = addProcess();
        //生效
        engine().repositoryService().release(process.getId());
        //添加配置
        WindProcessConfig config = addConfig(process);
        //添加配置
        engine().repositoryService().addProcessConfig(config);
        Map<String,Object> businessConfig = new HashMap<>();
        businessConfig.put("content","审批代办");
        businessConfig.put("link","任务链接");
        int ret = engine().repositoryService().addBusinessConfig(config.getId(),businessConfig);
        assert ret == 1;
        commit();
    }

    @Test
    public void removeBusinessConfig(){
        WindProcess process = addProcess();
        //生效
        engine().repositoryService().release(process.getId());
        //添加配置
        WindProcessConfig config = addConfig(process);
        //添加配置
        engine().repositoryService().addProcessConfig(config);
        Map<String,Object> businessConfig = new HashMap<>();
        businessConfig.put("content","审批代办");
        businessConfig.put("link","任务链接");
        engine().repositoryService().addBusinessConfig(config.getId(),businessConfig);
        engine().repositoryService().removeBusinessConfig(config.getId(),"link");
        commit();
    }

    @Test
    public void addCondition(){
        WindProcess process = addProcess();
        //生效
        engine().repositoryService().release(process.getId());
        //添加配置
        WindProcessConfig config = addConfig(process);
        //添加配置
        engine().repositoryService().addProcessConfig(config);
        Condition condition = new Condition()
                .setKey("userId")
                .setCondition(ConditionType.eq)
                .setValue("10001");
        Condition condition1 = new Condition()
                .setKey("money")
                .setCondition(ConditionType.ngt)
                .setValue("100");
        List<Condition> conditions = new LinkedList<>();
        conditions.add(condition);
        conditions.add(condition1);

        int ret = engine().repositoryService().addConditionConfig(config.getId(),conditions);
        assert ret == 1;
        commit();
    }

    @Test
    public void removeCondition(){
        WindProcess process = addProcess();
        //生效
        engine().repositoryService().release(process.getId());
        //添加配置
        WindProcessConfig config = addConfig(process);
        //添加配置
        engine().repositoryService().addProcessConfig(config);
        Condition condition = new Condition()
                .setKey("userId")
                .setCondition(ConditionType.eq)
                .setValue("10001");
        Condition condition1 = new Condition()
                .setKey("money")
                .setCondition(ConditionType.ngt)
                .setValue("100");
        List<Condition> conditions = new LinkedList<>();
        conditions.add(condition);
        conditions.add(condition1);
        engine().repositoryService().addConditionConfig(config.getId(),conditions);
        int ret = engine().repositoryService().removeConditionConfig(config.getId(),"money");
        assert ret == 1;
        commit();
    }
}
