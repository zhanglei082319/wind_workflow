package com.bcx.wind.workflow.test;

import com.bcx.wind.workflow.WindEngine;
import com.bcx.wind.workflow.access.Access;
import com.bcx.wind.workflow.core.constant.WindDB;
import com.bcx.wind.workflow.db.datasource.WindDataSource;
import com.bcx.wind.workflow.db.tran.WindTransactionManager;
import com.bcx.wind.workflow.imp.WindEngineBuilder;

import javax.sql.DataSource;
import java.util.concurrent.CountDownLatch;

/**
 * @author zhanglei
 */
public class BaseTest {

    public static final String DB_URL = "jdbc:postgresql://localhost:5432/hd_workflow";

    public static final String DRIVER_CLASS = "org.postgresql.Driver";

    public static final String USER_NAME  = "postgres";

    public static final String PASSWORD = "253897";

    public static CountDownLatch latch = null;

    protected WindDataSource dataSource = new WindDataSource(DB_URL,
            DRIVER_CLASS,
            USER_NAME,
            PASSWORD)
            .setMinSize(1);

//    protected WindDataSource dataSource = new WindDataSource("jdbc:mysql://127.0.0.1:3306/wind_workflow?useUnicode=true&characterEncoding=utf8&serverTimezone=UTC"
//            ,"com.mysql.jdbc.Driver","root","253897");

    protected volatile WindEngine engine;

    protected synchronized  WindEngine engine(){
        if(engine == null) {
            WindEngineBuilder builder = WindEngineBuilder.getInstance();
            builder.buildDataSource(dataSource)
                    .buildDbType(WindDB.JDBC);
            engine = builder.buildEngine();
        }
        engine.historyService();
        engine.managerService();
        engine.runtimeService();
        engine.getConfiguration();

        return engine;
    }


    protected Access  access(){
        return engine().getConfiguration().getAccess();
    }


    protected void commit(){
        WindTransactionManager.getInstance().commit();
    }

    /**
     * 该方法为了解决junit不支持多线程测试的问题
     * 调用一下该方法后，在程序需要结束时调用 finishTest()方法
     */
    protected void waitTest(int threadSize){
        latch = new CountDownLatch(threadSize);

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void finishTest(){
        if(latch != null)
           latch.countDown();
    }

}
