package com.bcx.wind.workflow.test.core.submit;

import com.bcx.wind.workflow.pojo.Wind;
import org.junit.Test;

public class SubmitRouterApproveTest extends SubmitBase {

    @Test
    public void submitTest(){
        Wind wind = routerApprove();
        assert wind != null;
        commit();
        System.out.println(wind);
    }
}
