package com.bcx.wind.workflow.test.core.revoke;

import com.bcx.wind.workflow.entity.WindRevokeData;
import com.bcx.wind.workflow.pojo.ApproveUser;
import com.bcx.wind.workflow.pojo.Wind;
import com.bcx.wind.workflow.pojo.WindUser;
import com.bcx.wind.workflow.pojo.variable.RevokeVariable;
import com.bcx.wind.workflow.pojo.variable.SubmitVariable;
import com.bcx.wind.workflow.test.core.submit.SubmitBase;
import org.junit.Test;
import java.util.List;

public class RevokeTest extends SubmitBase {

    @Test
    public void revokeTest(){
        miniter1();

        RevokeVariable variable = new RevokeVariable()
                .setOrderId(orderId);

        Wind wind = engine.windCoreService().revoke(variable);
        assert wind != null;


        List<ApproveUser> approveUsers = miniterApproveUser();
        //提交参数
        SubmitVariable submitVariable = new SubmitVariable()
                .setOrderId(orderId)
                .setUser(new WindUser().setUserId("2010").setUserName("杨过"))
                .setApproveUsers(approveUsers)
                .setSystem("dms")
                .setSuggest("approveB提交！");

        Wind wind1 = engine().windCoreService().submit(submitVariable);
        assert wind1 != null;
        commit();
    }


    @Test
    public void queryRevokeDataTest(){
        miniter1();

        List<WindRevokeData> revokeData = engine.runtimeService().queryRevokeDataByOrderId(orderId);

        assert revokeData != null;
        commit();
    }
}
