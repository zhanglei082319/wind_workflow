package com.bcx.wind.workflow.test.db;

import com.bcx.wind.workflow.db.connection.WindConnection;
import com.bcx.wind.workflow.db.datasource.WindDataSource;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Test;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * wind 数据源测试
 *
 * @author zhanglei
 */
public class DataSourceTest extends BaseTest {

    @Test
    public void dataSourceTest(){

        this.dataSource.setMaxSize(20);
        for(int i=0 ; i< 200 ; i++){
            new Thread(()->{

                try {
                    Thread.sleep(50);
                    WindConnection connection = dataSource.pullConnection();
                    assert dataSource.getActiveConnectionCount() <= 20;
                    System.out.println("连接池总数量："+dataSource.getActiveConnectionCount()+", "+
                            (dataSource.getConnPools().getActiveConnections().size()+dataSource.getConnPools().getConnections().size()));
                    Thread.sleep(1000);
                    dataSource.pushConnection(connection);
                    finishTest();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }).start();
        }

        waitTest(200);

    }
}
