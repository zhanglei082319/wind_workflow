package com.bcx.wind.workflow.test.impl;

import com.bcx.wind.workflow.WindEngine;
import com.bcx.wind.workflow.core.constant.WindDB;
import com.bcx.wind.workflow.imp.WindEngineBuilder;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Test;


/**
 * 工作流引擎创建测试
 *
 * @author zhanglei
 */
public class WindEngineBuilderTest extends BaseTest {

    @Test
    public void buildEngineTest(){
        WindEngineBuilder builder = WindEngineBuilder.getInstance();
        builder.buildDataSource(dataSource)
                .buildDbType(WindDB.JDBC);
        WindEngine engine = builder.buildEngine();
        System.out.println();
    }

}
