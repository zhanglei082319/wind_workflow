package com.bcx.wind.workflow.core.flow.node;

import com.bcx.wind.workflow.core.constant.NodeType;

/**
 * 或者聚合节点
 *
 * @author zhanglei
 */
public class OrJoinNode extends BaseJoinNode {


    public OrJoinNode(String nodeId, String nodeName) {
        super(nodeId, nodeName);
        this.nodeType = NodeType.OR_JOIN;
    }

    @Override
    public void executor() {
        filterNotMustApprove();
        next(null);
    }

    public void build(){
        //构建路线
        buildPaths(now);
        //构建节点指针
        createNodePointer();
    }
}
