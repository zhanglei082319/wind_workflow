package com.bcx.wind.workflow.core.constant;

/**
 * 任务状态
 *
 * @author zhanglei
 */
public enum  TaskStatus {
    /**
     * 执行
     */
    run,

    /**
     * 暂停
     */
    stop,
}
