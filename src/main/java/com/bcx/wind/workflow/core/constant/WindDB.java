package com.bcx.wind.workflow.core.constant;

/**
 * 工作流持久层类型
 *
 * @author zhanglei
 */
public enum  WindDB {

    /**
     * spring mybatis jdbc
     */
    SPRING("spring"),
    MYBATIS("mybatis"),
    JDBC("jdbc");

    /**
     * 值
     */
    private String value;

    private WindDB(String value){
        this.value = value;
    }



}
