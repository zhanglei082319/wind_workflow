package com.bcx.wind.workflow.core.constant;

/**
 * 流程实例中的变量
 *
 * @author zhanglei
 */
public class OrderVariableConstant {

    /**
     * 任务节点的审批人，在退回操作时，获取退回节点的审批人进行退回。该值在提交时，保存提交节点的审批人。
     */
    public static final String TASK_APPROVE_USER = "taskApproveUser";


    /**
     * 审批人合并的数据
     */
    public static final String APPROVE_USER_MERGE = "approveUserMerge";


    /**
     * 串签审批人数据
     */
    public static final String STRAND_APPROVE_USER = "strandApproveUser";


    /**
     * 会签时最少审批人变量数据
     */
    public static final String JOINTLY_MIN_COUNT = "jointlyMinCount";

    /**
     * 会签时需要通过多少人成功 数量
     */
    public static final String JOINTLY_SUCCESS_COUNT = "jointlySuccessCount";

    /**
     * 会签任务当前已经通过数量
     */
    public static final String JOINTLY_NOW_COUNT = "jointlyNowCount";
}
