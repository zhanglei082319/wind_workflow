package com.bcx.wind.workflow.core.constant;

/**
 * 流程定义状态
 *
 * @author zhanglei
 */
public class ProcessStatus {

    /**
     * 发布
     */
    public  static final String  RELEASE = "release";

    /**
     * 回收
     */
    public static final  String  RECOVERY = "recovery";


    /**
     * 模板
     */
    public static final String  TEMPLATE = "template";
}
