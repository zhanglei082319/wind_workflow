package com.bcx.wind.workflow.core.lock;

/**
 * 锁接口
 *
 * @author zhanglei
 */
public interface Lock {

    /**
     * 获取锁  指定具体锁
     * @param object 锁键
     */
    void  lock(Object object);


    /**
     * 释放锁  指定具体锁
     * @param object 锁键
     */
    void  unlock(Object object);



}
