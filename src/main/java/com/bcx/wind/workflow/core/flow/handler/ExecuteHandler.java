package com.bcx.wind.workflow.core.flow.handler;

/**
 * 执行任务 主要针对于会签，串签，普通等任务类型进行区分执行
 *
 * @author zhanglei
 */
public interface ExecuteHandler {

    /**
     * 执行
     */
    void  execute();
}
