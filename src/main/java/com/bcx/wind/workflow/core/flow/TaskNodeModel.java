package com.bcx.wind.workflow.core.flow;


import java.util.List;

/**
 * 任务节点模型  类型接口
 *
 * @author zhanglei
 */
public interface TaskNodeModel extends TaskModel{

    /**
     * 是否为会签
     *
     * @return   boolean
     */
    boolean jointly();

    /**
     * 是否串签
     *
     * @return  boolean
     */
    boolean strand();


    /**
     * 返回是否在分支聚合中
     *
     * @return  boolean
     */
    boolean  inForkJoin();

    /**
     * 是否在并且分支中
     *
     * @return  boolean
     */
    boolean  inAnd();

    /**
     * 是否在或者分支中
     *
     * @return  boolean
     */
    boolean  inOr();


    /**
     * 是否在动态分支中
     *
     * @return  boolean
     */
    boolean  inDyna();


    /**
     * 是否为第一个任务
     *
     * @return  boolean
     */
    boolean  isFirstTask();


    /**
     * 是否为最后一个任务
     *
     * @return boolean
     */
    boolean  isLastTask();


}
