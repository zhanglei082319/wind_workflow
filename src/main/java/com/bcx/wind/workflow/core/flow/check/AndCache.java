package com.bcx.wind.workflow.core.flow.check;

/**
 * 判断节点是否在并且分支中
 *
 * @author zhanglei
 */

import com.bcx.wind.workflow.support.ObjectHelper;

/**
 * 并且分支缓存，用于区分节点是否在分支中
 *
 * @author zhanglei
 */
public class AndCache {

    private ThreadLocal<Integer> inAnd = new ThreadLocal<>();

    private AndCache(){}

    private static class AndCacheInstance {
        private AndCacheInstance(){}
        private static AndCache andCache = new AndCache();
    }

    public static AndCache getInstance(){
        return AndCacheInstance.andCache;
    }

    public boolean inAnd(){
        Integer integer = this.inAnd.get();
        return !ObjectHelper.isEmpty(integer) && integer==1;
    }

    public  void  inAnd(Integer inAnd){
        this.inAnd.set(inAnd);
    }

    public  Integer  get(){
        return inAnd.get();
    }

    public  void  remove(){
        this.inAnd.remove();
    }
}

