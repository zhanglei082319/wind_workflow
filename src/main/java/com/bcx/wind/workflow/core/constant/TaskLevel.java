package com.bcx.wind.workflow.core.constant;

/**
 * 任务级别
 *
 * 目前就两种  主任务  和 主任务衍生出来的任务 子任务
 * @author zhanglei
 */
public enum  TaskLevel {

    /**
     * 主任务
     */
    main,

    /**
     * 子任务
     */
    child,
}
