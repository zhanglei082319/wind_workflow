package com.bcx.wind.workflow.core.flow.node;

import com.bcx.wind.workflow.core.constant.NodeType;
import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.support.ObjectHelper;

import java.util.List;

/**
 * 并且聚合节点
 *
 * @author zhanglei
 */
public class AndJoinNode extends BaseJoinNode{

    public AndJoinNode(String nodeId, String nodeName) {
        super(nodeId, nodeName);
        this.nodeType = NodeType.AND_JOIN;
    }

    @Override
    public void executor() {
        List<WindTask> allTasks =  filterNotMustApprove();

        if(!ObjectHelper.isEmpty(allTasks)){
            return;
        }
        //向后面执行
        next(null);
    }


    public void build(){
        //构建路线
        buildPaths(now);
        //构建节点指针
        createNodePointer();
    }
}
