package com.bcx.wind.workflow.core.constant;

/**
 * 工作流节点类型
 *
 * @author zhanglei
 */
public enum NodeType {

    /**
     * 节点类型
     */
    START("start"),
    END("end"),
    TASK("task"),
    SCRIBE("scribe"),
    SCRIBE_TASK("scribeTask"),
    DISC("disc"),
    CUSTOM("custom"),
    AND("and"),
    OR("or"),
    DYNA("dyna"),
    AND_JOIN("andJoin"),
    OR_JOIN("orJoin"),
    DYNA_JOIN("dynaJoin"),
    ROUTER("router"),
    PROCESS("process");

    private String value;

    private NodeType(String value){
        this.value = value;
    }

    public String value(){
        return this.value;
    }

    public NodeType  getNodeType(String type){
        NodeType[] types = values();
        for(NodeType nodeType : types){
            if(nodeType.value.equals(type)){
                return nodeType;
            }
        }
        return null;
    }

    public boolean  exists(String type){
        return this.getNodeType(type) != null;
    }

}
