package com.bcx.wind.workflow.core.flow.check;

import com.bcx.wind.workflow.support.ObjectHelper;

/**
 * 判断任务节点是否在或者分支中
 *
 * @author zhanglei
 */
public class OrCache {

    private ThreadLocal<Integer>  inOr = new ThreadLocal<>();

    private OrCache(){}


    private static class OrCacheInstance{
        private OrCacheInstance(){}
        private static OrCache orCache = new OrCache();
    }

    public static OrCache getInstance(){
        return OrCacheInstance.orCache;
    }

    public boolean inOr(){
        Integer integer =  this.inOr.get();
        return !ObjectHelper.isEmpty(integer) && integer==1;
    }

    public Integer  get(){
        return  inOr.get();
    }

    public void  inOr(int inOr){
        this.inOr.set(inOr);
    }

    public void remove(){
        this.inOr.remove();
    }
}

