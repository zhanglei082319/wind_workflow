package com.bcx.wind.workflow.core.flow;

/**
 * 基类
 *
 * @author zhanglei
 */
public class Node {

    /**
     * 节点名称
     */
    private String nodeId;

    /**
     * 显示名称
     */
    private String nodeName;

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }
}
