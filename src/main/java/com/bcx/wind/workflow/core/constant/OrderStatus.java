package com.bcx.wind.workflow.core.constant;

/**
 * 流程实例状态
 *
 * @author zhanglei
 */
public enum OrderStatus {

    /**
     * 运行
     */
    run,
    /**
     * 暂停
     */
    stop,
    /**
     * 完成
     */
    complete,

}
