package com.bcx.wind.workflow.core.flow.node;

import com.bcx.wind.workflow.core.constant.NodeType;

/**
 * 订阅节点
 *
 * @author zhanglei
 */
public class ScribeNode extends BaseNode {

    public ScribeNode(String nodeId, String nodeName) {
        super(nodeId, nodeName);
        this.nodeType = NodeType.SCRIBE;
    }

    @Override
    public void executor() {
        next(null);
    }


    public void build(){
        //构建路线
        buildPaths(now);
        //构建节点指针
        createNodePointer();
    }
}
