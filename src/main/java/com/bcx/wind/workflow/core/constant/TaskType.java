package com.bcx.wind.workflow.core.constant;

/**
 * 任务类型
 *
 * @author zhanglei
 */
public enum  TaskType {

    /**
     * 会签
     */
    jointly,
    /**
     * 普通
     */
    ordinary,
    /**
     * 串签
     */
    strand,
    /**
     * 订阅任务
     */
    scribe;



    public static TaskType  taskType(String type){
        try {
            return valueOf(type);
        }catch (Exception e){
            return null;
        }
    }

}
