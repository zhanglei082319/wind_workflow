package com.bcx.wind.workflow.core.flow.handler;

import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.pojo.Task;

import java.util.stream.Collectors;

/**
 * 会签handler
 *
 * @author zhanglei
 */
public class JointlyHandler extends BaseExecuteHandler{

    public JointlyHandler(CommandContext commandContext, Task task) {
        super(commandContext, task);
    }

    @Override
    public void commandExecute() {
        //添加会签任务需要完成的最少提交数量
        addTaskApproveCount();

        //合并审批人
        mergeUser();
    }

    @Override
    public int allTaskCount() {
        return this.wind().getAllTasks().stream().filter(t->t.getTaskName().equals(this.task.getNodeId()))
                   .collect(Collectors.toList()).size();
    }


}
