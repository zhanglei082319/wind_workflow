package com.bcx.wind.workflow.core.flow.node;

import com.bcx.wind.workflow.core.flow.Node;
import com.bcx.wind.workflow.core.flow.check.AndCache;
import com.bcx.wind.workflow.core.flow.check.DynaCache;
import com.bcx.wind.workflow.core.flow.check.OrCache;
import com.bcx.wind.workflow.errorcontext.ErrorContext;
import com.bcx.wind.workflow.errorcontext.WindLocalError;
import com.bcx.wind.workflow.exception.WorkflowException;
import com.bcx.wind.workflow.message.MessageHelper;

import static com.bcx.wind.workflow.message.ErrorCode.*;
import static com.bcx.wind.workflow.message.ErrorCode.XML_ASSIGNE_NOT_FOUND;

/**
 * @author zhanglei
 */
abstract class AbstractNode extends Node {

    /**
     * 校验聚合节点处出错
     * @param joinName   聚合节点名称
     * @param  forkType  分支类型
     */
    void  joinCacheChange(String joinName,int forkType){
        switch (forkType){
            case 1:
                if(AndCache.getInstance().inAnd()){
                    AndCache.getInstance().inAnd(0);
                    return;
                }
                break;
            case 2:
                if(OrCache.getInstance().inOr()){
                    OrCache.getInstance().inOr(0);
                    return;
                }
                break;
            case 3:
                if(DynaCache.getInstance().inDyna()){
                    DynaCache.getInstance().inDyna(0);
                    return;
                }
                break;
            default:break;
        }
        ErrorContext errorContext = WindLocalError.get();
        errorContext.code(XML_FORK_JOIN_ERROR)
                .errorMsg(MessageHelper.getMsg(XML_FORK_JOIN_ERROR,joinName));
        errorContext.processBuilderError()
                .errorValue(new String[]{joinName});
        throw new WorkflowException(MessageHelper.getMsg(XML_FORK_JOIN_ERROR,joinName));
    }


    /**
     * 校验分支出错
     * @param forkName 分支名称
     * @param  forkType  分支类型 1并且分支  2或者分支  3动态分支
     */
    void  forkCacheChange(String forkName,int forkType){
        switch (forkType){
            case  1:
                if(!AndCache.getInstance().inAnd()){
                    AndCache.getInstance().inAnd(1);
                    return;
                }
                break;
            case 2:
                if(!OrCache.getInstance().inOr()){
                    OrCache.getInstance().inOr(1);
                    return;
                }
                break;
            case 3:
                if(!DynaCache.getInstance().inDyna()){
                    DynaCache.getInstance().inDyna(1);
                    return;
                }
                break;
            default: break;
        }

        ErrorContext errorContext = WindLocalError.get();
        errorContext.code(XML_FORK_JOIN_ERROR)
                .errorMsg(MessageHelper.getMsg(XML_FORK_JOIN_ERROR,forkName));
        errorContext.processBuilderError()
                .errorValue(new String[]{forkName});

        throw new WorkflowException(MessageHelper.getMsg(XML_FORK_JOIN_ERROR,forkName));
    }


    void pathToSameNowNode(String to,String nowNodeId,String path){
        if(to.equals(nowNodeId)){
            ErrorContext errorContext = WindLocalError.get();

            errorContext.code(XML_PATH_LOOP)
                    .errorMsg(MessageHelper.getMsg(XML_PATH_LOOP,path));
            errorContext.processBuilderError()
                    .errorValue(new String[]{to,path});

            throw new WorkflowException(MessageHelper.getMsg(XML_PATH_LOOP,path));
        }
    }

    void pathToNotFound(boolean exists,String[] errorValue,String path){
        if(!exists){
            ErrorContext errorContext = WindLocalError.get();

            errorContext.code(XML_PATH_TO_NULL)
                    .errorMsg(MessageHelper.getMsg(XML_PATH_TO_NULL,path));
            errorContext.processBuilderError()
                    .errorValue(errorValue);
            throw new WorkflowException(MessageHelper.getMsg(XML_PATH_TO_NULL,path));
        }
    }


    void assigneeNotFound(boolean findAssignee,String[] errorValue,String assignee){
        if(!findAssignee){
            ErrorContext errorContext = WindLocalError.get();
            errorContext.code(XML_ASSIGNE_NOT_FOUND)
                    .errorMsg(MessageHelper.getMsg(XML_ASSIGNE_NOT_FOUND,assignee));
            errorContext.processBuilderError()
                    .errorValue(errorValue);

            throw new WorkflowException(MessageHelper.getMsg(XML_ASSIGNE_NOT_FOUND,assignee));
        }
    }


}
