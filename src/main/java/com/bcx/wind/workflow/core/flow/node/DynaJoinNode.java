package com.bcx.wind.workflow.core.flow.node;

import com.bcx.wind.workflow.core.constant.NodeType;
import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.support.ObjectHelper;

import java.util.List;

/**
 * 动态分支聚合节点
 *
 * @author zhanglei
 */
public class DynaJoinNode extends BaseJoinNode {

    public DynaJoinNode(String nodeId, String nodeName) {
        super(nodeId, nodeName);
        this.nodeType = NodeType.DYNA_JOIN;
    }

    public void build(){
        //构建路线
        buildPaths(now);
        //构建节点指针
        createNodePointer();
    }

    @Override
    public void executor() {
        List<WindTask> allTasks =  filterNotMustApprove();

        if(!ObjectHelper.isEmpty(allTasks)){
            return;
        }
        //向后面执行
        next(null);
    }

}
