package com.bcx.wind.workflow.core.flow.node;

import com.bcx.wind.workflow.core.constant.NodeType;
import com.bcx.wind.workflow.core.flow.TaskModel;

/**
 * 订阅任务节点
 *
 * @author zhanglei
 */
public class ScribeTaskNode extends BaseNode implements TaskModel {

    /**
     * 是否拦截
     */
    private boolean interceptor;

    public ScribeTaskNode(String nodeId, String nodeName) {
        super(nodeId, nodeName);
        this.nodeType = NodeType.SCRIBE_TASK;
    }

    @Override
    public void executor() {
        //此处不需要实现
    }


    public void build(){
        //构建路线
        buildPaths(now);
        //构建节点指针
        createNodePointer();
    }

    public boolean isInterceptor() {
        return interceptor;
    }

    public void setInterceptor(boolean interceptor) {
        this.interceptor = interceptor;
    }
}
