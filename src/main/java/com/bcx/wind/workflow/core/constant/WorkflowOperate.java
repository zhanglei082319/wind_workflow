package com.bcx.wind.workflow.core.constant;

/**
 * 工作流操作
 *
 * @author zhanglei
 */
public enum  WorkflowOperate {

    /**
     * 提交
     */
    submit,
    /**
     * 退回
     */
    reject,
    /**
     * 撤回
     */
    withdraw,
    /**
     * 完结
     */
    complete,
    /**
     * 撤销
     */
    revoke,
    /**
     * 转办
     */
    transfer,
    /**
     * 关闭
     */
    close,
    /**
     * 创建
     */
    build;
}
