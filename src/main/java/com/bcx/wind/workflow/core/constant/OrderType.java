package com.bcx.wind.workflow.core.constant;

/**
 * 流程实例类型
 */
public enum  OrderType {

    /**
     * 主流程
     */
    main,

    /**
     * 子流程
     */
    child,

}
