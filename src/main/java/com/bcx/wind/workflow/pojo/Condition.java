package com.bcx.wind.workflow.pojo;

import com.bcx.wind.workflow.core.constant.ConditionType;
import com.bcx.wind.workflow.core.constant.Logic;

/**
 * 匹配规则实体
 *
 * @author zhanglei
 */
public class Condition {

    /**
     * 匹配键
     */
    private String key;

    /**
     * 匹配规则
     */
    private ConditionType condition;

    /**
     * 匹配值  可以是数组，也可以是字符串
     */
    private Object value;

    /**
     * 逻辑  默认并且
     */
    private Logic logic = Logic.AND;

    public Condition(){

    }

    public Condition(String key,ConditionType condition,String value){
        this.key = key;
        this.condition = condition;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public Condition setKey(String key) {
        this.key = key;
        return this;
    }

    public ConditionType getCondition() {
        return condition;
    }

    public Condition setCondition(ConditionType condition) {
        this.condition = condition;
        return this;
    }

    public Condition setValue(String value) {
        this.value = value;
        return this;
    }

    public Object getValue() {
        return value;
    }

    public Condition setValue(Object value) {
        this.value = value;
        return this;
    }

    public Logic getLogic() {
        return logic;
    }

    public Condition setLogic(Logic logic) {
        this.logic = logic;
        return this;
    }
}
