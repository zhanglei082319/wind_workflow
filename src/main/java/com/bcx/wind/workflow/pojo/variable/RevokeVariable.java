package com.bcx.wind.workflow.pojo.variable;

import com.bcx.wind.workflow.pojo.WindUser;

import java.util.Map;

/**
 * 撤销操作参数实体
 *
 * @author zhanglei
 */
public class RevokeVariable {

    /**
     * 业务数据ID
     */
    private String businessId;

    /**
     * 流程实例ID
     */
    private  String  orderId;

    /**
     * 建议
     */
    private String suggest;

    /**
     * 撤销ID   wind_revoke_data  id
     */
    private String histId;

    /**
     * 匹配参数
     */
    private Map<String,Object> args;

    /**
     * 当前用户
     */
    private WindUser user;

    /**
     * 模块
     */
    private String system;

    public String getBusinessId() {
        return businessId;
    }

    public RevokeVariable setBusinessId(String businessId) {
        this.businessId = businessId;
        return this;
    }

    public String getOrderId() {
        return orderId;
    }

    public RevokeVariable setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getSuggest() {
        return suggest;
    }

    public RevokeVariable setSuggest(String suggest) {
        this.suggest = suggest;
        return this;
    }

    public String getHistId() {
        return histId;
    }

    public RevokeVariable setHistId(String histId) {
        this.histId = histId;
        return this;
    }

    public Map<String, Object> getArgs() {
        return args;
    }

    public RevokeVariable setArgs(Map<String, Object> args) {
        this.args = args;
        return this;
    }

    public WindUser getUser() {
        return user;
    }

    public RevokeVariable setUser(WindUser user) {
        this.user = user;
        return this;
    }

    public String getSystem() {
        return system;
    }

    public RevokeVariable setSystem(String system) {
        this.system = system;
        return this;
    }
}
