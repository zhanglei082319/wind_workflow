package com.bcx.wind.workflow.pojo.variable;

import com.bcx.wind.workflow.pojo.WindUser;

import java.util.Map;

/**
 * 退回操作参数
 *
 * @author zhanglei
 */
public class RejectVariable {

    /**
     * 流程实例号
     */
    private  String orderId;

    /**
     * 指定任务ID
     */
    private  String taskId;

    /**
     * 当前用户
     */
    private WindUser user;

    /**
     * 匹配参数
     */
    private Map<String,Object> args;

    /**
     * 任务扩展变量字段数据， 该参数不参与引擎业务，会被自动保存到任务变量variable字段中，供二次开发使用
     */
    private Map<String,Object> vars;

    /**
     * 退回节点
     */
    private String  returnNode;

    /**
     * 模块
     */
    private String system;

    /**
     * 建议
     */
    private String suggest;

    public String getOrderId() {
        return orderId;
    }

    public RejectVariable setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public WindUser getUser() {
        return user;
    }

    public RejectVariable setUser(WindUser user) {
        this.user = user;
        return this;
    }

    public Map<String, Object> getArgs() {
        return args;
    }

    public RejectVariable setArgs(Map<String, Object> args) {
        this.args = args;
        return this;
    }

    public String getReturnNode() {
        return returnNode;
    }

    public RejectVariable setReturnNode(String returnNode) {
        this.returnNode = returnNode;
        return this;
    }

    public String getSystem() {
        return system;
    }

    public RejectVariable setSystem(String system) {
        this.system = system;
        return this;
    }

    public String getSuggest() {
        return suggest;
    }

    public RejectVariable setSuggest(String suggest) {
        this.suggest = suggest;
        return this;
    }

    public String getTaskId() {
        return taskId;
    }

    public RejectVariable setTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public Map<String, Object> getVars() {
        return vars;
    }

    public RejectVariable setVars(Map<String, Object> vars) {
        this.vars = vars;
        return this;
    }
}
