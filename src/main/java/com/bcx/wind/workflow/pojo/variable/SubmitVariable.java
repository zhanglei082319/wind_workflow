package com.bcx.wind.workflow.pojo.variable;

import com.bcx.wind.workflow.pojo.ApproveUser;
import com.bcx.wind.workflow.pojo.WindUser;

import java.util.List;
import java.util.Map;

/**
 * 工作流核心操作参数
 *
 * @author zhanglei
 */
public class SubmitVariable {

    /**
     * 当前用户
     */
    private WindUser user;


    /**
     * 流程实例ID
     */
    private String orderId;


    /**
     * 指定任务的Id
     */
    private String taskId;


    /**
     * 审批人
     */
    private List<ApproveUser> approveUsers;


    /**
     * 建议
     */
    private String suggest;


    /**
     * 提交路线
     */
    private Map<String,List<String>> submitPath;


    /**
     * 匹配参数
     */
    private Map<String,Object> args;


    /**
     * 任务扩展变量字段数据， 该参数不参与引擎业务，会被自动保存到任务变量variable字段中，供二次开发使用
     */
    private Map<String,Object> vars;


    /**
     * 自由提交节点
     */
    private String submitNode;


    /**
     * 流程所属业务系统，流程可支持多个系统
     */
    private String system;

    public WindUser getUser() {
        return user;
    }

    public SubmitVariable setUser(WindUser user) {
        this.user = user;
        return this;
    }


    public String getOrderId() {
        return orderId;
    }

    public SubmitVariable setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public List<ApproveUser> getApproveUsers() {
        return approveUsers;
    }

    public SubmitVariable setApproveUsers(List<ApproveUser> approveUsers) {
        this.approveUsers = approveUsers;
        return this;
    }


    public String getSuggest() {
        return suggest;
    }

    public SubmitVariable setSuggest(String suggest) {
        this.suggest = suggest;
        return this;
    }

    public Map<String, List<String>> getSubmitPath() {
        return submitPath;
    }

    public SubmitVariable setSubmitPath(Map<String, List<String>> submitPath) {
        this.submitPath = submitPath;
        return this;
    }

    public Map<String, Object> getArgs() {
        return args;
    }

    public SubmitVariable setArgs(Map<String, Object> args) {
        this.args = args;
        return this;
    }

    public String getSubmitNode() {
        return submitNode;
    }

    public SubmitVariable setSubmitNode(String submitNode) {
        this.submitNode = submitNode;
        return this;
    }

    public String getSystem() {
        return system;
    }

    public SubmitVariable setSystem(String system) {
        this.system = system;
        return this;
    }

    public String getTaskId() {
        return taskId;
    }

    public SubmitVariable setTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public Map<String, Object> getVars() {
        return vars;
    }

    public SubmitVariable setVars(Map<String, Object> vars) {
        this.vars = vars;
        return this;
    }
}
