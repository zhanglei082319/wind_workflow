package com.bcx.wind.workflow.pojo.variable;

import com.bcx.wind.workflow.pojo.WindUser;

import java.util.Map;

/**
 * 撤回操作参数
 *
 * @author zhanglei
 */
public class WithDrawVariable {

    /**
     * 流程实例ID
     */
    private String orderId;

    /**
     * 建议
     */
    private String suggest;

    /**
     * 当前用户
     */
    private WindUser user;

    /**
     * 匹配数据
     */
    private Map<String,Object> args;

    /**
     * 所属模块
     */
    private String system;


    public String getOrderId() {
        return orderId;
    }

    public WithDrawVariable setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getSuggest() {
        return suggest;
    }

    public WithDrawVariable setSuggest(String suggest) {
        this.suggest = suggest;
        return this;
    }

    public WindUser getUser() {
        return user;
    }

    public WithDrawVariable setUser(WindUser user) {
        this.user = user;
        return this;
    }

    public Map<String, Object> getArgs() {
        return args;
    }

    public WithDrawVariable setArgs(Map<String, Object> args) {
        this.args = args;
        return this;
    }

    public String getSystem() {
        return system;
    }

    public WithDrawVariable setSystem(String system) {
        this.system = system;
        return this;
    }
}
