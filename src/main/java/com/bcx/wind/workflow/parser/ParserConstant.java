package com.bcx.wind.workflow.parser;

/**
 * 解析所需常量
 *
 * @author zhanglei
 */
public class ParserConstant {

    /**
     * 名称
     */
    public static final String NAME = "name";

    /**
     * 显示名称
     */
    public static final String DISPLAY_NAME = "displayName";


    public static final String NULL_STR = "";

    public static final String P_HTM = "<p>";

    public static final String RED_P_HTM = "<p style=\"color:red\">";

    public static final String P_HTML_ = "</P>";

    public static final String LT = "&lt;";

    public static final String GT = "&gt;";

    public static final String DOCTYPE = "DOCTYPE";

    public static final String WORKFLOW_PROCESS_SYSTEM = "workflow-1-process.dtd";

    public static final String WORKFLOW_PROCESS_DTD = "com/bcx/wind/workflow/parser/dtd/workflow-1-process.dtd";

    public static final String  DTD = "<!DOCTYPE process PUBLIC \"-//workflow\" \"workflow-1-process.dtd\">";

    public static final String XML_DOC = "?>";

    public static final String XML_TAIL = ">";

    public static final String XML_HEAD = "<";

    public static final String XML_TAIL_ = "/>";

}
