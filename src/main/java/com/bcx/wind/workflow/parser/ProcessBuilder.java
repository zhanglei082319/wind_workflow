package com.bcx.wind.workflow.parser;

import com.bcx.wind.workflow.core.flow.check.AndCache;
import com.bcx.wind.workflow.core.flow.check.DynaCache;
import com.bcx.wind.workflow.core.flow.check.OrCache;
import com.bcx.wind.workflow.core.flow.node.ProcessModel;
import com.bcx.wind.workflow.core.flow.node.StartNode;
import com.bcx.wind.workflow.errorcontext.DefaultErrorContext;
import com.bcx.wind.workflow.errorcontext.ErrorContext;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.errorcontext.WindLocalError;
import com.bcx.wind.workflow.exception.WorkflowException;
import com.bcx.wind.workflow.message.MessageHelper;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.StreamHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.*;

import static com.bcx.wind.workflow.core.constant.NodeNameConstant.START;
import static com.bcx.wind.workflow.core.flow.check.NodeCache.getInstance;
import static com.bcx.wind.workflow.message.ErrorCode.*;
import static com.bcx.wind.workflow.parser.ParserConstant.DISPLAY_NAME;
import static com.bcx.wind.workflow.parser.ParserConstant.NAME;

/**
 * 流程定义创建器
 *
 * @author zhanglei
 */
public class ProcessBuilder {

    private static Logger logger = LoggerFactory.getLogger(ProcessBuilder.class);

    /**
     * 流程定义流
     */
    private InputStream stream;


    /**
     * 流程定义二进制
     */
    private byte[]  bytes;

    /**
     * 流程定义字符串
     */
    private String  content;

    /**
     * 生成的流程定义xml内容
     */
    private String source;

    /**
     * 解析好后的流程定义对象  如果解析失败为null
     */
    private ProcessModel processModel;


    /**
     * 错误信息
     */
    private ErrorContext errorContent;

    /**
     * 构建是否成功
     */
    private boolean success = true;


    ProcessBuilder(InputStream stream){
        this.stream = stream;
    }

    ProcessBuilder(String content){
        this.content = content;
    }

    ProcessBuilder(byte[] bytes){
        this.bytes = bytes;
    }

    private Document  buildDocument(){
        if(this.stream != null){
            this.source = XmlHelper.getStandardXml(StreamHelper.getStr(stream));
            return XmlHelper.buildDoc(this.source);
        }else if(this.content != null){
            this.source = XmlHelper.getStandardXml(content);
            return XmlHelper.buildDoc(content);
        }else if(this.bytes != null){
            try {
                String xml = new String(this.bytes,"utf-8");
                this.source = XmlHelper.getStandardXml(xml);
                return XmlHelper.buildDoc(this.source);
            } catch (UnsupportedEncodingException e) {
                logger.error(e.getMessage(),e);
                if(logger.isDebugEnabled()){
                    logger.debug(MessageHelper.getMsg(PROCESS_ERROR));
                }
                WindError.error(PROCESS_ERROR,e);
            }
        }else {
            if(logger.isDebugEnabled()){
                logger.debug(MessageHelper.getMsg(XML_EMPTY));
            }
            WindError.error(XML_EMPTY,null);
        }
        return null;
    }


    public void  buildProcess(String processId){
        //构建document
        Document document = buildDocument();
        //构建流程定义
        try {
            initProcess(document, processId);
        }catch (Exception e){
            if(logger.isDebugEnabled()){
                logger.debug(e.getMessage());
            }
            logger.error(e.getMessage(),e);
            buildHalfwayErrorContent();
        }
        //构建结束回收校验操作
        endOperate();
    }


    /**
     * 构建创建流程定义对象过程中存在的错误
     */
    private void  buildHalfwayErrorContent(){
        if(WindLocalError.get() != null){
            this.errorContent =  WindLocalError.get();
            findErrorXmlContent();
        }else {
            this.errorContent = new DefaultErrorContext()
                    .code(XML_ERROR);
            this.errorContent.processBuilderError()
                    .setErrorMsg(MessageHelper.getMsg(XML_ERROR))
                    .setContent(this.source);
        }
    }


    /**
     * 通过错误内容 从流程定义模型xml中找具体错误内容信息
     */
    private void  findErrorXmlContent(){
        String[] value = this.errorContent.processBuilderError().errorValue();
        if(!ObjectHelper.isEmpty(value)){
            StringReader reader = new StringReader(this.source);
            BufferedReader buf = new BufferedReader(reader);
            int numberLine = 0;
            while(true){
                try {
                    String line = buf.readLine();
                    numberLine++;
                    if(line == null){
                        break;
                    }
                    boolean exists = true;
                    for(String v : value){
                        exists = line.contains(v);
                    }
                    if(exists){
                        this.errorContent.processBuilderError().errorLine(numberLine)
                                .errorContent(line);
                        break;
                    }
                } catch (IOException e) {
                    logger.error(e.getMessage(),e);
                    WindLocalError.get()
                            .errorMsg("build process fail,because : "+e.getMessage())
                            .code(IO_ERROR);
                    break;
                }

            }
        }
        this.errorContent.processBuilderError().content(this.source);
    }


    private void initProcess(Document document,String processId){
        if(document != null) {
            Element element = document.getDocumentElement();

            String processName = element.getAttribute(NAME);
            String processDisplayName = element.getAttribute(DISPLAY_NAME);
            this.processModel = new ProcessModel(processName, processDisplayName, processId);
            processModel.setElement(element);
            processModel.setNow(element);

            NodeList nodeList = element.getChildNodes();
            Element startElement = null;
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node instanceof Element) {
                    String nodeName = node.getNodeName();
                    if (nodeName.equals(START)) {
                        startElement = (Element) node;
                    }
                }
            }
            checkStartNode(startElement);
            if(startElement != null) {
                StartNode startNode = initProcessStart(startElement, element);
                //设置流程子节点
                processModel.setChildNode(startNode);
            }
        }
    }

    /**
     * 构建流程定义开始节点
     * @param node      开始节点原始
     * @param element   流程定义所有元素
     */
    private StartNode initProcessStart(Element node,Element element){
        //构建开始节点
        String nodeId =  node.getAttribute(NAME);
        String displayName =  node.getAttribute(DISPLAY_NAME);
        StartNode startNode = new StartNode(nodeId, displayName);

        getInstance().put(nodeId,startNode);

        startNode.setElement(element);
        startNode.setNow(node);
        startNode.setParentNode(processModel);
        startNode.build();
        return startNode;
    }

    private void  endOperate(){
        //删除构建缓存
        clearCacheData();
        //打印错误信息
        printErrorMsg();
        //校验
        checkBuildErrorLocal();
    }


    private void  checkStartNode(Element startElement){
        if(startElement == null){
            this.processModel = null;
            this.success = false;
            if(logger.isDebugEnabled()){
                logger.debug(MessageHelper.getMsg(XML_NO_START));
                logger.debug(this.source);
            }
            WindError.error(XML_NO_START,null);
        }
    }

    private void checkBuildErrorLocal(){
        if(this.errorContent != null){
            this.success = false;
            this.processModel = null;
            throw new WorkflowException(errorContent.errorMsg());
        }
        if(WindLocalError.get().error()){
            this.processModel = null;
            this.success = false;
            this.errorContent =  WindLocalError.get();
            throw new WorkflowException(errorContent.errorMsg());
        }
    }

    private void printErrorMsg(){
        if(this.errorContent != null) {
            if (logger.isDebugEnabled()) {
                logger.debug("error xml content : "+this.errorContent.processBuilderError().content());
                logger.debug("error message: "+this.errorContent.errorMsg());
            }
        }
    }


    private void clearCacheData(){
        getInstance().remove();
        AndCache.getInstance().remove();
        OrCache.getInstance().remove();
        DynaCache.getInstance().remove();
    }

    public ErrorContext getErrorContent() {
        return errorContent;
    }

    public ProcessModel getProcessModel() {
        return processModel;
    }

    public boolean isSuccess() {
        return success;
    }
}
