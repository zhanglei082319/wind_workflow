package com.bcx.wind.workflow.parser;

import java.io.InputStream;

/**
 * 工作流定义构建工厂
 *
 * @author zhanglei
 */
public class ProcessBuilderFactory {

    public static ProcessBuilder getBuilder(String xml){
        return new ProcessBuilder(xml);
    }

    public static ProcessBuilder getBuilder(InputStream stream){
        return new ProcessBuilder(stream);
    }

    public static ProcessBuilder getBuilder(byte[] bytes){
        return new ProcessBuilder(bytes);
    }
}
