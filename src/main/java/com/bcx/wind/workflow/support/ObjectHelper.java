package com.bcx.wind.workflow.support;

import com.bcx.wind.workflow.core.constant.Logic;
import com.bcx.wind.workflow.core.flow.NodeModel;
import com.bcx.wind.workflow.core.flow.TaskModel;
import com.bcx.wind.workflow.core.script.FlowScriptEngine;
import com.bcx.wind.workflow.entity.WindEndHistory;
import com.bcx.wind.workflow.entity.WindProcessConfig;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.exception.WorkflowException;
import com.bcx.wind.workflow.imp.Configuration;
import com.bcx.wind.workflow.pojo.Condition;
import com.bcx.wind.workflow.pojo.WindConfig;
import sun.security.krb5.Config;

import javax.script.ScriptEngine;
import javax.script.ScriptException;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.bcx.wind.workflow.core.constant.Logic.AND;
import static com.bcx.wind.workflow.core.constant.Logic.OR;

/**
 * @author zhanglei
 */
public final class ObjectHelper {

    private static final Pattern NUMBER = Pattern.compile("^(-)?\\d+(\\.\\d+)?$");

    private ObjectHelper(){}

    public static String getValue(String key,Map args){
        return isEmpty(args.get(key)) ? "" : args.get(key).toString();
    }

    public static boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        } else if (obj instanceof Optional) {
            return !((Optional)obj).isPresent();
        } else if (obj instanceof CharSequence) {
            return ((CharSequence)obj).length() == 0;
        } else if (obj.getClass().isArray()) {
            return Array.getLength(obj) == 0;
        } else if (obj instanceof Collection) {
            return ((Collection)obj).isEmpty();
        } else {
            return obj instanceof Map && ((Map) obj).isEmpty();
        }
    }

    public static boolean hasEmpty(Object...objects){
        if(isEmpty(objects)){
            return true;
        }
        for(Object obj : objects){
            if(isEmpty(obj)){
                return true;
            }
        }
        return false;
    }


    public static boolean allEmpty(Object...objects){
        if(isEmpty(objects)){
            return true;
        }

        for(Object obj : objects){
            if(!isEmpty(obj)){
                return false;
            }
        }

        return true;
    }


    public static String primaryKey(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }

    public static Object getValue(Map<String,Object> args,String key){
        if(isEmpty(args)){
            return "";
        }

        return args.get(key);
    }


    /**
     * 判断是否是数字，支持负数，小数点等,所以不能完全判断是否纯数字
     *
     * @param str 判断字符串
     * @return boolean
     */
    private static boolean isNumeric(String str) {
        return NUMBER.matcher(str).matches();
    }



    /**
     * 判断名称中是否包含指定字符
     *
     * @param relName    名称
     * @param value      指定字符
     * @return            boolean
     */
    public static boolean hasValue(String relName,String value){
        return relName.equals(value) || relName.contains(value);
    }



    /**
     * 通过条件校验 和  业务数据进行公式比较  返回boolean
     *
     * @param con           配置的匹配规则
     * @param dataMap       业务数据
     * @return               布尔  公式成立true 不成立false
     */
    @SuppressWarnings("unchecked")
    public static boolean checkValue(Condition con, Map<String,Object> dataMap){
        //条件逻辑运算符  并且或者
        Logic logic  = con.getLogic();
        logic = ObjectHelper.isEmpty(logic) ? OR : logic;
        //条件校验中的键，对应业务数据中的属性
        String key = con.getKey();
        //比较运算符
        String condition = con.getCondition().name();
        //条件校验值
        Object value = con.getValue();
        //业务数据值
        String dataValue = getDataValue(dataMap,key);
        //如果配置校验数据为数组
        if(value instanceof List){
            List<String> compareValues = (List)value;
            return checkListValues(compareValues,dataValue,condition,logic);
        }else {
            String compareStrValue = value.toString();
            String[] compareStrValues = compareStrValue.split(",");
            if(compareStrValues.length==1) {
                return  checkValue(dataValue, condition, compareStrValue);
            }
            return checkListValues(Arrays.asList(compareStrValues),dataValue,condition,OR);
        }
    }


    private static  boolean  checkListValues(List<String> compareValues,String dataValue,String condition,Logic logic){
        return  checkValue(compareValues,dataValue,condition,logic);
    }


    /**
     * 校验数组元素包含
     * @param compareValues 配置数据数组
     * @param dataValue 业务数据
     * @param condition  比较符号
     * @param logic       并且  或者
     * @return boolean
     */
    private static boolean checkValue(List<String> compareValues,String dataValue,String condition,Logic logic){
        boolean result = false;
        for(String value : compareValues){
            result = checkValue(dataValue,condition,value);
            //或者运算符使用
            if(result && OR.equals(logic)) {
                return true;
            }else if(!result && AND.equals(logic)){
                return false;
            }
        }
        return result;
    }



    private static String getDataValue(Map<String,Object> data,String key){
        String dataValue = getValue(key,data);
        if(ObjectHelper.isEmpty(dataValue)){
            dataValue =  ObjectHelper.getValue(JsonHelper.toHump(key),data);
            if(ObjectHelper.isEmpty(dataValue)){
                dataValue = ObjectHelper.getValue(JsonHelper.toUnderLineField(key),data);
                if(ObjectHelper.isEmpty(dataValue)){
                    dataValue = "";
                }
            }
        }
        return dataValue;
    }



    /**
     * 比较
     *
     * @param conditionValue   比较值
     * @param condition         自定义比较运算符
     * @param dataValue         被比较值
     * @return                   boolean
     */
    private static boolean checkValue(String conditionValue,String condition,String dataValue){
        if(ObjectHelper.hasEmpty(condition,conditionValue,dataValue)){
            return false;
        }
        BigDecimal v1 = new BigDecimal(0);
        BigDecimal v2 = new BigDecimal(0);
        Pattern pat;
        Matcher mat;
        if(isNumeric(conditionValue) || isNumeric(dataValue)) {
            v1 = new BigDecimal(conditionValue);
            v2 = new BigDecimal(dataValue);
        }

        switch (condition){
            //字符串等于
            case "eq":
                return conditionValue.equals(dataValue);
            //字符串 !=
            case "ueq":
                return !conditionValue.equals(dataValue);
            //like 相似
            case "lk":
                return conditionValue.contains(dataValue);
            //not like
            case "ulk":
                return !conditionValue.contains(dataValue);
            //被包含
            case "in":
                return dataValue.contains(conditionValue);
            //不被包含
            case "uin":
                return !dataValue.contains(conditionValue);
            //conditionValue start with val3
            case "st":
                return dataValue.startsWith(conditionValue);
            //conditionValue end with val3
            case "ed":
                return dataValue.endsWith(conditionValue);
            case "rg":
                pat = Pattern.compile(dataValue);
                mat = pat.matcher(conditionValue);
                return mat.matches();
            //dataValue 是主题match
            case "irg":
                pat = Pattern.compile(conditionValue);
                mat = pat.matcher(dataValue);
                return mat.matches();
            //数字
            //=
            case "neq":
                return v1.compareTo(v2) == 0;
            //!=
            case "nueq":
                return v1.compareTo(v2) != 0;
            // <
            case "nlt":
                return v1.compareTo(v2) < 0;
            // <=
            case "nelt":
                return v1.compareTo(v2) < 1;
            // >
            case "ngt":
                return v1.compareTo(v2) > 0;
            // >=
            case "negt":
                return v1.compareTo(v2) > -1;
            default:
                return false;
        }

    }


    /**
     * 获取节点配置数据
     * @param configList     流程所有配置
     * @param nodeId         节点ID
     * @return                任务节点配置数据
     */
    public static WindProcessConfig getNodeConfig(List<WindProcessConfig> configList, String nodeId, Map<String,Object> dataMap){
        if(!ObjectHelper.isEmpty(configList)){

            List<WindProcessConfig> configs = configList.stream().filter(config-> nodeId.equals(config.getNodeId()))
                    .collect(Collectors.toList());

            if(!ObjectHelper.isEmpty(configs) && dataMap != null){
                configs = configs.stream().filter(config->{
                    //首先匹配脚本
                    String script = config.getScript();
                    if(!ObjectHelper.isEmpty(script)){
                        ScriptEngine engine = Configuration.windContext().get(ScriptEngine.class);
                        engine = engine == null ? FlowScriptEngine.getEngine() : engine;
                        putDataToScriptEngine(dataMap,engine);
                        try {
                            Object bool = engine.eval(script);
                            if(bool instanceof Boolean){
                                return (boolean) bool;
                            }
                        } catch (ScriptException e) {
                            WindError.error(e.getMessage());
                        }
                    }

                    String logic = ObjectHelper.isEmpty(config.getLogic()) ? AND.name(): config.getLogic();
                    List<Condition> conditions = config.conditions();
                    boolean result = true;
                    for(Condition condition : conditions){
                        result  =  ObjectHelper.checkValue(condition,dataMap);
                        if(!result && logic.equals(AND.name())){
                            return false;
                        }else if(result && logic.equals(OR.name())){
                            return true;
                        }
                    }
                    if(conditions.size()==1 && conditions.get(0) == null){
                        return true;
                    }
                    return result || ObjectHelper.isEmpty(conditions);
                }).collect(Collectors.toList());

                if(!ObjectHelper.isEmpty(configs)){
                    return  configs.stream().max(Comparator.comparingInt(WindProcessConfig::getSort))
                            .orElseThrow(()->new WorkflowException("processConfig property sort is null!"));
                }
            }
            if(!ObjectHelper.isEmpty(configs)){
                return  configs.stream().max(Comparator.comparingInt(WindProcessConfig::getSort))
                        .orElseThrow(()->new WorkflowException("processConfig property sort is null!"));
            }
        }
        return null;
    }


    private static void putDataToScriptEngine(Map<String,Object> dataMap,ScriptEngine engine){
        for(Map.Entry<String,Object> data : dataMap.entrySet()){
            engine.put(data.getKey(),data.getValue());
        }
    }


    /**
     * 通过多个键迭代删除map数据
     *
     * @param variable  需要删除的map
     * @param keys      键数组
     */
    public static void removeVariable(Map<String,Object> variable,String... keys) {
        if (keys == null || keys.length == 0) {
            return;
        }
        String key = keys[0];

        if (keys.length == 1) {
            variable.remove(key);
            return;
        }
        Object value = variable.get(key);
        if(value != null) {
            Map<String, Object> childVariable = JsonHelper.objectToMap(value);
            //过滤第一个键
            String[] newKeys = new String[keys.length - 1];
            System.arraycopy(keys, 1, newKeys, 0, newKeys.length);

            //递归删除
            removeVariable(childVariable, newKeys);
            variable.put(key, childVariable);
        }
    }


    /**
     * 在map中迭代设置值
     *
     * @param variable   map数据
     * @param v           值
     * @param keys       迭代键
     */
    public static void setVariable(Map<String,Object> variable ,Object v,String ...keys){
        if (keys == null || keys.length == 0) {
            return;
        }
        String key = keys[0];

        if (keys.length == 1) {
            variable.put(key,v);
            return;
        }
        Object value = variable.get(key);
        if(value != null) {
            Map<String, Object> childVariable = JsonHelper.objectToMap(value);
            //过滤第一个键
            String[] newKeys = new String[keys.length - 1];
            System.arraycopy(keys, 1, newKeys, 0, newKeys.length);
            setVariable(childVariable, v, newKeys);
            variable.put(key, childVariable);
        }
    }

    public static void main(String[] args) {
        Map<String,Object> variable = new HashMap<>();
        Map<String,Map<String,Object>> vari = new HashMap<>();
        Map<String,Object> var = new HashMap<>();
        var.put("abc","123");
        var.put("bcd","345");
        vari.put("key",var);
        vari.put("key1",var);
        variable.put("variable",vari);
        setVariable(variable,"12342","variable","key","bcd");
        removeVariable(variable,"variable","key","abc");
        System.out.println();
    }

}
