package com.bcx.wind.workflow.message;

/**
 * 失败信息code
 *
 * @author zhanglei
 */
public class ErrorCode {

    /**流程定义二进制数据错误，构建失败**/
    public static final int PROCESS_ERROR =  101;

    /**流程定义数据为空！**/
    public static final int XML_EMPTY =  102;

    /**流程定义中缺少开始节点**/
    public static final int XML_NO_START = 103;

    /**流程定义xml数据错误，请确认是否符合流程定义规范！**/
    public static final int XML_ERROR = 104;

    /**文件处理失败**/
    public static final int IO_ERROR = 105;

    /**路线：${0}无法找到后续节点！**/
    public static final int XML_PATH_TO_NULL = 106;

    /**路线：${0}不可指向自身节点！**/
    public static final int XML_PATH_LOOP = 107;

    /**构建流程时，审批人数据：${0}无法找到！**/
    public static final int  XML_ASSIGNE_NOT_FOUND = 108;

    /**分支和聚合不匹配！错误分支${0}**/
    public static final int  XML_FORK_JOIN_ERROR = 109;

    /**查询条件为空**/
    public static final int  QUERY_FILTER_NULL = 110;

    /**流程定义【${0}】查询为空**/
    public static final int  PROCESS_QUERY_NULL = 111;

    /**发布状态或模板状态的流程定义不可删除**/
    public static final int  PROCESS_REMOVE_STATUS_ERROR = 112;

    /**只有模板状态的流程定义可发布**/
    public static final int  PROCESS_RELEASE_STATUS_ERROR = 113;

    /**数据库操作失败！请稍后**/
    public static final int  DATABASE_OPERATE_ERROR = 114;

    /**回收状态的流程定义不可回收**/
    public static final int PROCESS_RECOVERY_STATUS_ERROR = 115;

    /**流程定义配置数据不可为空**/
    public static final int PROCESS_CONFIG_IS_NOT_NULL = 116;

    /**更新${0}数据缺少主键**/
    public static final int UPDATE_OPERATE_LACK_PRIMARY = 117;

    /**${0}缺少${1}**/
    public static final int LACK_DATA = 118;

    /**${0}${1}数据错误**/
    public static final int DATA_ERROR = 119;

    /**${0}${1}查询为空**/
    public static final int QUERY_EMPTY = 120;

    /**未生效的流程定义不可配置**/
    public static final int ADD_CONFIG_NO_SUPPORT = 121;

    /**流程【${0}】中，不存在节点【${1}】**/
    public static final int NODE_NOT_FOUND = 122;

    /**${0}${1} 操作，缺少参数${2}**/
    public static final int OPERATE_FAIL = 123;

    /**任务创建失败，因为缺少${0}**/
    public static final int CREATE_TASK_FAIL = 124;

    /**更新任务失败，因为缺少${0}**/
    public static final int UPDATE_TASK_FAIL = 125;

    /**流程实例创建失败，因为缺少${0}**/
    public static final int CREATE_ORDER_FAIL = 126;

    /**更新流程实例失败，因为缺少${0}**/
    public static final int UPDATE_ORDER_FAIL = 127;

    /**历史流程实例创建失败，因为缺少${0}**/
    public static final int CREATE_HIST_ORDER_FAIL = 128;

    /**添加执行履历失败，因为缺少${0}**/
    public static final int CREATE_ACT_HIST_FAIL = 129;

    /**更新执行履历失败，因为缺少${0}**/
    public static final int UPDATE_ACT_HIST_FAIL = 130;

    /**删除执行履历失败，因为缺少${0}**/
    public static final int REMOVE_ACT_HIST_FAIL = 131;

    /**创建工作流失败，缺少参数${0}**/
    public static final int BUILD_FLOW_FAIL = 132;

    /**流程实例【${0}】不存在**/
    public static final int ORDER_NOT_EXISTS = 133;

    /**无法获取流程定义【${0}】的【${1}】配置，操作失败**/
    public static final int PROCESS_CONFIG_NOT_FOUND = 134;

    /**对不起，该流程下没有您的任务**/
    public static final int NOT_FOUND_TASK = 135;

    /**136=提交路线【${0}】无法获取下一步任务节点**/
    public static final int PATH_NO_FOUND_NODE = 136;

    /**流程实例${0}执行时，缺少审批人**/
    public static final int APPROVE_USER_NOT_FOUND = 137;

    /**提交工作流失败，缺少审批人**/
    public static final int SUBMIT_NOT_FOUND_APPROVEUSER = 138;

    /**提交工作流失败，缺少参数**/
    public static final int SUBMIT_NOT_FOUND_ARGS = 139;

    /**提交工作流失败，缺少参数${0}**/
    public static final int SUBMIT_FAIL = 140;

    /**流程【${0}】的节点【${1}】，配置项【${2}】错误！**/
    public static final int NODE_CONFIG_ERROR = 141;

    /**指定的提交或者退回节点${0}不合法！**/
    public static final int SUBMIT_NODE_NOT_FOUND = 142;

    /**任务节点【${0}】执行无法获取提交路线**/
    public static final int SUBMIT_LINE_NOT_FOUND = 143;

    /**无法找到退回节点，退回失败**/
    public static final int RETURN_NODE_NOT_FOUND = 144;

    /**退回工作流失败，缺少参数**/
    public static final int REJECT_NOT_FOUND_ARGS = 145;

    /**146=退回工作流事变，缺少参数${0}**/
    public static final int REJECT_FAIL = 146;

    /**任务节点${0}不可退回，退回失败**/
    public static final int TASK_NODE_REJECT_ERROR = 147;

    /**对不起，当前节点不可驳回**/
    public static final int TASK_CAN_NOT_REJECT = 148;

    /**完结工作流失败，缺少参数**/
    public static final int COMPLETE_OPERATE_NOT_FOUND_VARIABLE = 149;

    /**完结工作流失败，缺少参数${0}**/
    public static final int COMPLETE_FAIL = 150;

    /**会签，串签，分支任务不可完结**/
    public static final int COMPLETE_NOT_SUPPORT = 151;

    /**对不起，该流程已经执行完毕！**/
    public static final int ORDER_IS_COMPLETE = 152;

    /**撤回失败，缺少参数**/
    public static final int WITH_DRAW_NO_ARGS = -153;

    /**撤回失败，缺少参数${0}**/
    public static final int WITH_DRAW_FAIL = -154;

    /**会签，串签，分支任务不可撤回**/
    public static final int WITH_DRAW_NOT_SUPPORT = 151;

    /**撤销失败，缺少参数${0}**/
    public static final int REVOKE_FAIL = 156;

    /**撤销失败，缺少参数**/
    public static final int REVOKE_NO_ARGS = 157;

    /**撤销失败，无法获取历史操作记录**/
    public static final int REVOKE_NOT_FOUND_HIST = 158;

    /**该流程处于挂起状态，请恢复流程后再操作**/
    public static final int ORDER_IS_STOP_STATUS = 159;

    /**流程定义模块不存在**/
    public static final int PROCESS_MODULE_NOT_FOUND = 160;

    /**模块不可为空**/
    public static final int PROCESS_MODULE_IS_NULL = 161;

    /**父模块不存在**/
    public static final int PROCESS_PARENT_MODULE_NOT_FOUND = 162;

    /**模块名称不可重复**/
    public static final int PROCESS_MODULE_IS_SAME = 163;

    /**流程定义【${0}】尚有未执行完毕的流程，不可删除**/
    public static final int PROCESS_CANNOT_BE_DELETED = 164;

    /**根模块不可删除**/
    public static final int ROOT_MODULE_CANNOT_DELETED = 165;

    /**默认模块不可删除**/
    public static final int DEFAULT_MODULE_CANNOT_DELETED = 166;

    /**缺少用户信息**/
    public static final int USER_MSG_NOT_FOUND = 167;

    /**缺少转办人和被转办人信息**/
    public static final int LACK_TRANSFER_DATA = 168;

    /**任务ID为空**/
    public static final int TASK_ID_IS_NULL = 169;

    /**该任务具有流程唯一性，不可关闭**/
    public static final int TASK_IS_ONLY = 170;

    /**恢复失败，流程不存在或流程状态错误**/
    public static final int  HANG_UP_FAIL = 171;

    /**挂起事变，流程不存在或流程状态错误**/
    public static final int  RECOVERY_FAIL = 172;

    /**指定的加签任务不存在**/
    public static final int  ADD_APPROVE_USER_TASK_NOT_FOUND = 173;

    /**任务加签缺少审批人**/
    public static final int  ADD_APPROVE_USER_NOT_FOUND = 174;

    /**当前任务不支持此操作**/
    public static final int  TASK_NOT_SUPPORT_OPERATE = 175;

    /**加签审批人已经存在**/
    public static final int  ADD_APPROVE_USER_IS_EXIST = 176;

    /**主任务不可为空**/
    public static final int  MAIN_TASK_IS_NULL = 177;

    /**子任务操作人不可为空**/
    public static final int  CHILD_TASK_OPERATOR_IS_NULL = 178;

    /**对不起，您没有权限完成该任务**/
    public static final int  NO_AUTH_ON_CHILD_TASK = 179;

    /**任务操作人不可为空**/
    public static final int  TASK_OPERATOR_IS_NULL = 180;

    /**任务不存在审批人${0}**/
    public static final int  APPROVE_USER_NOT_EXIST = 181;
}
