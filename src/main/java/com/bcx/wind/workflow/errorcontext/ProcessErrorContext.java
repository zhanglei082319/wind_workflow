package com.bcx.wind.workflow.errorcontext;

/**
 * 流程定义构建错误信息
 *
 * @author zhanglei
 */
public interface ProcessErrorContext {

    /**
     * 错误流程定义内容
     *
     * @return  内容
     */
    String  content();

    /**
     * 设置流程定义内容
     *
     * @param content  内容
     * @return ProcessErrorContent  错误流程信息
     */
    ProcessErrorContext   content(String content);


    /**
     * 错误xml行数
     *
     * @return  行数
     */
    int   errorLine();


    /**
     * 设置错误行数
     *
     * @param errorLine  行数
     * @return ProcessErrorContent  错误流程信息
     */
    ProcessErrorContext  errorLine(int errorLine);


    /**
     * 错误的流程定义具体内容
     *
     * @return  具体错误内容
     */
    String  errorContent();

    /**
     * 设置具体错误内容
     *
     * @param errorContent 错误内容
     * @return ProcessErrorContent  错误流程信息
     */
    ProcessErrorContext   errorContent(String errorContent);


    /**
     * 错误流程定义 转化成html格式
     *
     * @return  html形式的错误xml
     */
    String  errorHtml();


    /**
     * 设置html错误内容
     *
     * @param errorHtml html形式的错误xml
     * @return ProcessErrorContent  错误流程信息
     */
    ProcessErrorContext   errorHtml(String errorHtml);


    /**
     * 获取错误代码xml信息
     *
     * @return  xml具体字符串
     */
    String[]  errorValue();


    /**
     * 设置错误字符串数组
     *
     * @param values  字符串数组
     * @return ProcessErrorContent  错误流程信息
     */
    ProcessErrorContext   errorValue(String[] values);

}
