package com.bcx.wind.workflow.errorcontext;

import com.bcx.wind.workflow.exception.WorkflowException;
import com.bcx.wind.workflow.message.MessageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 错误统一处理
 *
 * @author zhanglei
 */
public class WindError {

    private static Logger logger = LoggerFactory.getLogger(WindError.class);

    public static void  error(int code,Throwable throwable,String...args){
        String errorMsg = MessageHelper.getMsg(code,args);
        if(throwable == null){
            throwable = new WorkflowException(errorMsg);
        }
        WindLocalError.getLocal()
                .code(code)
                .errorMsg(errorMsg)
                .throwable(throwable);
        //打印堆栈
        logger.error(errorMsg,throwable);
        throw new WorkflowException(errorMsg);
    }


    public static RuntimeException error(String errorMsg){
        RuntimeException throwable = new WorkflowException(errorMsg);
        logger.info(errorMsg);
        WindLocalError.getLocal()
                .code(-1)
                .errorMsg(errorMsg)
                .throwable(throwable);
        //打印堆栈
        logger.error(errorMsg,throwable);
        throw throwable;
    }


}
