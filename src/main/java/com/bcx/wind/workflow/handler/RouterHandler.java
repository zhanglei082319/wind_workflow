package com.bcx.wind.workflow.handler;

import com.bcx.wind.workflow.core.constant.OrderStatus;
import com.bcx.wind.workflow.core.constant.OrderType;
import com.bcx.wind.workflow.core.flow.NodeModel;
import com.bcx.wind.workflow.core.flow.node.RouterNode;
import com.bcx.wind.workflow.entity.WindOrder;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.pojo.Task;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.TimeHelper;

import static com.bcx.wind.workflow.core.constant.Constant.JSON_NULL;

/**
 * 子流程执行
 *
 * @author zhanglei
 */
public class RouterHandler extends BaseHandler{


    public RouterHandler(CommandContext commandContext, NodeModel curNode, Task preTask) {
        super(commandContext, curNode, preTask);
    }

    @Override
    public void execute() {
        //创建子流程实例
        createChildOrder();
        NodeModel firstTask = ((RouterNode) this.curNode).getFirstTaskNode();
        new TaskHandler(this.commandContext,firstTask,this.preTask).execute();
    }


    private void  createChildOrder(){
        WindOrder childOrder = new WindOrder()
                .setId(ObjectHelper.primaryKey())
                .setProcessId(this.wind().getWindProcess().getId())
                .setStatus(OrderStatus.run.name())
                .setType(OrderType.child.name())
                .setCreateTime(TimeHelper.nowDate())
                .setCreateUser(user().getUserId())
                .setParentId(wind().getWindOrder().getId())
                .setVersion(1)
                .setVariable(JSON_NULL)
                .setBusinessId(wind().getBusinessId())
                .setSystem(wind().getSystem());
        this.commandContext.access().insertOrderInstance(childOrder);
    }
}
