package com.bcx.wind.workflow.handler;

import com.bcx.wind.workflow.core.constant.TaskLevel;
import com.bcx.wind.workflow.core.constant.TaskStatus;
import com.bcx.wind.workflow.core.constant.TaskType;
import com.bcx.wind.workflow.core.flow.NodeModel;
import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.pojo.ApproveUser;
import com.bcx.wind.workflow.pojo.Task;
import com.bcx.wind.workflow.pojo.WindUser;
import com.bcx.wind.workflow.support.Assert;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.TimeHelper;

import java.util.List;
import java.util.stream.Collectors;

import static com.bcx.wind.workflow.core.constant.Constant.JSON_NULL;
import static com.bcx.wind.workflow.message.ErrorCode.APPROVE_USER_NOT_FOUND;

/**
 * 订阅任务
 *
 * @author zhanglei
 */
public class ScribeTaskHandler extends BaseHandler {


    public ScribeTaskHandler(CommandContext commandContext, NodeModel curNode, Task preTask) {
        super(commandContext, curNode, preTask);
    }

    @Override
    public void execute() {
        //审批人
        boolean createTask = false;
        List<ApproveUser> users = wind().getApproveUsers();
        Assert.notEmptyError(APPROVE_USER_NOT_FOUND,users,wind().getWindOrder().getId());
        if(!ObjectHelper.isEmpty(users)){
            for(ApproveUser user : users){

                if(curNode.nodeId().equals(user.getNodeId())){
                    createTask = true;
                    List<WindUser> approveUsers = user.getUsers();
                    buildTask(approveUsers);
                }
            }
        }
        if(!createTask){
            WindError.error(APPROVE_USER_NOT_FOUND,null,wind().getWindOrder().getId());
        }
    }


    private void buildTask(List<WindUser> approveUser){
        //创建任务
        WindTask task = createTask();
        task = commandContext.runtimeService().createNewTask(task);
        //创建审批人
        List<String> actors = approveUser.stream().map(WindUser::getUserId).collect(Collectors.toList());
        commandContext.runtimeService().addTaskActor(task.getId(),actors);
        //添加到工作流实例中
        preTask.setWindTask(task);
    }


    /**
     * 构建任务数据
     * @return   任务实例
     */
    private WindTask createTask(){
        return  new WindTask()
                .setId(ObjectHelper.primaryKey())
                .setTaskName(curNode.nodeId())
                .setDisplayName(curNode.nodeName())
                .setTaskType(TaskType.scribe.name())
                .setCreateTime(TimeHelper.nowDate())
                .setApproveCount(0)
                .setStatus(TaskStatus.run.name())
                .setOrderId(wind().getWindOrder().getId())
                .setProcessId(wind().getWindProcess().getId())
                .setVariable(JSON_NULL)
                .setPosition(taskLink())
                .setCreateUser(user().proxyUserId())
                .setTaskLevel(TaskLevel.child.name());
    }

}
