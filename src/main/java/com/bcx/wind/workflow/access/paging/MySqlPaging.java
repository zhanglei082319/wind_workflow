package com.bcx.wind.workflow.access.paging;

import com.bcx.wind.workflow.access.WindPage;

/**
 * mysql分页操作
 * limit 10,9
 * 从第10条数据开始 拿出9条数据出来
 *
 * @author zhanglei
 */
public class MySqlPaging implements Paging{

    @Override
    public StringBuilder buildSql(StringBuilder sql, WindPage page) {

        return sql.append(" limit ")
                .append((pageNum(page)-1) * pageSize(page))
                .append(" , ")
                .append(pageSize(page));
    }

    @Override
    public String buildSql(String sql, WindPage page) {
        StringBuilder builder = new StringBuilder(sql);
        builder = buildSql(builder,page);
        return builder.toString();
    }
}
