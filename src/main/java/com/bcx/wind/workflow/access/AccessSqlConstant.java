package com.bcx.wind.workflow.access;

/**
 * 数据库 sql语句常量
 *
 * @author zhanglei
 */
public final class AccessSqlConstant {

    public static final int  QUERY_TYPE = 1;

    public static final int  COUNT_TYPE = 0;

    public static final String ID = " id ";

    public static final String PROCESS_NAME = " process_name " ;

    public static final String PROCESS_ID = "process_id ";

    public static final String STATUS = " status ";

    public static final String VERSION = " version ";

    public static final String PARENT_ID = " parent_id ";

    public static final String MODULE = " module ";

    public static final String CREATE_TIME = " create_time ";

    public static final String SYSTEM = " system ";

    public static final String ORDER_TYPE = " type ";

    public static final String ORDER_ID = " order_id ";

    public static final String TASK_ID = " task_id ";

    public static final String ACTOR_ID = " actor_id ";

    public static final String BUSINESS_ID = " business_id ";

    public static final String NODE_ID = " node_id ";

    public static final String TASK_NAME = " task_name ";

    public static final String ORDER_BY = " order by ";

    public static final String EXPIRE_TIME = " expire_time ";

    public static final String TASK_TYPE = " task_type ";

    public static final String TASK_LEVEL = "task_level";

    public static final String POSITION = " position ";

    public static final String CREATE_USER = " create_user ";

    public static final String VAR = " =? ";

    public static final String AND = " and ";

    public static final String GREATER_THAN = " > ";

    public static final String LESS_THAN = " < ";

    public static final String ASK_CHAR = " ? ";

    public static final String PLACE = " 1=1 ";

    public static final String SPACE = " ";

    public static final String BETWEEN = " between ? and ? ";

    public static final String IN = " in ";

    public static final String LEFT_BRACK = " ( ";

    public static final String RIGHT_BRACK = " ) ";

    public static final String COMMA = " , ";

    public static final String DESC = " desc ";

    public static final String ASC = " asc ";



    public static final String TASK_CREATE_TIME = "task_create_time";
    /**
     * 流程定义
     */
    public static final String PROCESS_DEFINITION_INSERT = " insert into wind_process_definition(id,process_name,display_name,status,version,parent_id,create_time,content,system,module) values(?,?,?,?,?,?,?,?,?,?);";

    public static final String PROCESS_DEFINITION_UPDATE = " update wind_process_definition set  process_name=? , display_name=? , status=? , version=? , parent_id=? , create_time=? , content=? , system=? , module=? where id=?";

    public static final String PROCESS_DEFINITION_DELETE = " delete from wind_process_definition where ";

    public static final String PROCESS_DEFINITION_SELECT = " select * from wind_process_definition where ";

    public static final String PROCESS_DEFINITION_COUNT = " select count(id) from wind_process_definition where";

    public static final String PROCESS_MAX_VERSION = " select max(version) from wind_process_definition where process_name=? ";

    public static final String PROCESS_MAX_VERSION_RELEASE = " select max(version) from wind_process_definition where process_name=? and status=1 ";


    /**
     * 流程定义模块
     */
    public static final String PROCESS_MODULE_INSERT = " insert into wind_process_module(id,module_name,parent_id,system)values(?,?,?,?); ";

    public static final String PROCESS_MODULE_UPDATE = " update wind_process_module set module_name=? , parent_id=? , system=?  where id=? ";

    public static final String PROCESS_MODULE_DELETE = " delete from wind_process_module where ";

    public static final String PROCESS_MODULE_SELECT = " select * from wind_process_module ";



    /**
     * 执行完毕履历
     */
    public static final String ACTIVE_HISTORY_INSERT = " insert into wind_act_hist(id,task_id,task_name,task_display_name,process_id,order_id,process_name,process_display_name,operate,suggest,approve_time,actor_id,actor_name,create_time,approve_user_variable,task_type,system,submit_user_variable,variable,task_create_user,task_level,task_parent_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

    public static final String ACTIVE_HISTORY_UPDATE = " update wind_act_hist set task_id=? , task_name=? , task_display_name=? , process_id=? , order_id=? , process_name=? , process_display_name=? , operate=? , suggest=? , approve_time=? , actor_id=? , actor_name=? , create_time=? , approve_user_variable=?,task_type=? , system=? , submit_user_variable=? , variable=? , task_create_user=? ,task_level=? , task_parent_id=?  where id=?";

    public static final String ACTIVE_HISTORY_DELETE = " delete from wind_act_hist where ";

    public static final String ACTIVE_HISTORY_SELECT = " select * from wind_act_hist where ";

    public static final String ACTIVE_HISTORY_COUNT = " select count(*) from wind_act_hist where ";


    /**
     * 执行完毕履历
     */
    public static final String COMPLETE_HISTORY_INSERT = " insert into wind_end_hist(id,task_id,task_name,task_display_name,process_id,order_id,process_name,process_display_name,operate,suggest,approve_time,actor_id,actor_name,create_time,approve_user_variable,task_type,system,submit_user_variable,variable,task_create_user,task_level,task_parent_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

    public static final String COMPLETE_HISTORY_INSERTS = " insert into wind_end_hist (id,task_id,task_name,task_display_name,process_id,order_id,process_name,process_display_name,operate,suggest,approve_time,actor_id,actor_name,create_time,approve_user_variable,task_type,system,submit_user_variable,variable,task_create_user,task_level,task_parent_id) values ";

    public static final String COMPLETE_HISTORY_UPDATE = " update wind_end_hist set task_id=? , task_name=? , task_display_name=? , process_id=? , order_id=? , process_name=? , process_display_name=? , operate=? , suggest=? , approve_time=? , actor_id=? , actor_name=? ,create_time=? , approve_user_variable=?,task_type=? , system=? , submit_user_variable=? ,variable=? , task_create_user=? ,task_level=? , task_parent_id=? where id=?";

    public static final String COMPLETE_HISTORY_DELETE = " delete from wind_end_hist where ";

    public static final String COMPLETE_HISTORY_SELECT = " select * from wind_end_hist where ";

    public static final String COMPLETE_HISTORY_COUNT = " select count(*) from wind_end_hist where ";


    /**
     * 流程实例
     */
    public static final String ORDER_INSTANCE_INSERT = " insert into wind_order(id,process_id,status,create_user,create_time,expire_time,parent_id,version,variable,data,system,finish_time,business_id,type) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";

    public static final String ORDER_INSTANCE_UPDATE = " update wind_order set process_id=? , status=? , create_user=?, create_time=?, expire_time=?, parent_id=?, version=version+1, variable=?, data=? , system=?,finish_time=?,business_id=? , type=? where id=? and version=?; ";

    public static final String ORDER_INSTANCE_DELETE = " delete from wind_order where ";

    public static final String ORDER_INSTANCE_SELECT = " select * from wind_order where ";

    public static final String ORDER_INSTANCE_COUNT = " select count(*) from wind_order where";




    /**
     *   流程实例历史
     */
    public static final String ORDER_HISTORY_INSERT = " insert into wind_hist_order(id,process_id,status,create_user,create_time,expire_time,parent_id,version,variable,data,system,finish_time,business_id,type) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";

    public static final String ORDER_HISTORY_UPDATE = " update wind_hist_order set process_id=? , status=? , create_user=?, create_time=?, expire_time=?, parent_id=?, version=?, variable=?, data=? , system=?,finish_time=?,business_id=? ,type=? where id=? ; ";

    public static final String ORDER_HISTORY_DELETE = " delete from wind_hist_order where ";

    public static final String ORDER_HISTORY_SELECT = " select * from wind_hist_order where ";

    public static final String ORDER_HISTORY_COUNT = " select count(*) from wind_hist_order where";


    /**
     *  流程配置
     */
    public static final String PROCESS_CONFIG_INSERT = " insert into wind_process_config(id,process_id,config_name,process_name,node_id,condition,node_config,approve_user,business_config,sort,level,create_time,logic,script) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";

    public static final String PROCESS_CONFIG_UPDATE = " update wind_process_config set process_id=? , config_name=? , process_name=? , node_id=? , condition=? , node_config=? , approve_user=? , business_config=?, sort=? , level=?, create_time=? , logic=? ,script=?  where id=?; ";

    public static final String PROCESS_CONFIG_DELETE = " delete from wind_process_config where ";

    public static final String PROCESS_CONFIG_SELECT = " select * from wind_process_config where ";

    public static final String PROCESS_CONFIG_UPDATE_SORT = "update wind_process_config set sort=sort+? where id in ( ";


    /**
     * 任务审批人
     */
    public static final String TASK_ACTOR_INSERT = " insert into wind_task_actor(task_id,actor_id)values(?,?); ";

    public static final String TASK_ACTOR_INSERT_LIST = "  insert into wind_task_actor(task_id,actor_id)values ";

    public static final String TASK_ACTOR_DELETE = " delete from wind_task_actor where ";

    public static final String TASK_ACTOR_SELECT = " select * from wind_task_actor where ";

    public static final String TASK_ACTOR_DELETE_ACTOR = " delete from wind_task_actor where task_id = ? and actor_id=? ";


    /**
     * 任务实例
     */
    public static final String TASK_INSTANCE_INSERT = " insert into wind_task(id,task_name,display_name,task_type,create_time,expire_time,approve_count,status,order_id,process_id,variable,parent_id,version,position,create_user,task_level)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";

    public static final String TASK_INSTANCE_UPDATE = " update wind_task set task_name=?, display_name=?,task_type=?,create_time=?,expire_time=?,approve_count=?,status=?,order_id=?,process_id=?,variable=?,parent_id=?,version=version+1 ,position=? ,create_user=? ,task_level=?  where id=? and version=?; ";

    public static final String TASK_INSTANCE_DELETE = " delete from wind_task where ";

    public static final String TASK_QUERY = " select * from wind_task where ";

    public static final String TASK_INSTANCE_SELECT = " select * from wind_task ";

    public static final String TASK_INSTANCE_COUNT = " select count(*) from wind_task ";



    /**
     * 撤销数据暂存
     */
    public static final String REVOKE_DATA_INSERT = "insert into wind_revoke_data(id,order_id,process_id,process_display_name,task_name,task_display_name," +
            "actor_id,operate,suggest,operate_time,wind_order,wind_task,wind_task_actor,create_time) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    public static final String REVOKE_DATA_UPDATE = "update wind_revoke_data set order_id=?, process_id=?, process_display_name=?, task_name=?, task_display_name=?, " +
            " actor_id=?, operate=?, suggest=?, operate_time, wind_order=?, wind_task=?, wind_task_actor=?, create_time=? where id=? ";

    public static final String REVOKE_DATA_DELETE = "delete from wind_revoke_data where ";

    public static final String REVOKE_DATA_SELECT = "select * from wind_revoke_data where ";



    public static final String  SELECT_TASK_INSTANCE = "select \n" +
            "process.id as process_id,\n" +
            "process.process_name as process_name,\n" +
            "process.display_name as process_display_name,\n" +
            "ord.id as order_id,\n" +
            "ord.business_id as business_id,\n" +
            "ord.create_user as order_create_user,\n" +
            "ord.data as order_data,\n" +
            "ord.create_time as order_create_time,\n" +
            "ord.expire_time as order_expire_time, \n" +
            "ord.variable as order_variable,\n" +
            "task.id as task_id,\n" +
            "task.task_name as task_name,\n" +
            "task.display_name as task_display_name,\n" +
            "task.create_time as task_create_time,\n" +
            "task.position as task_position,\n" +
            "task.task_type as task_type,\n" +
            "task.create_user as task_create_user,\n" +
            "task.expire_time as task_expire_time,\n" +
            "task.variable as task_variable,\n" +
            "actor.actor_id as task_actor\n" +
            "\n" +
            "from wind_process_definition process RIGHT JOIN wind_order ord  ON process.id = ord.process_id RIGHT JOIN wind_task task ON task.order_id = ord.id\n" +
            "RIGHT JOIN wind_task_actor actor ON actor.task_id = task.id\n" +
            "where ";

    public static final String  SELECT_TASK_INSTANCE_COUNT = "select \n" +
            "count(task.id)\n" +
            "from wind_process_definition process RIGHT JOIN wind_order ord  ON process.id = ord.process_id RIGHT JOIN wind_task task ON task.order_id = ord.id\n" +
            "RIGHT JOIN wind_task_actor actor ON actor.task_id = task.id\n" +
            "where ";
}
