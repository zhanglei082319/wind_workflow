package com.bcx.wind.workflow.access.paging;

import com.bcx.wind.workflow.access.WindPage;

/**
 * postgres数据库分页
 *
 * limit  5  offset  0
 * 从第0条数据开始往后，拿出5条数据
 *
 * @author zhanglei
 */
public class PostgresPaging implements Paging {


    @Override
    public StringBuilder buildSql(StringBuilder sql, WindPage page) {
        return sql.append(" limit ")
                .append(pageSize(page))
                .append(" offset ")
                .append(pageSize(page)*(pageNum(page)-1));
    }

    @Override
    public String buildSql(String sql, WindPage page) {
        StringBuilder builder = new StringBuilder(sql);
        builder = buildSql(builder,page);
        return builder.toString();
    }
}
