package com.bcx.wind.workflow.access;

import java.util.List;

/**
 * 工作流分页查询参数实体
 *
 * @author zhanglei
 */
public class WindPage<T> {

    /**
     * 每页数据量
     */
    private int pageSize = 10;

    /**
     * 页号
     */
    private int pageNum = 1;

    /**
     * 数据总数
     */
    private long count;

    /**
     * 数据集合
     */
    private List<T> list;

    public WindPage(){

    }

    public WindPage(int pageSize,int pageNum){
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
