package com.bcx.wind.workflow.access;

import com.bcx.wind.workflow.entity.*;
import com.bcx.wind.workflow.support.ObjectHelper;

import java.util.LinkedList;
import java.util.List;

import static com.bcx.wind.workflow.access.AccessSqlConstant.*;


/**
 * 持久层抽象实现类，主要实现sql语句的拼接
 *
 * @author zhanglei
 */
public abstract class AbstractAccess extends AbstractAccessSupport {

    /**
     * 添加更新保存数据
     *
     * @param sql    sql语句
     * @param args   参数
     * @return       更新结果
     */
    protected abstract int saveOrUpdate(String sql,Object[] args);


    /**
     * 查询数据，由最终持久层完善
     *
     * @param sql    sql语句
     * @param args   参数
     * @param clazz  类
     * @param <T>           类型
     * @return       查询
     */
    protected abstract <T>List<T> query(String sql, Object[] args,Class<T> clazz);


    /**
     * 查询单条数据
     *
     * @param sql       sql语句
     * @param args      参数
     * @param clazz     类型
     * @param <T>       返回类型
     * @return          查询结果
     */
    protected abstract <T>T   queryOne(String sql,Object[] args,Class<T> clazz);


    /**
     * 查询指定数据指定字段值
     *
     * @param sql   sql语句
     * @param args  参数
     * @return      属性值
     */
    protected abstract Object  queryObject(String sql,Object[] args);


    /**
     * 查询数量
     *
     * @param sql   sql语句
     * @param args  参数
     * @return      总数
     */
    public abstract long  count(String sql,Object[] args);


    @Override
    public int queryMaxVersionProcess(String processName) {
        Object[] args = new Object[]{processName};
        Object ret =  queryObject(PROCESS_MAX_VERSION,args);
        return ret == null ? -1 : (int) ret;
    }

    @Override
    public int addProcess(WindProcess process) {
        Object[] args = new Object[]{process.getId(),process.getProcessName(),process.getDisplayName(),process.getStatus(),
                process.getVersion(),process.getParentId(),process.getCreateTime(),process.getContent(),process.getSystem(),process.getModule()};
        return saveOrUpdate(PROCESS_DEFINITION_INSERT,args);
    }

    @Override
    public int updateProcess(WindProcess process) {
        Object[] args = new Object[]{process.getProcessName(),process.getDisplayName(),process.getStatus(),
                process.getVersion(),process.getParentId(),process.getCreateTime(),process.getContent(),process.getSystem(),process.getModule(),process.getId()};
        return saveOrUpdate(PROCESS_DEFINITION_UPDATE,args);
    }

    @Override
    public int removeProcessById(String processId) {
        Object[] args = new Object[]{processId};
        String builder = PROCESS_DEFINITION_DELETE + ID + VAR;
        return saveOrUpdate(builder,args);
    }


    @Override
    public List<WindProcess> selectProcessList(QueryFilter filter, WindPage<WindProcess> page) {
        //结果sql
        StringBuilder retSql = new StringBuilder(PROCESS_DEFINITION_SELECT);
        List<Object> args = buildProcessQuery(retSql,filter,page,QUERY_TYPE);
        //数量sql
        StringBuilder totalSql = new StringBuilder(PROCESS_DEFINITION_COUNT);
        List<Object> totalArgs = buildProcessQuery(totalSql,filter,null,COUNT_TYPE);

        //结果
        List<WindProcess> processes = query(retSql.toString(),args.toArray(),WindProcess.class);
        //数量
        long total = count(totalSql.toString(),totalArgs.toArray());
        page.setCount(total);
        page.setList(processes);
        return processes;
    }


    @Override
    public List<WindProcess> selectProcessList(QueryFilter filter) {
        StringBuilder sql = new StringBuilder(PROCESS_DEFINITION_SELECT);
        List<Object> args = buildProcessQuery(sql,filter,null,QUERY_TYPE);
        return query(sql.toString(),args.toArray(),WindProcess.class);
    }

    @Override
    public WindProcess getProcessDefinitionById(String processId) {
        Object[] args = new Object[]{processId};
        String builder = PROCESS_DEFINITION_SELECT + ID + VAR;
        return queryOne(builder,args,WindProcess.class);
    }


    @Override
    public int insertProcessModule(WindProcessModule windProcessModule) {
        Object[] args = new Object[]{windProcessModule.getId(),windProcessModule.getModuleName(),windProcessModule.getParentId(),
        windProcessModule.getSystem()};
        return this.saveOrUpdate(PROCESS_MODULE_INSERT,args);
    }

    @Override
    public int updateProcessModule(WindProcessModule windProcessModule) {
        Object[] args = new Object[]{windProcessModule.getModuleName(),windProcessModule.getParentId(),
                windProcessModule.getSystem(),windProcessModule.getId()};
        return this.saveOrUpdate(PROCESS_MODULE_UPDATE,args);
    }

    @Override
    public int deleteProcessModule(String id) {
        String sql = PROCESS_MODULE_DELETE + " id = ? ";
        Object[] args = new Object[]{id};
        return saveOrUpdate(sql,args);
    }

    @Override
    public List<WindProcessModule> selectProcessModule(String system) {
        String sql = PROCESS_MODULE_SELECT + " where system = ? ";
        return query(sql,new String[]{system},WindProcessModule.class);
    }

    @Override
    public List<WindProcessModule> selectProcessModule() {
        return query(PROCESS_MODULE_SELECT,new String[]{},WindProcessModule.class);
    }

    @Override
    public List<WindProcessModule> selectProcessModule(String moduleName, String system, String parentId) {
        LinkedList<String> args = new LinkedList<>();
        args.addLast(moduleName);
        args.addLast(parentId);
        String sql = PROCESS_MODULE_SELECT + " where module_name=? and parent_id=? ";
        if(!ObjectHelper.isEmpty(system)){
            sql = sql +" and system=? ";
            args.addLast(system);
        }
        return query(sql,args.toArray(),WindProcessModule.class);
    }

    @Override
    public WindProcessModule selectProcessModuleById(String id) {
        String sql = PROCESS_MODULE_SELECT + " where id = ? ";
        return queryOne(sql,new String[]{id},WindProcessModule.class);
    }


    @Override
    public List<WindProcessModule> selectProcessModuleByParentId(String parentId) {
        String sql = PROCESS_MODULE_SELECT + " where parent_id = ? ";
        return query(sql,new String[]{parentId},WindProcessModule.class);
    }


    @Override
    public int insertActiveHistory(WindActHistory activeHistory) {
        Object[] args = new Object[]{activeHistory.getId(),activeHistory.getTaskId(),activeHistory.getTaskName(),
                    activeHistory.getTaskDisplayName(),activeHistory.getProcessId(),activeHistory.getOrderId(),activeHistory.getProcessName(),
                    activeHistory.getProcessDisplayName(),activeHistory.getOperate(),activeHistory.getSuggest(),activeHistory.getApproveTime(),
                    activeHistory.getActorId(),activeHistory.getActorName(),activeHistory.getCreateTime(),activeHistory.getApproveUserVariable(),activeHistory.getTaskType(),
                    activeHistory.getSystem(),activeHistory.getSubmitUserVariable(),activeHistory.getVariable(),activeHistory.getTaskCreateUser(),
                    activeHistory.getTaskLevel(),activeHistory.getTaskParentId()};
        return saveOrUpdate(ACTIVE_HISTORY_INSERT,args);
    }

    @Override
    public int removeActiveHistoryById(String id) {
        Object[] args = new Object[]{id};
        String sql = ACTIVE_HISTORY_DELETE + ID + VAR;
        return saveOrUpdate(sql,args);
    }

    @Override
    public int removeActiveHistoryByIds(List<String> ids) {
        LinkedList<Object> args = new LinkedList<>();
        StringBuilder sql = new StringBuilder(ACTIVE_HISTORY_DELETE);
        buildIdsSql(args,sql,ids,ID);
        return saveOrUpdate(sql.toString(),args.toArray());
    }

    @Override
    public int removeActiveHistoryByOrderId(String orderId) {
        String sql = ACTIVE_HISTORY_DELETE + ORDER_ID + VAR;
        Object[] args = new Object[]{orderId};
        return saveOrUpdate(sql,args);
    }

    @Override
    public int updateActiveHistory(WindActHistory activeHistory) {
        Object[] args = new Object[]{activeHistory.getTaskId(),activeHistory.getTaskName(),
                activeHistory.getTaskDisplayName(),activeHistory.getProcessId(),activeHistory.getOrderId(),activeHistory.getProcessName(),
                activeHistory.getProcessDisplayName(),activeHistory.getOperate(),activeHistory.getSuggest(),activeHistory.getApproveTime(),
                activeHistory.getActorId(),activeHistory.getActorName(),activeHistory.getCreateTime(),activeHistory.getApproveUserVariable(),activeHistory.getTaskType(),
                activeHistory.getSystem(),activeHistory.getSubmitUserVariable(),activeHistory.getVariable(),activeHistory.getTaskCreateUser()
                ,activeHistory.getTaskLevel(),activeHistory.getTaskParentId(),activeHistory.getId()};
        return saveOrUpdate(ACTIVE_HISTORY_UPDATE,args);
    }

    @Override
    public List<WindActHistory> selectActiveHistoryList(QueryFilter filter) {
        StringBuilder sql = new StringBuilder(ACTIVE_HISTORY_SELECT);
        List<Object> args = buildHistoryQuery(sql,filter,null,QUERY_TYPE);
        return query(sql.toString(),args.toArray(),WindActHistory.class);
    }

    @Override
    public List<WindActHistory> selectActiveHistoryList(QueryFilter filter, WindPage<WindActHistory> windPage) {
        //查询结果sql
        StringBuilder resultSql = new StringBuilder(ACTIVE_HISTORY_SELECT);
        List<Object> resultArgs = buildHistoryQuery(resultSql,filter,windPage,QUERY_TYPE);
        List<WindActHistory> result = query(resultSql.toString(),resultArgs.toArray(),WindActHistory.class);

        //查询数量sql
        StringBuilder countSql = new StringBuilder(ACTIVE_HISTORY_COUNT);
        List<Object> totalArgs = buildHistoryQuery(countSql,filter,null,COUNT_TYPE);
        long total = count(countSql.toString(),totalArgs.toArray());

        windPage.setCount(total);
        windPage.setList(result);
        return result;
    }

    @Override
    public WindActHistory getActiveHistoryById(String id) {
        String sql = ACTIVE_HISTORY_SELECT + ID + VAR;
        Object[] args = new Object[]{id};
        return queryOne(sql,args,WindActHistory.class);
    }



    @Override
    public int insertCompleteHistory(WindEndHistory completeHistory) {
        Object[] args = new Object[]{completeHistory.getId(),completeHistory.getTaskId(),completeHistory.getTaskName(),
                completeHistory.getTaskDisplayName(),completeHistory.getProcessId(),completeHistory.getOrderId(),completeHistory.getProcessName(),
                completeHistory.getProcessDisplayName(),completeHistory.getOperate(),completeHistory.getSuggest(),completeHistory.getApproveTime(),
                completeHistory.getActorId(),completeHistory.getActorName(),completeHistory.getCreateTime(),completeHistory.getApproveUserVariable(),completeHistory.getTaskType(),
                completeHistory.getSystem(),completeHistory.getSubmitUserVariable(),completeHistory.getVariable(),completeHistory.getTaskCreateUser(),
                completeHistory.getTaskLevel(),completeHistory.getTaskParentId()};
        return saveOrUpdate(COMPLETE_HISTORY_INSERT,args);
    }

    @Override
    public int insertCompleteHistoryList(List<WindActHistory> activeHistories) {
        StringBuilder sql = new StringBuilder(COMPLETE_HISTORY_INSERTS);
        List<Object> args = insertListCompleteHistory(activeHistories,sql);
        return saveOrUpdate(sql.toString(),args.toArray());
    }

    @Override
    public int removeCompleteHistoryById(String id) {
        String sql = COMPLETE_HISTORY_DELETE + ID + VAR;
        Object[] args = new Object[]{id};
        return saveOrUpdate(sql,args);
    }

    @Override
    public int removeCompleteHistoryIds(List<String> ids) {
        StringBuilder sql = new StringBuilder(COMPLETE_HISTORY_DELETE);
        LinkedList<Object> args = new LinkedList<>();
        buildIdsSql(args,sql,ids,ID);
        return saveOrUpdate(sql.toString(),args.toArray());
    }


    @Override
    public int updateCompleteHistory(WindEndHistory completeHistory) {
        Object[] args = new Object[]{completeHistory.getTaskId(),completeHistory.getTaskName(),
                completeHistory.getTaskDisplayName(),completeHistory.getProcessId(),completeHistory.getOrderId(),completeHistory.getProcessName(),
                completeHistory.getProcessDisplayName(),completeHistory.getOperate(),completeHistory.getSuggest(),completeHistory.getApproveTime(),
                completeHistory.getActorId(),completeHistory.getActorName(),completeHistory.getCreateTime(),completeHistory.getApproveUserVariable(),completeHistory.getTaskType(),
                completeHistory.getSystem(),completeHistory.getSubmitUserVariable(),completeHistory.getVariable(),completeHistory.getTaskCreateUser()
                ,completeHistory.getTaskLevel(),completeHistory.getTaskParentId(),completeHistory.getId()};
        return saveOrUpdate(COMPLETE_HISTORY_UPDATE,args);
    }


    @Override
    public List<WindEndHistory> selectCompleteHistoryList(QueryFilter filter) {
        StringBuilder sql = new StringBuilder(COMPLETE_HISTORY_SELECT);
        List<Object> args = buildHistoryQuery(sql,filter,null,QUERY_TYPE);
        return query(sql.toString(),args.toArray(),WindEndHistory.class);
    }


    @Override
    public List<WindEndHistory> selectCompleteHistoryList(QueryFilter filter, WindPage<WindEndHistory> windPage) {
        //查询结果sql
        StringBuilder resultSql = new StringBuilder(COMPLETE_HISTORY_SELECT);
        List<Object> resultArgs = buildHistoryQuery(resultSql,filter,windPage,QUERY_TYPE);
        List<WindEndHistory> result = query(resultSql.toString(),resultArgs.toArray(),WindEndHistory.class);

        //查询数量sql
        StringBuilder countSql = new StringBuilder(COMPLETE_HISTORY_COUNT);
        List<Object> totalArgs = buildHistoryQuery(countSql,filter,null,COUNT_TYPE);
        long total = count(countSql.toString(),totalArgs.toArray());

        windPage.setCount(total);
        windPage.setList(result);
        return result;
    }

    @Override
    public WindEndHistory getCompleteHistoryById(String id) {
        String sql = COMPLETE_HISTORY_SELECT + ID + VAR;
        Object[] args = new Object[]{id};
        return queryOne(sql,args,WindEndHistory.class);
    }


    @Override
    public int insertOrderInstance(WindOrder order) {
        Object[] args = new Object[]{order.getId(),order.getProcessId(),order.getStatus(),order.getCreateUser(),order.getCreateTime(),
                      order.getExpireTime(),order.getParentId(),order.getVersion(),order.getVariable(),order.getData(),order.getSystem(),
                      order.getFinishTime(),order.getBusinessId(),order.getType()};
        return saveOrUpdate(ORDER_INSTANCE_INSERT,args);
    }

    @Override
    public int updateOrderInstance(WindOrder order) {
        Object[] args = new Object[]{order.getProcessId(),order.getStatus(),order.getCreateUser(),order.getCreateTime(),
                order.getExpireTime(),order.getParentId(),order.getVariable(),order.getData(),order.getSystem(),
                order.getFinishTime(),order.getBusinessId(),order.getType(),order.getId(),order.getVersion()};
        return saveOrUpdate(ORDER_INSTANCE_UPDATE,args);
    }

    @Override
    public int removeByBusinessId(String businessId) {
        String sql = ORDER_INSTANCE_DELETE + BUSINESS_ID + VAR;
        Object[] args = new Object[]{businessId};
        return saveOrUpdate(sql,args);
    }

    @Override
    public int removeOrderInstanceById(String id) {
        String sql = ORDER_INSTANCE_DELETE + ID + VAR;
        Object[] args = new Object[]{id};
        return saveOrUpdate(sql,args);
    }

    @Override
    public int removeOrderInstanceByParentId(String parentOrderId) {
        String sql = ORDER_INSTANCE_DELETE + PARENT_ID + VAR;
        Object[] args = new Object[]{parentOrderId};
        return saveOrUpdate(sql,args);
    }

    @Override
    public WindOrder getOrderInstanceById(String id) {
        String sql = ORDER_INSTANCE_SELECT + ID + VAR;
        Object[] args = new Object[]{id};
        return queryOne(sql,args,WindOrder.class);
    }

    @Override
    public List<WindOrder> selectOrderInstanceList(QueryFilter filter) {
        StringBuilder sql = new StringBuilder(ORDER_INSTANCE_SELECT);
        List<Object> args = buildOrderQuery(sql,filter,null,QUERY_TYPE);
        return query(sql.toString(),args.toArray(),WindOrder.class);
    }

    @Override
    public List<WindOrder> selectOrderInstanceList(QueryFilter filter, WindPage<WindOrder> windPage) {
        //查询结果sql
        StringBuilder resultSql = new StringBuilder(ORDER_INSTANCE_SELECT);
        List<Object> resultArgs = buildOrderQuery(resultSql,filter,windPage,QUERY_TYPE);
        List<WindOrder> result = query(resultSql.toString(),resultArgs.toArray(),WindOrder.class);

        //查询数量sql
        StringBuilder totalSql = new StringBuilder(ORDER_INSTANCE_COUNT);
        List<Object> totalArgs = buildOrderQuery(totalSql,filter,null,COUNT_TYPE);
        long total = count(totalSql.toString(),totalArgs.toArray());

        windPage.setCount(total);
        windPage.setList(result);
        return result;
    }

    @Override
    public int insertWindHistOrder(WindHistOrder order) {
        Object[] args = new Object[]{order.getId(),order.getProcessId(),order.getStatus(),order.getCreateUser(),order.getCreateTime(),
                order.getExpireTime(),order.getParentId(),order.getVersion(),order.getVariable(),order.getData(),order.getSystem(),
                order.getFinishTime(),order.getBusinessId(),order.getType()};
        return saveOrUpdate(ORDER_HISTORY_INSERT,args);
    }

    @Override
    public int updateWindHistOrder(WindHistOrder order) {
        Object[] args = new Object[]{order.getProcessId(),order.getStatus(),order.getCreateUser(),order.getCreateTime(),
                order.getExpireTime(),order.getParentId(),order.getVersion(),order.getVariable(),order.getData(),order.getSystem(),
                order.getFinishTime(),order.getBusinessId(),order.getType(),order.getId()};
        return saveOrUpdate(ORDER_HISTORY_UPDATE,args);
    }

    @Override
    public int removeWindHistOrderById(String id) {
        String sql = ORDER_HISTORY_DELETE + ID + VAR;
        Object[] args = new Object[]{id};
        return saveOrUpdate(sql,args);
    }

    @Override
    public WindHistOrder getWindHistOrderById(String id) {
        String sql = ORDER_HISTORY_SELECT + ID + VAR;
        Object[] args = new Object[]{id};
        return queryOne(sql,args,WindHistOrder.class);
    }

    @Override
    public List<WindHistOrder> selectWindHistOrderList(QueryFilter filter) {
        StringBuilder sql = new StringBuilder(ORDER_HISTORY_SELECT);
        List<Object> args = buildOrderQuery(sql,filter,null,QUERY_TYPE);
        return query(sql.toString(),args.toArray(),WindHistOrder.class);
    }

    @Override
    public List<WindHistOrder> selectWindHistOrderList(QueryFilter filter, WindPage<WindHistOrder> windPage) {
        //查询结果sql
        StringBuilder resultSql = new StringBuilder(ORDER_HISTORY_SELECT);
        List<Object> resultArgs = buildOrderQuery(resultSql,filter,windPage,QUERY_TYPE);
        List<WindHistOrder> result = query(resultSql.toString(),resultArgs.toArray(),WindHistOrder.class);

        //查询数量sql
        StringBuilder totalSql = new StringBuilder(ORDER_HISTORY_COUNT);
        List<Object> totalArgs = buildOrderQuery(totalSql,filter,null,COUNT_TYPE);
        long total = count(totalSql.toString(),totalArgs.toArray());

        windPage.setCount(total);
        windPage.setList(result);
        return result;
    }

    @Override
    public int insertProcessConfig(WindProcessConfig processConfig) {
        Object[] args = new Object[]{processConfig.getId(),processConfig.getProcessId(),processConfig.getConfigName(),
                           processConfig.getProcessName(),processConfig.getNodeId(),processConfig.getCondition(),processConfig.getNodeConfig(),
                           processConfig.getApproveUser(),processConfig.getBusinessConfig(),processConfig.getSort(),processConfig.getLevel(),
                           processConfig.getCreateTime(),processConfig.getLogic(),processConfig.getScript()};
        return saveOrUpdate(PROCESS_CONFIG_INSERT,args);
    }

    @Override
    public int updateProcessConfig(WindProcessConfig processConfig) {
        Object[] args = new Object[]{processConfig.getProcessId(),processConfig.getConfigName(),
                processConfig.getProcessName(),processConfig.getNodeId(),processConfig.getCondition(),processConfig.getNodeConfig(),
                processConfig.getApproveUser(),processConfig.getBusinessConfig(),processConfig.getSort(),processConfig.getLevel(),
                processConfig.getCreateTime(),processConfig.getLogic(),processConfig.getScript(),processConfig.getId()};
        return saveOrUpdate(PROCESS_CONFIG_UPDATE,args);
    }

    @Override
    public int removeProcessConfigById(String id) {
        String sql = PROCESS_CONFIG_DELETE + ID + VAR;
        Object[] args = new Object[]{id};
        return saveOrUpdate(sql,args);
    }

    @Override
    public int removeProcessConfigByProcessId(String processId) {
        String sql = PROCESS_CONFIG_DELETE + PROCESS_ID + VAR;
        Object[] args = new Object[]{processId};
        return saveOrUpdate(sql,args);
    }

    @Override
    public WindProcessConfig getProcessConfigById(String id) {
        String sql = PROCESS_CONFIG_SELECT + ID + VAR;
        Object[] args = new Object[]{id};
        return queryOne(sql,args,WindProcessConfig.class);
    }

    @Override
    public int updateSort(List<String> ids, int add) {
        LinkedList<Object> args = new LinkedList<>();
        StringBuilder sql = new StringBuilder(PROCESS_CONFIG_UPDATE_SORT);

        args.addLast(add);
        for(int i=0 ; i<ids.size() ; i++){
            if(i < ids.size()-1){
                sql.append(ASK_CHAR)
                        .append(COMMA);
                args.addLast(ids.get(i));
                continue;
            }
            sql.append(ASK_CHAR).append(RIGHT_BRACK);
            args.addLast(ids.get(i));
        }

        return saveOrUpdate(sql.toString(),args.toArray());
    }

    @Override
    public List<WindProcessConfig> selectProcessConfigList(QueryFilter filter) {
        StringBuilder sql = new StringBuilder(PROCESS_CONFIG_SELECT);
        List<Object> args = buildConfigQuery(sql,filter,QUERY_TYPE);
        return query(sql.toString(),args.toArray(),WindProcessConfig.class);
    }

    @Override
    public int insertTaskActor(WindTaskActor taskActor) {
        Object[] args = new Object[]{taskActor.getTaskId(),taskActor.getActorId()};

        return saveOrUpdate(TASK_ACTOR_INSERT,args);
    }

    @Override
    public int insertTaskActor(List<WindTaskActor> taskActors) {
        StringBuilder sql = new StringBuilder(TASK_ACTOR_INSERT_LIST);
        List<Object> args = insertListActors(taskActors,sql);
        return saveOrUpdate(sql.toString(),args.toArray());
    }

    @Override
    public int removeTaskActorByTaskId(String taskId) {
        String sql = TASK_ACTOR_DELETE + TASK_ID + VAR;
        Object[] args = new Object[]{taskId};
        return saveOrUpdate(sql,args);
    }

    @Override
    public int removeTaskActorByTaskIds(List<String> taskIds) {
        StringBuilder sql = new StringBuilder(TASK_ACTOR_DELETE);
        LinkedList<Object> args = new LinkedList<>();
        buildIdsSql(args,sql,taskIds,TASK_ID);
        return saveOrUpdate(sql.toString(),args.toArray());
    }

    @Override
    public int removeTaskActor(String taskId, String taskActor) {
        Object[] args = new Object[]{taskId,taskActor};
        return saveOrUpdate(TASK_ACTOR_DELETE_ACTOR,args);
    }

    @Override
    public List<WindTaskActor> selectTaskActorList(QueryFilter filter) {
        StringBuilder sql = new StringBuilder(TASK_ACTOR_SELECT);
        List<Object> args = buildTaskActorQuery(sql,filter);
        return query(sql.toString(),args.toArray(),WindTaskActor.class);
    }

    @Override
    public int insertTaskInstance(WindTask task) {
        Object[] args = new Object[]{task.getId(),task.getTaskName(),task.getDisplayName(),task.getTaskType(),
                      task.getCreateTime(),task.getExpireTime(),task.getApproveCount(),task.getStatus(),task.getOrderId(),
                      task.getProcessId(),task.getVariable(),task.getParentId(),task.getVersion(),task.getPosition(),task.getCreateUser(),task.getTaskLevel()};
        return saveOrUpdate(TASK_INSTANCE_INSERT,args);
    }

    @Override
    public int updateTaskInstance(WindTask task) {
        Object[] args = new Object[]{task.getTaskName(),task.getDisplayName(),task.getTaskType(),
                task.getCreateTime(),task.getExpireTime(),task.getApproveCount(),task.getStatus(),task.getOrderId(),
                task.getProcessId(),task.getVariable(),task.getParentId(),task.getPosition(),task.getCreateUser(),task.getTaskLevel(),task.getId(),task.getVersion()};
        return saveOrUpdate(TASK_INSTANCE_UPDATE,args);
    }

    @Override
    public int removeTaskInstanceById(String id) {
        Object[] args = new Object[]{id};
        String sql = TASK_INSTANCE_DELETE + ID + VAR;
        return saveOrUpdate(sql,args);
    }

    @Override
    public int removeTaskInstanceByParentId(String parentId) {
        Object[] args = new Object[]{parentId};
        String sql = TASK_INSTANCE_DELETE + PARENT_ID + VAR;
        return saveOrUpdate(sql,args);
    }

    @Override
    public int removeTaskByOrderId(String orderId) {
        String sql = TASK_INSTANCE_DELETE + ORDER_ID + VAR;
        Object[] args = new Object[]{orderId};
        return saveOrUpdate(sql,args);
    }

    @Override
    public int removeTaskByTaskIds(List<String> taskIds) {
        StringBuilder sql = new StringBuilder(TASK_INSTANCE_DELETE);
        LinkedList<Object> args = new LinkedList<>();
        buildIdsSql(args,sql,taskIds,ID);
        return saveOrUpdate(sql.toString(),args.toArray());
    }

    @Override
    public WindTask getTaskInstanceById(String id) {
        String sql = TASK_QUERY + ID + VAR;
        Object[] args = new Object[]{id};
        return queryOne(sql,args,WindTask.class);
    }

    @Override
    public List<WindTask> selectTaskInstanceList(QueryFilter queryFilter) {
        StringBuilder sql = new StringBuilder(TASK_INSTANCE_SELECT);
        List<Object> args = buildTaskQuery(sql,queryFilter,null,QUERY_TYPE);
        return query(sql.toString(),args.toArray(),WindTask.class);
    }

    @Override
    public List<WindTask> selectTaskInstanceList(QueryFilter queryFilter, WindPage<WindTask> windPage) {
        //查询结果sql
        StringBuilder resultSql = new StringBuilder(TASK_INSTANCE_SELECT);
        List<Object> resultArgs = buildTaskQuery(resultSql,queryFilter,windPage,QUERY_TYPE);
        List<WindTask> result =  query(resultSql.toString(),resultArgs.toArray(),WindTask.class);

        //查询数量sql
        StringBuilder totalSql = new StringBuilder(TASK_INSTANCE_COUNT);
        List<Object> totalArgs = buildTaskQuery(totalSql,queryFilter,null,COUNT_TYPE);
        long total = count(totalSql.toString(),totalArgs.toArray());

        windPage.setCount(total);
        windPage.setList(result);
        return result;
    }

    @Override
    public int insertWindRevokeData(WindRevokeData revoke) {
        Object[] args = new Object[]{revoke.getId(),revoke.getOrderId(),revoke.getProcessId(),revoke.getProcessDisplayName(),
                  revoke.getTaskName(),revoke.getTaskDisplayName(),revoke.getActorId(),revoke.getOperate(),revoke.getSuggest(),
                  revoke.getOperateTime(),revoke.getWindOrder(),revoke.getWindTask(),revoke.getWindTaskActor(),revoke.getCreateTime()};
        return saveOrUpdate(REVOKE_DATA_INSERT,args);
    }

    @Override
    public int deleteWindRevokeDataByOrderId(String orderId) {
        String sql = REVOKE_DATA_DELETE + ORDER_ID + VAR;
        Object[] args = new Object[]{orderId};
        return saveOrUpdate(sql,args);
    }

    @Override
    public List<WindRevokeData> selectRevokeData(QueryFilter filter) {
        StringBuilder sql = new StringBuilder(REVOKE_DATA_SELECT);
        List<Object> args = buildRevokeQuery(sql,filter);
        return query(sql.toString(),args.toArray(),WindRevokeData.class);
    }

    @Override
    public List<WindRevokeData> selectRevokeByActor(String orderId, String actorId) {
        String sql = REVOKE_DATA_SELECT + ORDER_ID + VAR + AND + ACTOR_ID + VAR;
        Object[] args = new Object[]{orderId,actorId};
        return query(sql,args,WindRevokeData.class);
    }

    @Override
    public List<WindTaskInstance> selectTaskInstance(TaskFilter taskFilter) {
        StringBuilder sql = new StringBuilder(SELECT_TASK_INSTANCE);
        List<Object> args = buildQueryTaskInstanceSql(sql,taskFilter,null,1);
        return query(sql.toString(),args.toArray(),WindTaskInstance.class);
    }

    @Override
    public long selectTaskInstanceCount(TaskFilter filter) {
        //页数
        StringBuilder countSql = new StringBuilder(SELECT_TASK_INSTANCE_COUNT);
        List<Object> countArgs = buildQueryTaskInstanceSql(countSql,filter,null,0);
        return count(countSql.toString(),countArgs.toArray());
    }

    @Override
    public List<WindTaskInstance> selectTaskInstance(TaskFilter taskFilter, WindPage<WindTaskInstance> windPage) {
        StringBuilder sql = new StringBuilder(SELECT_TASK_INSTANCE);
        List<Object> args = buildQueryTaskInstanceSql(sql,taskFilter,windPage,1);
        List<WindTaskInstance> instances =  query(sql.toString(),args.toArray(),WindTaskInstance.class);

        //页数
        StringBuilder countSql = new StringBuilder(SELECT_TASK_INSTANCE_COUNT);
        List<Object> countArgs = buildQueryTaskInstanceSql(countSql,taskFilter,null,0);
        long count = count(countSql.toString(),countArgs.toArray());

        windPage.setCount(count);
        windPage.setList(instances);
        return instances;
    }
}
