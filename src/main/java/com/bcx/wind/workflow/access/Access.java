package com.bcx.wind.workflow.access;

import com.bcx.wind.workflow.entity.*;

import java.sql.Connection;
import java.util.List;

/**
 * 工作流持久层接口
 *
 * @author zhanglei
 */
public interface Access {

    /*--------------------流程定义-----------------*/


    /**
     * 通过流程定义名称查询最高版本
     *
     * @param processName  流程定义名称
     * @return             版本
     */
    int  queryMaxVersionProcess(String processName);

    /**
     * 新增流程定义
     * @param processDefinition  流程定义
     *
     * @return 添加结果
     */
    int  addProcess(WindProcess processDefinition);


    /**
     * 更新流程定义
     * @param processDefinition   流程定义
     *
     * @return 更新结果
     */
    int  updateProcess(WindProcess processDefinition);


    /**
     * 通过模型ID删除模型
     *
     * @param processId  模型ID
     * @return 删除结果
     */
    int  removeProcessById(String processId);


    /**
     * 查询流程定义
     * @param filter  过滤条件
     * @param page    分页查询
     * @return        流程定义集合
     */
    List<WindProcess>  selectProcessList(QueryFilter filter, WindPage<WindProcess> page);


    /**
     * 查询流程定义
     *
     * @param filter  过滤条件
     * @return        流程定义集合
     */
    List<WindProcess>  selectProcessList(QueryFilter filter);


    /**
     * 通过流程定义ID 查询流程定义
     *
     * @param processId  流程定义ID
     * @return           流程定义
     */
    WindProcess  getProcessDefinitionById(String processId);


    /*--------------------流程定义-----------------*/

    /*--------------------流程定义模块---------------*/

    /**
     * 新增流程定义模块
     *
     * @param windProcessModule  流程丁意思模块
     * @return  新增结果
     */
    int  insertProcessModule(WindProcessModule windProcessModule);


    /**
     * 更新流程定义模块
     *
     * @param windProcessModule  流程定义模块
     * @return  更新结果
     */
    int  updateProcessModule(WindProcessModule windProcessModule);

    /**
     * 删除流程定义模块
     *
     * @param id 模块ID
     * @return  删除结果
     */
    int  deleteProcessModule(String id);

    /**
     * 通过系统标识，查询该系统下的所有流程定义模块
     *
     *
     * @return   流程定义模块集合
     */
    List<WindProcessModule>  selectProcessModule(String system);

    /**
     * 查询所有的流程定义模块
     *
     * @return  所有流程定义模块
     */
    List<WindProcessModule>  selectProcessModule();


    /**
     * 查询同级下指定名称的所有模块
     *
     * @param moduleName  模块名称
     * @param system      系统标识
     * @param parentId    父模块ID
     * @return           结果
     */
    List<WindProcessModule>  selectProcessModule(String moduleName,String system,String parentId);


    /**
     * 通过ID 查询单个流程定义模块
     *
     * @param id  主键ID
     * @return   单个流程定义模块
     */
    WindProcessModule   selectProcessModuleById(String id);


    /**
     * 通过父模块ID 查询所有子模块集合
     *
     * @param parentId  父模块ID
     * @return   子模块集合
     */
    List<WindProcessModule> selectProcessModuleByParentId(String parentId);




    /*--------------------流程定义模块---------------*/



    /*--------------------正在执行履历-----------------*/


    /**
     * 新增正在执行履历
     *
     * @param activeHistory   正在执行履历
     * @return   新增结果
     */
    int  insertActiveHistory(WindActHistory activeHistory);


    /**
     * 通过id 删除正在执行履历
     *
     * @param id  主键ID
     * @return    删除结果
     */
    int  removeActiveHistoryById(String id);


    /**
     * 通过id数组删除执行履历
     *
     * @param ids id数组
     * @return    删除结果
     */
    int  removeActiveHistoryByIds(List<String> ids);


    /**
     * 通过流程实例号删除执行历史履历
     *
     * @param orderId  流程实例号
     * @return      删除结果
     */
    int  removeActiveHistoryByOrderId(String orderId);


    /**
     * 更新正在执行履历
     *
     * @param activeHistory  正在执行履历实体信息
     *
     * @return  更新结果
     */
    int  updateActiveHistory(WindActHistory activeHistory);


    /**
     * 查询正在执行履历
     *
     * @param filter   查询条件
     * @return         查询结果
     */
    List<WindActHistory>  selectActiveHistoryList(QueryFilter filter);


    /**
     * 分页查询正在执行履历
     *
     * @param filter    查询过滤条件
     * @param windPage  分页条件
     * @return          查询结果
     */
    List<WindActHistory>  selectActiveHistoryList(QueryFilter filter, WindPage<WindActHistory> windPage);


    /**
     * 通过ID主键 查询单条正在执行履历信息
     *
     * @param id  主键
     * @return    单条信息
     */
    WindActHistory  getActiveHistoryById(String id);


    /*--------------------正在执行履历-----------------*/



    /*--------------------执行完毕履历-----------------*/
    /**
     * 新增执行完毕履历
     *
     * @param completeHistory 执行完毕履历
     * @return   新增结果
     */
    int  insertCompleteHistory(WindEndHistory completeHistory);


    /**
     * 批量新增
     * @param activeHistories   执行履历
     * @return 新增结果
     */
    int  insertCompleteHistoryList(List<WindActHistory> activeHistories);


    /**
     * 通过id 删除执行完毕履历
     *
     * @param id  主键ID
     * @return    删除结果
     */
    int  removeCompleteHistoryById(String id);


    /**
     * 通过id数组删除完毕履历
     *
     * @param ids   ids
     * @return      删除结果
     */
    int  removeCompleteHistoryIds(List<String> ids);


    /**
     * 更新执行完毕履历
     *
     * @param completeHistory  执行完毕履历实体信息
     *
     * @return  更新结果
     */
    int  updateCompleteHistory(WindEndHistory completeHistory);


    /**
     * 查询执行完毕履历
     *
     * @param filter   查询条件
     * @return         查询结果
     */
    List<WindEndHistory>  selectCompleteHistoryList(QueryFilter filter);


    /**
     * 分页查询执行完毕履历
     *
     * @param filter    查询过滤条件
     * @param windPage  分页条件
     * @return          查询结果
     */
    List<WindEndHistory>  selectCompleteHistoryList(QueryFilter filter, WindPage<WindEndHistory> windPage);


    /**
     * 通过ID主键 查询单条执行完毕履历信息
     *
     * @param id  主键
     * @return    单条信息
     */
    WindEndHistory getCompleteHistoryById(String id);

    /*--------------------执行完毕履历-----------------*/


    /*--------------------流程执行实例-----------------*/

    /**
     * 新增流程执行实例
     *
     * @param orderInstance  流程执行实例实体
     * @return      新增结果
     */
    int insertOrderInstance(WindOrder orderInstance);


    /**
     * 更新流程执行实例
     *
     * @param orderInstance  流程执行实例
     * @return               更新结果
     */
    int updateOrderInstance(WindOrder orderInstance);


    /**
     * 通过业务ID 删除流程实例
     *
     * @param businessId  业务ID
     * @return             删除结果
     */
    int removeByBusinessId(String businessId);



    /**
     * 通过主键删除流程执行实例
     *
     * @param id  主键
     * @return    删除结果
     */
    int removeOrderInstanceById(String id);


    /**
     * 通过流程实例父Id删除流程实例
     * @param parentOrderId   父ID
     * @return   删除结果
     */
    int removeOrderInstanceByParentId(String parentOrderId);


    /**
     * 通过流程实例主键查询流程实例
     *
     * @param id  主键
     * @return    单条流程执行实例
     */
    WindOrder getOrderInstanceById(String id);



    /**
     * 通过条件查询流程执行实例
     *
     * @param filter  过滤查询条件
     * @return        流程执行实例集合
     */
    List<WindOrder>  selectOrderInstanceList(QueryFilter filter);


    /**
     * 分页查询流程执行实例
     *
     * @param filter     过滤条件
     * @param windPage   分页条件
     * @return           流程执行实例集合
     */
    List<WindOrder>  selectOrderInstanceList(QueryFilter filter, WindPage<WindOrder> windPage);


    /*--------------------流程执行实例-----------------*/



    /*--------------------流程历史执行实例-----------------*/

    /**
     * 新增流程执行实例
     *
     * @param windHistOrder  流程历史执行实例实体
     * @return      新增结果
     */
    int insertWindHistOrder(WindHistOrder windHistOrder);


    /**
     * 更新流程执行实例
     *
     * @param windHistOrder  流程历史执行实例实体
     * @return               更新结果
     */
    int updateWindHistOrder(WindHistOrder windHistOrder);


    /**
     * 通过主键删除流程执行实例
     *
     * @param id  主键
     * @return    删除结果
     */
    int removeWindHistOrderById(String id);


    /**
     * 通过流程实例主键查询流程实例
     *
     * @param id  主键
     * @return    单条流程执行实例
     */
    WindHistOrder getWindHistOrderById(String id);


    /**
     * 通过条件查询流程执行实例
     *
     * @param filter  过滤查询条件
     * @return        流程执行实例集合
     */
    List<WindHistOrder>  selectWindHistOrderList(QueryFilter filter);


    /**
     * 分页查询流程执行实例
     *
     * @param filter     过滤条件
     * @param windPage   分页条件
     * @return           流程执行实例集合
     */
    List<WindHistOrder>  selectWindHistOrderList(QueryFilter filter, WindPage<WindHistOrder> windPage);


    /*--------------------流程历史执行实例-----------------*/








    /*--------------------流程配置-----------------*/

    /**
     * 新增流程配置
     *
     * @param processConfig  流程配置
     * @return               新增结果
     */
    int  insertProcessConfig(WindProcessConfig processConfig);


    /**
     * 更新配置信息
     *
     * @param processConfig  配置实体
     *
     * @return               更新结果
     */
    int  updateProcessConfig(WindProcessConfig processConfig);


    /**
     * 通过主键删除流程配置
     *
     * @param id 主键
     * @return   删除结果
     */
    int  removeProcessConfigById(String id);


    /**
     * 通过流程id  删除所属配置数据
     *
     * @param processId  流程ID
     * @return           删除结果
     */
    int  removeProcessConfigByProcessId(String processId);


    /**
     * 通过主键查询单条流程配置信息
     *
     * @param id   主键
     * @return     单条流程配置信息
     */
    WindProcessConfig getProcessConfigById(String id);


    /**
     * 更新排序规则
     * @param ids   需要更新的配置ID集合
     * @param add   在原有的基础上更新多少
     * @return      更新结果
     */
    int  updateSort(List<String> ids,int add);


    /**
     * 通过过滤条件查询流程配置
     *
     * @param filter  过滤条件
     * @return        流程配置集合
     */
    List<WindProcessConfig>   selectProcessConfigList(QueryFilter filter);


    /*--------------------流程配置-----------------*/


    /*--------------------任务审批人-----------------*/


    /**
     * 新增任务审批人
     *
     * @param taskActor   任务审批人对象
     * @return            新增结果
     */
    int insertTaskActor(WindTaskActor taskActor);


    /**
     * 批量添加任务审批人
     * @param taskActors 任务审批人数据
     * @return           添加结果
     */
    int insertTaskActor(List<WindTaskActor> taskActors);



    /**
     * 通过任务id删除 任务审批人
     *
     * @param taskId  任务ID
     * @return        删除结果
     */
    int removeTaskActorByTaskId(String taskId);


    /**
     * 通过任务id集合，删除任务审批人
     *
     * @param taskIds  任务id集合
     * @return         删除结果
     */
    int removeTaskActorByTaskIds(List<String> taskIds);


    /**
     * 通过任务ID 和审批人ID删除审批人
     * @param taskId      任务id
     * @param taskActor   审批人ID
     * @return            删除结果
     */
    int removeTaskActor(String taskId,String taskActor);


    /**
     * 通过过滤条件查询任务审批人
     *
     * @param filter  过滤条件
     * @return        任务审批人
     */
    List<WindTaskActor>  selectTaskActorList(QueryFilter filter);


    /*--------------------任务审批人-----------------*/



    /*--------------------任务实例-----------------*/

    /**
     * 新增任务实例
     *
     * @param taskInstance   任务实例实体
     * @return               新增结果
     */
    int  insertTaskInstance(WindTask taskInstance);


    /**
     * 更新任务实例实体
     *
     * @param taskInstance   任务实例实体
     * @return               更新结果
     */
    int  updateTaskInstance(WindTask taskInstance);


    /**
     * 通过任务ID删除任务实例
     *
     *
     * @param id   任务ID
     * @return     删除结果
     */
    int  removeTaskInstanceById(String id);


    /**
     * 通过父任务ID 删除子任务
     *
     * @param parentId  父任务ID
     * @return  删除结果
     */
    int  removeTaskInstanceByParentId(String parentId);


    /**
     * 通过流程实例号删除所属任务
     *
     * @param orderId  流程实例号
     * @return         删除结果
     */
    int  removeTaskByOrderId(String orderId);


    /**
     * 通过任务ID集合删除任务
     *
     * @param taskIds   任务id集合
     * @return          删除结果
     */
    int  removeTaskByTaskIds(List<String> taskIds);




    /**
     * 通过任务ID 查询单条任务实例
     *
     * @param id  任务ID
     * @return    单条任务实例
     */
    WindTask getTaskInstanceById(String id);


    /**
     * 通过过滤条件查询任务实例
     *
     * @param queryFilter  过滤条件
     * @return              任务实例集合
     */
    List<WindTask> selectTaskInstanceList(QueryFilter queryFilter);


    /**
     * 分页查询任务实例
     *
     * @param queryFilter   过滤条件
     * @param windPage      分页条件
     * @return              任务实例集合
     */
    List<WindTask> selectTaskInstanceList(QueryFilter queryFilter, WindPage<WindTask> windPage);

    /*--------------------任务实例-----------------*/


    /*-----------------撤销数据----------------------*/

    /**
     * 添加任务撤销数据
     *
     * @param revokeData   撤销数据
     * @return              添加结果
     */
    int     insertWindRevokeData(WindRevokeData revokeData);


    /**
     * 通过流程实例号，将撤销数据删除
     *
     * @param orderId   流程实例号
     * @return           删除结果
     */
    int     deleteWindRevokeDataByOrderId(String orderId);


    /**
     * 通过过滤条件查询 撤销数据
     *
     * @param filter  过滤条件
     * @return         撤销数据集合
     */
    List<WindRevokeData>   selectRevokeData(QueryFilter filter);


    /**
     * 通过流程实例ID 和 操作用户ID 查询该用户在该流程下撤销的数据集合
     *
     * @param orderId   流程实例ID
     * @param actorId   操作人ID
     * @return           撤销数据集合
     */
    List<WindRevokeData>   selectRevokeByActor(String orderId,String actorId);






    /*--------扩展功能接口-------*/

    /**
     * 过滤查询任务实例接口
     *
     * @param taskFilter   查询条件
     * @return        任务实例集合
     */
    List<WindTaskInstance>  selectTaskInstance(TaskFilter taskFilter);


    /**
     * 查询任务实例数量
     *
     * @param filter  任务实例过滤条件
     * @return 数量
     */
    long   selectTaskInstanceCount(TaskFilter filter);




    /**
     * 过滤查询任务实例接口  可分页
     *
     * @param taskFilter   查询条件
     * @param windPage    分页参数
     *
     * @return        任务实例集合
     */
    List<WindTaskInstance>  selectTaskInstance(TaskFilter taskFilter,WindPage<WindTaskInstance> windPage);


    /**
     * 获取数据库连接
     *
     * @return  连接
     */
    Connection getConnection();


}
