package com.bcx.wind.workflow.imp;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.access.TaskFilter;
import com.bcx.wind.workflow.access.WindPage;
import com.bcx.wind.workflow.entity.WindOrder;
import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.entity.WindTaskInstance;
import com.bcx.wind.workflow.imp.cmd.managercmd.*;
import com.bcx.wind.workflow.imp.service.ManagerService;
import com.bcx.wind.workflow.pojo.ProcessModule;
import com.bcx.wind.workflow.pojo.WindUser;

import java.util.List;

/**
 * <p>
 * 工作流管理监控实现
 * 主要功能  过滤查询正在执行的流程以及任务，包含延期的流程任务等
 * 转办 关闭任务  挂起恢复流程  加签减签
 * 转办：分为流程转办，任务转办，流程定义下转办，模块转办，所有转办
 * 关闭任务：只针对于任务关闭
 * 挂起恢复，只需改变流程实例状态即可
 * 加签减签，分为普通的加签  ， 会签加签，以及串签加签。
 * 添加子任务，子任务不会影响主任务执行，子任务审批人没有流程的执行权限，只有查看任务的权限。
 * </p>
 *
 * @author zhanglei
 */
public class ManagerServiceImpl extends ServiceImpl  implements ManagerService {

    @Override
    public int language(String lang) {
        return commandExecutor.executor(new LanguageCmd(lang));
    }

    @Override
    public ProcessModule queryToDoProcess(String userId, String system) {
        return this.commandExecutor.executor(new QueryToDoProcessCommand(userId,system));
    }

    @Override
    public ProcessModule queryToDoProcess(String userId) {
        return this.commandExecutor.executor(new QueryToDoProcessCommand(userId,null));
    }

    @Override
    public List<WindTaskInstance> queryTaskInstance(TaskFilter filter, WindPage<WindTaskInstance> page) {
        return this.commandExecutor.executor(new QueryTaskInstanceCommand(filter,page));
    }

    @Override
    public List<WindOrder> queryOrder(QueryFilter filter) {
        return this.commandExecutor.executor(new QueryOrderCommand(filter));
    }

    @Override
    public List<WindOrder> queryOrder(QueryFilter filter, WindPage<WindOrder> windPage) {
        return this.commandExecutor.executor(new QueryOrderCommand(filter,windPage));
    }

    @Override
    public List<WindTask> queryTask(QueryFilter filter) {
        return this.commandExecutor.executor(new QueryTaskCommand(filter));
    }

    @Override
    public List<WindTask> queryTask(QueryFilter filter, WindPage<WindTask> windPage) {
        return this.commandExecutor.executor(new QueryTaskCommand(filter,windPage));
    }

    @Override
    public List<WindTask> transferByTaskId(List<String> taskIds, String oldUserId, WindUser newUser,WindUser windUser) {
        return this.commandExecutor.executor(new TransferCommand(taskIds,null,null,null,null,null,oldUserId,newUser,windUser));
    }

    @Override
    public List<WindTask> transferByOrderId(List<String> orderIds, String oldUserId, WindUser newUser,WindUser windUser) {
        return this.commandExecutor.executor(new TransferCommand(null,orderIds,null,null,null,null,oldUserId,newUser,windUser));
    }

    @Override
    public List<WindTask> transferByProcessId(List<String> processIds, String oldUserId, WindUser newUser,WindUser windUser) {
        return this.commandExecutor.executor(new TransferCommand(null,null,processIds,null,null,null,oldUserId,newUser,windUser));
    }

    @Override
    public List<WindTask> transferByProcessName(List<String> processNames, String oldUserId, WindUser newUser,WindUser windUser) {
        return this.commandExecutor.executor(new TransferCommand(null,null,null,processNames,null,null,oldUserId,newUser,windUser));
    }

    @Override
    public List<WindTask> transferByProcessModule(List<String> modules, String oldUserId, WindUser newUser,WindUser windUser) {
        return this.commandExecutor.executor(new TransferCommand(null,null,null,null,modules,null,oldUserId,newUser,windUser));
    }

    @Override
    public List<WindTask> transferBySystem(List<String> systems, String oldUserId, WindUser newUser,WindUser windUser) {
        return this.commandExecutor.executor(new TransferCommand(null,null,null,null,null,systems,oldUserId,newUser,windUser));
    }

    @Override
    public int closeTask(String taskId,WindUser windUser) {
        return this.commandExecutor.executor(new CloseTaskCommand(taskId,windUser));
    }

    @Override
    public int hangUpOrderByOrderId(String orderId) {
        return this.commandExecutor.executor(new HangUpOrderCommand(orderId,null));
    }

    @Override
    public int hangUpOrderByBusinessId(String businessId) {
        return this.commandExecutor.executor(new HangUpOrderCommand(null,businessId));
    }

    @Override
    public int recoveryOrderByOrderId(String orderId) {
        return this.commandExecutor.executor(new RecoveryOrderCommand(orderId,null));
    }

    @Override
    public int recoveryOrderByBusinessId(String businessId) {
        return this.commandExecutor.executor(new RecoveryOrderCommand(null,businessId));
    }

    @Override
    public List<WindTask> addApproveUsers(String taskId, List<WindUser> windUsers,WindUser user) {
        return this.commandExecutor.executor(new AddApproveUsersCommand(taskId,windUsers,user));
    }

    @Override
    public WindTask reduceApproveUser(String taskId, List<String> userIds,WindUser user) {
        return this.commandExecutor.executor(new ReduceApproveUserCommand(taskId,userIds,user));
    }

    @Override
    public List<WindTask> addChildTask(String taskId, List<WindUser> users) {
        return this.commandExecutor.executor(new CreateChildTaskCommand(taskId,users,null));
    }

    @Override
    public List<WindTask> addChildTask(String taskId, List<WindUser> users, WindUser user) {
        return this.commandExecutor.executor(new CreateChildTaskCommand(taskId,users,user));
    }

    @Override
    public int completeChildTask(String taskId, WindUser user,String suggest) {
        return this.commandExecutor.executor(new CompleteChildTaskCommand(taskId,user,suggest));
    }
}
