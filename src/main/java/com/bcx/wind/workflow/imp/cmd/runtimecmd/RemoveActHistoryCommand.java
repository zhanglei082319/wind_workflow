package com.bcx.wind.workflow.imp.cmd.runtimecmd;

import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;

import static com.bcx.wind.workflow.message.ErrorCode.REMOVE_ACT_HIST_FAIL;

/**
 * 删除执行历史命令  通过流程实例号删除执行历史
 *
 * @author zhanglei
 */
public class RemoveActHistoryCommand implements Command<Void> {

    private String orderId;

    public RemoveActHistoryCommand(String orderId){
        this.orderId = orderId;
    }

    @Override
    public Void executor(CommandContext context) {
        checkArgs();
        context.access().removeActiveHistoryByOrderId(this.orderId);
        return null;
    }

    private void checkArgs(){
        if(orderId == null){
            WindError.error(REMOVE_ACT_HIST_FAIL,null,"order");
        }
    }
}
