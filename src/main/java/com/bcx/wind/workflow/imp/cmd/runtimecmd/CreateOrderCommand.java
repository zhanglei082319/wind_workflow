package com.bcx.wind.workflow.imp.cmd.runtimecmd;

import com.bcx.wind.workflow.entity.WindOrder;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.support.Assert;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.TimeHelper;

import static com.bcx.wind.workflow.core.constant.Constant.JSON_NULL;
import static com.bcx.wind.workflow.message.ErrorCode.CREATE_ORDER_FAIL;
import static com.bcx.wind.workflow.message.ErrorCode.OPERATE_FAIL;

/**
 * 新建流程实例命令
 *
 * @author zhanglei
 */
public class CreateOrderCommand implements Command<WindOrder> {

    /**
     * 流程实例
     */
    private WindOrder windOrder;

    public CreateOrderCommand(WindOrder windOrder){
        this.windOrder = windOrder;
    }

    @Override
    public WindOrder executor(CommandContext context) {
        //校验
        checkArgs();
        //创建
        context.access().insertOrderInstance(this.windOrder);
        return this.windOrder;
    }

    private void checkArgs(){
        if(this.windOrder == null){
            WindError.error(OPERATE_FAIL,null,"order","createOrder","windOrder");
        }
        Assert.notEmptyError(CREATE_ORDER_FAIL,this.windOrder.getProcessId(),"processId");
        Assert.notEmptyError(CREATE_ORDER_FAIL,this.windOrder.getCreateUser(),"createUser");
        Assert.notEmptyError(CREATE_ORDER_FAIL,this.windOrder.getStatus(),"status");
        Assert.notEmptyError(CREATE_ORDER_FAIL,this.windOrder.getBusinessId(),"businessId");
        if(windOrder.getId()==null || "".equals(windOrder.getId().trim())){
            this.windOrder.setId(ObjectHelper.primaryKey());
        }
        windOrder.setCreateTime(TimeHelper.nowDate());
        windOrder.setVersion(1);
        if(this.windOrder.getVariable() == null || "".equals(this.windOrder.getVariable().trim())){
            this.windOrder.setVariable(JSON_NULL);
        }
    }
}
