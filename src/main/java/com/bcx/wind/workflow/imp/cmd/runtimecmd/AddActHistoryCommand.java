package com.bcx.wind.workflow.imp.cmd.runtimecmd;

import com.bcx.wind.workflow.entity.WindActHistory;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.support.Assert;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.TimeHelper;

import static com.bcx.wind.workflow.message.ErrorCode.CREATE_ACT_HIST_FAIL;
import static com.bcx.wind.workflow.message.ErrorCode.DATABASE_OPERATE_ERROR;

/**
 * 添加执行履历命令
 *
 * @author zhanglei
 */
public class AddActHistoryCommand implements Command<Void> {

    /**
     * 执行履历
     */
    private WindActHistory windActHistory;

    public AddActHistoryCommand(WindActHistory windActHistory){
        this.windActHistory = windActHistory;
    }

    @Override
    public Void executor(CommandContext context) {
        //校验
        checkArgs();
        //添加
        if(context.access().insertActiveHistory(this.windActHistory) != 1){
            WindError.error(DATABASE_OPERATE_ERROR,null);
        }
        return null;
    }

    private void checkArgs(){
        if(this.windActHistory == null){
            WindError.error(CREATE_ACT_HIST_FAIL,null,"windActHistory");
        }
        Assert.notEmptyError(CREATE_ACT_HIST_FAIL,this.windActHistory.getTaskId(),"taskId");
        Assert.notEmptyError(CREATE_ACT_HIST_FAIL,this.windActHistory.getTaskName(),"taskName");
        Assert.notEmptyError(CREATE_ACT_HIST_FAIL,this.windActHistory.getTaskDisplayName(),"taskDisplayName");
        Assert.notEmptyError(CREATE_ACT_HIST_FAIL,this.windActHistory.getProcessId(),"processId");
        Assert.notEmptyError(CREATE_ACT_HIST_FAIL,this.windActHistory.getOrderId(),"orderId");
        Assert.notEmptyError(CREATE_ACT_HIST_FAIL,this.windActHistory.getProcessName(),"processName");
        Assert.notEmptyError(CREATE_ACT_HIST_FAIL,this.windActHistory.getProcessDisplayName(),"processDisplayName");
        Assert.notEmptyError(CREATE_ACT_HIST_FAIL,this.windActHistory.getApproveUserVariable(),"approveUser");
        Assert.notEmptyError(CREATE_ACT_HIST_FAIL,this.windActHistory.getTaskType(),"taskType");

        if(this.windActHistory.getId() == null) {
            this.windActHistory.setId(ObjectHelper.primaryKey());
        }
        this.windActHistory.setCreateTime(TimeHelper.nowDate());
    }

}
