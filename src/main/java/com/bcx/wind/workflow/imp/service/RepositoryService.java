package com.bcx.wind.workflow.imp.service;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.access.WindPage;
import com.bcx.wind.workflow.entity.WindProcess;
import com.bcx.wind.workflow.entity.WindProcessConfig;
import com.bcx.wind.workflow.entity.WindProcessModule;
import com.bcx.wind.workflow.pojo.Condition;
import com.bcx.wind.workflow.pojo.ProcessModule;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * 工作流静态资源服务接口
 * 包含对流程定义以及流程定义数据的增删改查 操作
 * 流程定义的发布回收，删除，新增
 * 流程配置的新增，更新删除等。
 *
 * @author zhanglei
 */
public interface RepositoryService{

    /**
     * 添加新的流程定义
     * 添加流程定义和更新流程定义放在一个接口中，这里的添加流程定义会判断当前的流程定义名称是否在数据库中已经存在，
     * 如果存在，则获取老版本的流程定义的最高版本号，将版本号+1，流程定义名称不变添加一条新的数据。
     * 如果不存在，则直接添加新的数据，版本为1
     *
     * @param processXml  流程定义内容
     * @param system      模块
     * @return             添加成功后返回的流程定义对象
     */
    WindProcess  deploy(String processXml,String system);


    /**
     * 添加新的流程定义
     * 和上面的接口功能相当，参数数据类型不同
     *
     * @param stream   流程定义内容  流形式
     * @param system    模块
     * @return          添加成功后返回的流程定义对象
     */
    WindProcess  deploy(InputStream stream,String system);


    /**
     * 通过流程定义ID  查询单条流程定义
     *
     * @param processId  流程定义ID
     * @return            流程定义
     */
    WindProcess  queryById(String processId);


    /**
     * 通过流程定义名称，获取最高版本的流程定义
     *
     * @param processName  流程定义名称
     * @return         最高版本的流程定于
     */
    WindProcess  queryMaxVersion(String processName);


    /**
     * 查询流程定义
     *
     * @param queryFilter  filter过滤条件
     * @return              流程定义集合
     */
    List<WindProcess>  queryProcess(QueryFilter queryFilter);


    /**
     * 分页查询流程定义
     *
     * @param queryFilter  过滤条件
     * @param page          分页条件
     * @return              流程定义集合
     */
    List<WindProcess>  queryProcessPage(QueryFilter queryFilter, WindPage<WindProcess> page);


    /**
     * 通过流程定义ID  删除流程定义  发布和模板状态不可删除  回收状态的流程定义需要查看该流程
     * 定义是否存在正在执行的流程实例，如果不存在则可删除，否则不可删除。
     * 删除后，需要将流程定义关联的流程配置数据也一并删除，相关缓存数据也一并删除
     *
     * @param processId  流程定义ID
     * @return            删除结果
     */
    int  removeByProcessId(String processId);


    /**
     * 发布流程   发布流程定义会将流程定义状态更改成RELEASE, 并且自动生成默认的流程配置数据
     * 为了提高系统执行效率，会将发布的流程和流程配置数据添加到缓存中。后期执行流程获取发布的流程定义时，先在缓存中查询
     *
     * @param processId  流程定义ID
     * @return            发布结果
     */
    int  release(String processId);


    /**
     * 回收流程定义
     * 回收操作相对简单，状态必须为模板和发布才可回收，将状态更改为回收状态
     * 流程定义回收后，该流程定义下正在执行的流程实例还是继续执行下去，只是在下一次执行流程实例时不会再使用这个流程定义了
     *
     * @param processId  流程定义ID
     * @return           回收结果
     */
    int  recovery(String processId);


    /**
     * 新增流程定义模块
     *
     * @param windProcessModule  流程定义模块对象
     * @return   新增结果
     */
    int  addProcessModule(WindProcessModule windProcessModule);


    /**
     * 更新流程定模块
     *
     * @param windProcessModule  流程定义模块对象
     * @return   更新结果
     */
    int  updateProcessModule(WindProcessModule windProcessModule);


    /**
     * 删除流程定义模块
     *
     * @param id 主键
     * @return  删除结果
     */
    int  deleteProcessModule(String id);


    /**
     * 查询指定系统下 流程定义模块组合数据对象  如果有缓存，返回缓存数据
     *
     * @param system  系统标识
     * @return       模块组合
     */
    ProcessModule  selectWindProcessModule(String system);


    /**
     * 查询所有流程定义模块组合  如果有缓存，返回缓存数据
     * @return  模块组合
     */
    ProcessModule  selectWindProcessModule();


    /**
     * 查询指定系统下 流程定义模块组合数据对象  不走缓存
     *
     * @param system  系统标识
     * @return       模块组合
     */
    ProcessModule   selectDataWindProcessModule(String system);


    /**
     * 查询所有流程定义模块组合  不走缓存
     * @return  模块组合
     */
    ProcessModule   selectDataWindProcessModule();


    /**
     * 添加流程定义配置数据
     *
     * @param config  配置数据
     * @return         添加结果
     */
    int  addProcessConfig(WindProcessConfig config);


    /**
     * 更新流程定义配置
     *
     * @param config  配置数据
     * @return        更新结果
     */
    int  updateProcessConfig(WindProcessConfig config);


    /**
     * 过滤查询流程定义配置数据
     *
     * @param filter  过滤条件
     * @return         查询结果
     */
    List<WindProcessConfig>  queryProcessConfig(QueryFilter filter);


    /**
     * 通过流程定义配置id 删除配置数据
     *
     * @param configId  配置ID
     * @return           删除结果
     */
    int  removeProcessConfigById(String configId);


    /**
     * 通过流程定义ID 删除配置数据
     *
     * @param processId  流程定义ID
     * @return            删除结果
     */
    int  removeProcessConfigByProcessId(String processId);


    /**
     * 添加节点配置数据
     *
     * @param configId   配置ID
     * @param key         节点配置键
     * @param value       节点配置值
     * @return            添加结果
     */
    int  addNodeConfig(String configId,String key,Object value);


    /**
     * 添加节点配置数据
     *
     * @param configId   配置ID
     * @param nodeConfigMap   节点配置数据
     * @return                 添加结果
     */
    int  addNodeConfig(String configId,Map<String,Object> nodeConfigMap);


    /**
     * 删除节点配置键
     *
     * @param configId  配置ID
     * @param key       节点配置键
     * @return           删除结果
     */
    int  removeNodeConfig(String configId,String key);


    /**
     * 删除节点配置  通过键集合
     *
     * @param configId  配置ID
     * @param key    键集合
     * @return       删除结果
     */
    int  removeNodeConfig(String configId,List<String> key);


    /**
     * 添加业务配置
     *
     * @param configId   配置ID
     * @param key         业务配置键
     * @param value       业务配置值
     * @return            添加结果
     */
    int  addBusinessConfig(String configId,String key,String value);


    /**
     * 批量添加业务参数配置
     *
     * @param configId   配置ID
     * @param businessConfigMap  业务参数数据
     * @return          添加结果
     */
    int  addBusinessConfig(String configId,Map<String,Object> businessConfigMap);


    /**
     * 删除业务参数
     *
     * @param configId  配置ID
     * @param key       业务配置键
     * @return           添加结果
     */
    int  removeBusinessConfig(String configId,String key);


    /**
     * 批量删除业务参数配置  通过键集合
     *
     * @param configId  配置ID
     * @param keys      键集合
     * @return         删除结果
     */
    int  removeBusinessConfig(String configId,List<String> keys);


    /**
     * 添加配置路由匹配条件
     *
     * @param configId   配置ID
     * @param condition  匹配规则数据
     * @return            添加结果
     */
    int  addConditionConfig(String configId, Condition condition);


    /**
     * 批量添加匹配规则配置
     *
     * @param configId    配置ID
     * @param conditions  匹配规则配置集合
     * @return             添加结果
     */
    int  addConditionConfig(String configId, List<Condition> conditions);

    /**
     * 删除配置数据中的匹配规则数据组
     *
     * @param configId  配置ID
     * @param key       匹配键
     * @return          删除结果
     */
    int  removeConditionConfig(String configId,String key);


    /**
     * 批量删除匹配规则配置  通过key集合
     *
     * @param configId 配置ID
     * @param keys     key集合
     * @return         删除结果
     */
    int  removeConditionConfig(String configId,List<String> keys);

}
