package com.bcx.wind.workflow.imp.cmd.managercmd;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.access.WindPage;
import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.support.Assert;

import java.util.List;

import static com.bcx.wind.workflow.message.ErrorCode.QUERY_FILTER_NULL;

public class QueryTaskCommand implements Command<List<WindTask>> {
    /**
     * 过滤条件
     */
    private QueryFilter queryFilter;

    /**
     * 分页数据
     */
    private WindPage windPage;

    public QueryTaskCommand(QueryFilter filter){
        this.queryFilter = filter;
    }


    public QueryTaskCommand(QueryFilter queryFilter , WindPage windPage){
        this.queryFilter = queryFilter;
        this.windPage = windPage;
    }



    @Override
    public List<WindTask> executor(CommandContext context) {
        try{
            return execute(context);
        }catch (Exception e){
            context.log(e);
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    private List<WindTask> execute(CommandContext context) {
        Assert.notEmptyError(QUERY_FILTER_NULL,this.queryFilter);

        if(this.windPage == null){
            return context.access().selectTaskInstanceList(this.queryFilter);
        }

        return context.access().selectTaskInstanceList(this.queryFilter,this.windPage);
    }
}
