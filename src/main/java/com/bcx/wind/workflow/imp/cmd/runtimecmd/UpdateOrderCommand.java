package com.bcx.wind.workflow.imp.cmd.runtimecmd;

import com.bcx.wind.workflow.entity.WindOrder;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.support.Assert;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.TimeHelper;

import static com.bcx.wind.workflow.core.constant.Constant.JSON_NULL;
import static com.bcx.wind.workflow.message.ErrorCode.*;
import static com.bcx.wind.workflow.message.ErrorCode.CREATE_ORDER_FAIL;

/**
 * 更新流程实例命令
 *
 * @author zhanglei
 */
public class UpdateOrderCommand implements Command<Void> {

    /**
     * 需要更新的流程实例
     */
    private WindOrder windOrder;

    public UpdateOrderCommand(WindOrder windOrder){
        this.windOrder = windOrder;
    }

    @Override
    public Void executor(CommandContext context) {
        checkData(context);
        //更新
        context.access().updateOrderInstance(this.windOrder);
        return null;
    }

    /**
     * 校验是否为空
     */
    private void checkData(CommandContext context){
        if(this.windOrder == null){
            WindError.error(OPERATE_FAIL,null,"order","updateOrder","windOrder");
        }
        Assert.notEmptyError(UPDATE_ORDER_FAIL,this.windOrder.getProcessId(),"processId");
        Assert.notEmptyError(UPDATE_ORDER_FAIL,this.windOrder.getCreateUser(),"createUser");
        Assert.notEmptyError(UPDATE_ORDER_FAIL,this.windOrder.getStatus(),"status");
        Assert.notEmptyError(UPDATE_ORDER_FAIL,this.windOrder.getId(),"id");

        if(this.windOrder.getVariable() == null || "".equals(this.windOrder.getVariable().trim())){
            this.windOrder.setVariable(JSON_NULL);
        }
    }
}
