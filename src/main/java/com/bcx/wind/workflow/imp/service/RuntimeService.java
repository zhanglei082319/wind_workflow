package com.bcx.wind.workflow.imp.service;

import com.bcx.wind.workflow.entity.*;

import java.util.List;
import java.util.Map;

/**
 * 工作流运行时服务接口  仅对工作流内部服务，外部不可使用
 *
 *  主要作用：对工作流执行中一些表数据的增删改查
 *  如 流程实例新建，更新，删除，历史流程实例新增。
 *  任务实例新建，删除，更新，审批人添加删除，
 *  撤销数据新增，删除
 *  执行履历新增，更新，删除，完毕履历新增等。
 *
 *
 * @author zhanglei
 */
public interface RuntimeService{

    /**
     * 添加新的任务实例
     *
     * @param windTask  任务实例
     * @return           任务实例
     */
    WindTask   createNewTask(WindTask windTask);


    /**
     * 更新任务实例
     *
     * @param task  任务
     */
    void   updateTask(WindTask task);

    /**
     * 任务添加变量
     *
     * @param taskId    任务ID
     * @param variable  任务变量
     */
    void   addTaskVariable(String taskId,Map<String,Object> variable);


    /**
     * 为指定任务添加变量
     *
     * @param taskId  任务ID
     * @param key     变量键
     * @param value   变量值
     */
    void   addTaskVariable(String taskId,String key,Object value);


    /**
     * 通过任务ID  删除任务  包含审批人
     *
     * @param taskId  任务ID
     */
    void   removeTask(String taskId);


    /**
     * 通过流程实例号 删除关联任务  包含任务审批人
     *
     * @param orderId  流程实例号
     */
    void   removeTaskByOrderId(String orderId);


    /**
     * 添加任务审批人
     *
     * @param taskId  任务ID
     * @param users   审批人
     */
    void   addTaskActor(String taskId, List<String> users);


    /**
     * 通过任务ID  指定审批人 ，删除这个任务的指定审批人
     *
     * @param actorId  审批人
     * @param taskId   任务ID
     */
    void   removeTaskActor(String taskId,String actorId);


    /**
     * 通过任务ID，删除这个任务的多个审批人
     *
     * @param taskId   任务ID
     * @param actors   多个审批人ID
     */
    void   removeTaskActor(String taskId,List<String> actors);


    /**
     * 新增流程实例
     *
     * @param windOrder  流程实例
     * @return            流程实例
     */
    WindOrder   createOrder(WindOrder windOrder);


    /**
     * 更新流程实例
     *
     * @param windOrder  流程实例
     */
    void   updateOrder(WindOrder windOrder);


    /**
     * 流程实例添加变量
     *
     * @param orderId  流程实例号
     * @param variable  变量
     */
    void   addOrderVariable(String orderId,Map<String,Object> variable);


    /**
     * 为指定流程实例添加变量
     *
     * @param orderId  流程实例ID
     * @param key     变量键
     * @param value   变量值
     */
    void   addOrderVariable(String orderId,String key,Object value);


    /**
     * 删除流程实例
     *
     * @param orderId  流程实例ID
     */
    void   removeOrder(String orderId);

    /**
     * 添加历史流程实例
     *
     * @param windHistOrder  历史流程实例
     */
    void   createHistOrder(WindHistOrder windHistOrder);


    /**
     * 添加正在执行履历
     *
     * @param actHistory  执行履历
     */
    void   addActHistory(WindActHistory actHistory);


    /**
     * 更新正在执行履历
     * @param windActHistory   更新执行履历
     */
    void   updateActHistory(WindActHistory windActHistory);


    /**
     * 通过实例号删除执行履历
     *
     * @param orderId  流程实例号
     */
    void   removeActHistory(String orderId);

    /**
     * 添加完毕履历
     *
     * @param windActHistories   执行履历
     */
    void   addEndHistory(List<WindActHistory> windActHistories);


    /**
     * 通过流程实例号查询该流程的撤销数据
     *
     * @param orderId  流程实例号
     * @return     撤销队列
     */
    List<WindRevokeData>  queryRevokeDataByOrderId(String orderId);


    /**
     * 通过业务ID查询该流程的撤销数据
     *
     * @param businessId  业务ID
     * @return     撤销队列
     */
    List<WindRevokeData>  queryRevokeDataByBusinessId(String businessId);
}















