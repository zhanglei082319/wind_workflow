package com.bcx.wind.workflow.imp.cmd.repositorycmd.processconfigcmd;

import com.bcx.wind.workflow.entity.WindProcessConfig;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.support.ObjectHelper;

import java.util.Map;

import static com.bcx.wind.workflow.message.ErrorCode.DATABASE_OPERATE_ERROR;
import static com.bcx.wind.workflow.message.ErrorCode.OPERATE_FAIL;
import static com.bcx.wind.workflow.message.ErrorCode.QUERY_EMPTY;

/**
 * 添加业务参数配置命令
 *
 * @author zhanglei
 */
public class AddBusinessConfigCommand implements Command<Integer> {

    /**
     * 配置ID
     */
    private String configId;

    /**
     * 业务参数配置
     */
    private Map<String,Object> businessConfig;

    /**
     * 流程配置数据
     */
    private WindProcessConfig windProcessConfig;

    /**
     * 执行数据
     */
    private CommandContext commandContext;

    public AddBusinessConfigCommand(String configId,Map<String,Object> businessConfig){
        this.configId = configId;
        this.businessConfig = businessConfig;
    }

    @Override
    public Integer executor(CommandContext context) {
        try{
            this.commandContext = context;
            return this.execute();
        }catch (Exception e){
            commandContext.log(e);
            return null;
        }

    }


    private Integer execute(){
        //校验参数
        checkArgs();
        //添加
        int ret = addBusinessConfig();
        //更新缓存
        this.commandContext.getConfiguration().addProcessConfigCache(this.windProcessConfig.getProcessId(),this.windProcessConfig);
        return ret;
    }

    /**
     * 添加节点配置操作
     * @return   添加结果
     */
    private int  addBusinessConfig(){
        for(Map.Entry<String,Object> args : this.businessConfig.entrySet()){
            String key = args.getKey();
            Object value = args.getValue();
            this.windProcessConfig.addBusinessConfig(key,value);
        }
        int ret = this.commandContext.access().updateProcessConfig(this.windProcessConfig);
        if(ret != 1){
            WindError.error(DATABASE_OPERATE_ERROR,null);
        }
        return ret;
    }

    /**
     * 校验参数
     */
    private  void checkArgs(){
        if(this.configId == null){
            WindError.error(OPERATE_FAIL,null,"processConfig","addBusinessConfig","configId");
        }
        this.windProcessConfig = this.commandContext.access().getProcessConfigById(configId);
        if(this.windProcessConfig == null){
            WindError.error(QUERY_EMPTY,null,"processConfig",configId);
        }

        if(ObjectHelper.isEmpty(this.businessConfig)){
            WindError.error(OPERATE_FAIL,null,"processConfig","addBusinessConfig","businessConfig");
        }
    }
}
