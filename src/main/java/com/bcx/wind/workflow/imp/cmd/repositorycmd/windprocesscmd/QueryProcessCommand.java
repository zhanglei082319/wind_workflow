package com.bcx.wind.workflow.imp.cmd.repositorycmd.windprocesscmd;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.access.WindPage;
import com.bcx.wind.workflow.entity.WindProcess;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;

import java.util.List;

import static com.bcx.wind.workflow.message.ErrorCode.QUERY_FILTER_NULL;

/**
 * 查询流程定义
 *
 * @author zhanglei
 */
public class QueryProcessCommand implements Command<List<WindProcess>> {

    /**
     * 查询条件
     */
    private QueryFilter queryFilter;

    /**
     * 分页条件
     */
    private WindPage<WindProcess> windPage;

    /**
     * 执行数据
     */
    private CommandContext commandContext;

    public QueryProcessCommand(QueryFilter filter,WindPage<WindProcess> page){
        this.queryFilter = filter;
        this.windPage = page;
    }

    @Override
    public List<WindProcess> executor(CommandContext context) {
        try{
            this.commandContext = context;
            return execute();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private List<WindProcess>  execute(){
        checkArgs();
        if(this.windPage != null) {
            return commandContext.access().selectProcessList(this.queryFilter, this.windPage);
        }
        return  commandContext.access().selectProcessList(this.queryFilter);
    }


    private void checkArgs(){
        if(this.queryFilter == null){
            WindError.error(QUERY_FILTER_NULL,null);
        }
    }
}
