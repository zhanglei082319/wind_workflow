package com.bcx.wind.workflow.imp.cmd.managercmd;

import com.bcx.wind.workflow.errorcontext.WindLocalError;
import com.bcx.wind.workflow.imp.Configuration;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;

/**
 * 更新当前工作流语言实现
 *
 * @author zhanglei
 */
public class LanguageCmd implements Command<Integer> {

    private String  language;

    public LanguageCmd(String language){
        this.language = language;
    }

    @Override
    public Integer executor(CommandContext context) {
        Configuration configuration = context.getConfiguration();
        configuration.getMessage().setLang(language);
        WindLocalError.remove();
        return 1;
    }
}
