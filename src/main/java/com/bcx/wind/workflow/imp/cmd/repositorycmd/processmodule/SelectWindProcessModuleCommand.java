package com.bcx.wind.workflow.imp.cmd.repositorycmd.processmodule;

import com.bcx.wind.workflow.entity.WindProcessModule;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.pojo.ProcessModule;
import com.bcx.wind.workflow.support.ObjectHelper;

import java.util.List;

import static com.bcx.wind.workflow.core.constant.Constant.ROOT_MODULE;

/**
 * 查询流程定义模块集合
 *
 * @author zhanglei
 */
public class SelectWindProcessModuleCommand implements Command<ProcessModule> {

    /**
     * 系统标识
     */
    private String system;

    /**
     * 是否走缓存
     */
    private boolean cache;

    /**
     * 执行依赖
     */
    private CommandContext commandContext;

    /**
     * 执行结果
     */
    private ProcessModule result;

    /**
     * 根模块
     */
    private WindProcessModule rootModule;

    /**
     * 所有模块
     */
    private List<WindProcessModule> allModules;

    public SelectWindProcessModuleCommand(String system,boolean cache){
        this.system = system;
        this.cache = cache;
    }


    @Override
    public ProcessModule executor(CommandContext context) {
        try{
            this.commandContext = context;
            return execute();
        }catch (Exception e) {
            return null;
        }
    }

    private ProcessModule execute() {
        //是否走缓存
        if(this.cache){
            ProcessModule module = this.commandContext.getConfiguration().getProcessModule();
            //如果有缓存，直接返回
            if(!ObjectHelper.isEmpty(module)){
                return module;
            }
        }
        this.allModules = buildAllModule();

        //获取根目录
        buildRootModule();

        this.result = new ProcessModule()
                .setWindProcessModule(this.rootModule);

        //构建返回值
        buildResult(this.allModules,this.result);
        return this.result;
    }


    private void buildResult(List<WindProcessModule> windProcessModules,ProcessModule processModule){
        for(WindProcessModule module : windProcessModules){
            String parentId = module.getParentId();
            WindProcessModule parentModule = processModule.getWindProcessModule();
            if(parentModule.getId().equals(parentId)){
                //添加子模块
                ProcessModule child = new ProcessModule()
                        .setWindProcessModule(module);
                processModule.getChild().add(child);
                //递归
                buildResult(windProcessModules,child);
            }

        }
    }


    /**
     * 获取更目录
     */
    private void buildRootModule(){
        if(!ObjectHelper.isEmpty(this.allModules)){
            this.rootModule = this.allModules.stream().filter(module->ROOT_MODULE.equals(module.getId()))
                    .findFirst().orElse(null);
            if(this.rootModule == null){
                //新增一个默认的
                this.rootModule = new WindProcessModule()
                        .setId(ObjectHelper.primaryKey())
                        .setModuleName("root");
                this.commandContext.access().insertProcessModule(rootModule);
            }
        }
    }


    /**
     * 模块所有模块
     * @return  模块集合
     */
    private List<WindProcessModule>  buildAllModule(){
        if(system != null){
            return this.commandContext.access().selectProcessModule(system);
        }
        return this.commandContext.access().selectProcessModule();
    }
}
