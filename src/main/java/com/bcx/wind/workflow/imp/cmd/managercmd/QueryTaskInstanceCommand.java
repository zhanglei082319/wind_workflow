package com.bcx.wind.workflow.imp.cmd.managercmd;

import com.bcx.wind.workflow.access.TaskFilter;
import com.bcx.wind.workflow.access.WindPage;
import com.bcx.wind.workflow.entity.WindTaskInstance;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.support.Assert;

import java.util.List;

import static com.bcx.wind.workflow.message.ErrorCode.QUERY_FILTER_NULL;

/**
 * 通过过滤条件查询任务实例命令
 *
 * @author zhanglei
 */
public class QueryTaskInstanceCommand implements Command<List<WindTaskInstance>> {

    /**
     * 查询过滤器
     */
    private TaskFilter filter;

    /**
     * 分页条件
     */
    private WindPage<WindTaskInstance> page;

    public QueryTaskInstanceCommand(TaskFilter filter,WindPage<WindTaskInstance> page){
        this.filter = filter;
        this.page = page;
    }


    @Override
    public List<WindTaskInstance> executor(CommandContext context) {
        try{
            return execute(context);
        }catch (Exception e) {
            context.log(e);
            return null;
        }
    }

    private List<WindTaskInstance> execute(CommandContext context) {
        Assert.notEmptyError(QUERY_FILTER_NULL,this.filter);
        return context.access().selectTaskInstance(this.filter,this.page);
    }
}
