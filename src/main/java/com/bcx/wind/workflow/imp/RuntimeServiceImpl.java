package com.bcx.wind.workflow.imp;

import com.bcx.wind.workflow.entity.*;
import com.bcx.wind.workflow.imp.cmd.runtimecmd.*;
import com.bcx.wind.workflow.imp.service.RuntimeService;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * 运行时服务实现
 *
 * @author zhanglei
 */
public class RuntimeServiceImpl extends ServiceImpl  implements RuntimeService {


    @Override
    public WindTask createNewTask(WindTask windTask) {
        return this.commandExecutor.executor(new CreateNewTaskCommand(windTask));
    }

    @Override
    public void updateTask(WindTask task) {
        this.commandExecutor.executor(new UpdateTaskCommand(task));
    }

    @Override
    public void addTaskVariable(String taskId, Map<String, Object> variable) {
        this.commandExecutor.executor(new AddTaskVariableCommand(taskId,variable));
    }

    @Override
    public void addTaskVariable(String taskId, String key, Object value) {
        this.commandExecutor.executor(new AddTaskVariableCommand(taskId,Collections.singletonMap(key,value)));
    }

    @Override
    public void removeTask(String taskId) {
        this.commandExecutor.executor(new RemoveTaskCommand(taskId,null));
    }

    @Override
    public void removeTaskByOrderId(String orderId) {
        this.commandExecutor.executor(new RemoveTaskCommand(null,orderId));
    }

    @Override
    public void addTaskActor(String taskId, List<String> users) {
        this.commandExecutor.executor(new AddTaskActorCommand(taskId,users));
    }

    @Override
    public void removeTaskActor(String taskId,String actorId) {
        this.commandExecutor.executor(new RemoveTaskActorCommand(taskId,Collections.singletonList(actorId)));
    }

    @Override
    public void removeTaskActor(String taskId,List<String> actorId) {
        this.commandExecutor.executor(new RemoveTaskActorCommand(taskId,actorId));
    }

    @Override
    public WindOrder createOrder(WindOrder windOrder) {
        return this.commandExecutor.executor(new CreateOrderCommand(windOrder));
    }

    @Override
    public void updateOrder(WindOrder windOrder) {
        this.commandExecutor.executor(new UpdateOrderCommand(windOrder));
    }

    @Override
    public void addOrderVariable(String orderId, Map<String, Object> variable) {
        this.commandExecutor.executor(new AddOrderVariableCommand(orderId,variable));
    }

    @Override
    public void addOrderVariable(String orderId, String key, Object value) {
        this.commandExecutor.executor(new AddOrderVariableCommand(orderId,Collections.singletonMap(key,value)));
    }

    @Override
    public void removeOrder(String orderId) {
        this.commandExecutor.executor(new RemoveOrderCommand(orderId));
    }

    @Override
    public void createHistOrder(WindHistOrder windHistOrder) {
        this.commandExecutor.executor(new CreateHistOrderCommand(windHistOrder));
    }

    @Override
    public void addActHistory(WindActHistory actHistory) {
        this.commandExecutor.executor(new AddActHistoryCommand(actHistory));
    }

    @Override
    public void updateActHistory(WindActHistory windActHistory) {
        this.commandExecutor.executor(new UpdateActHistoryCommand(windActHistory));
    }

    @Override
    public void removeActHistory(String orderId) {
        this.commandExecutor.executor(new RemoveActHistoryCommand(orderId));
    }

    @Override
    public void addEndHistory(List<WindActHistory> windActHistory) {
         this.commandExecutor.executor(new AddEndHistoryCommand(windActHistory));
    }

    @Override
    public List<WindRevokeData> queryRevokeDataByOrderId(String orderId) {
        return this.commandExecutor.executor(new QueryRevokeDataCommand(orderId,null));
    }

    @Override
    public List<WindRevokeData> queryRevokeDataByBusinessId(String businessId) {
        return this.commandExecutor.executor(new QueryRevokeDataCommand(null,businessId));
    }
}
