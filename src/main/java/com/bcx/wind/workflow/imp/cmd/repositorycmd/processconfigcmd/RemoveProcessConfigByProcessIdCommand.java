package com.bcx.wind.workflow.imp.cmd.repositorycmd.processconfigcmd;

import com.bcx.wind.workflow.entity.WindProcessConfig;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;

import java.util.List;

import static com.bcx.wind.workflow.message.ErrorCode.OPERATE_FAIL;

/**
 * 通过流程ID 删除实施配置命令
 *
 * @author zhanglei
 */
public class RemoveProcessConfigByProcessIdCommand implements Command<Integer> {

    /**
     * 流程定义ID
     */
    private String processId;

    /**
     * 执行数据
     */
    private CommandContext commandContext;

    public RemoveProcessConfigByProcessIdCommand(String processId){
        this.processId = processId;
    }

    @Override
    public Integer executor(CommandContext context) {
        try{
            this.commandContext = context;
            return execute();
        }catch (Exception e){
            commandContext.log(e);
            return null;
        }
    }


    private Integer execute(){
        checkArgs();
        //执行删除
        int ret = this.commandContext.access().removeProcessConfigByProcessId(this.processId);
        //删除缓存
        this.commandContext.getConfiguration().removeProcessConfigCache(this.processId);
        return ret;
    }

    /**
     * 校验删除参数
     *
     * 这里参数为processId  由于processId不为主键，这里不对是否存在做校验
     */
    private void checkArgs(){
        if(this.processId == null){
            WindError.error(OPERATE_FAIL,null,"processConfig","remove","processId");
        }
    }
}
