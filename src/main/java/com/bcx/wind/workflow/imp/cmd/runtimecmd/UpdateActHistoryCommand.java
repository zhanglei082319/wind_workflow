package com.bcx.wind.workflow.imp.cmd.runtimecmd;

import com.bcx.wind.workflow.entity.WindActHistory;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.support.Assert;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.TimeHelper;

import static com.bcx.wind.workflow.message.ErrorCode.CREATE_ACT_HIST_FAIL;
import static com.bcx.wind.workflow.message.ErrorCode.DATABASE_OPERATE_ERROR;
import static com.bcx.wind.workflow.message.ErrorCode.UPDATE_ACT_HIST_FAIL;

/**
 * 更新历史执行履历命令
 *
 * @author zhanglei
 */
public class UpdateActHistoryCommand implements Command<Void> {

    /**
     * 需要更新的执行履历
     */
    private WindActHistory windActHistory;

    public UpdateActHistoryCommand(WindActHistory windActHistory){
        this.windActHistory = windActHistory;
    }

    @Override
    public Void executor(CommandContext context) {
        //校验
        checkArgs();
        //更新
        if(context.access().updateActiveHistory(this.windActHistory) != 1){
            WindError.error(DATABASE_OPERATE_ERROR,null);
        }
        return null;
    }

    private void checkArgs(){
        if(this.windActHistory == null){
            WindError.error(UPDATE_ACT_HIST_FAIL,null,"windActHistory");
        }
        Assert.notEmptyError(UPDATE_ACT_HIST_FAIL,this.windActHistory.getId(),"id");
        Assert.notEmptyError(UPDATE_ACT_HIST_FAIL,this.windActHistory.getTaskId(),"taskId");
        Assert.notEmptyError(UPDATE_ACT_HIST_FAIL,this.windActHistory.getTaskName(),"taskName");
        Assert.notEmptyError(UPDATE_ACT_HIST_FAIL,this.windActHistory.getTaskDisplayName(),"taskDisplayName");
        Assert.notEmptyError(UPDATE_ACT_HIST_FAIL,this.windActHistory.getProcessId(),"processId");
        Assert.notEmptyError(UPDATE_ACT_HIST_FAIL,this.windActHistory.getOrderId(),"orderId");
        Assert.notEmptyError(UPDATE_ACT_HIST_FAIL,this.windActHistory.getProcessName(),"processName");
        Assert.notEmptyError(UPDATE_ACT_HIST_FAIL,this.windActHistory.getProcessDisplayName(),"processDisplayName");
        Assert.notEmptyError(UPDATE_ACT_HIST_FAIL,this.windActHistory.getOperate(),"operate");
        Assert.notEmptyError(UPDATE_ACT_HIST_FAIL,this.windActHistory.getActorId(),"actorId");
        Assert.notEmptyError(UPDATE_ACT_HIST_FAIL,this.windActHistory.getSubmitUserVariable(),"actorUser");

        Assert.notEmptyError(UPDATE_ACT_HIST_FAIL,this.windActHistory.getApproveUserVariable(),"approveUser");
        Assert.notEmptyError(UPDATE_ACT_HIST_FAIL,this.windActHistory.getTaskType(),"taskType");

        if(windActHistory.getApproveTime() == null) {
            this.windActHistory.setApproveTime(TimeHelper.nowDate());
        }

    }
}
