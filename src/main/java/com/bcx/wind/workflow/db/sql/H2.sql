

CREATE TABLE IF NOT EXISTS wind_act_hist (
id varchar(64)   NOT NULL primary  key ,
task_id varchar(64)   NOT NULL,
task_name varchar(64)   NOT NULL,
task_display_name varchar(255)  ,
process_id varchar(64)   NOT NULL,
order_id varchar(64)   NOT NULL,
process_name varchar(64)   NOT NULL,
process_display_name varchar(255)  ,
operate varchar(32)  ,
suggest varchar(1024)  ,
approve_time timestamp(6),
actor_id varchar(64)  ,
actor_name varchar(255)  ,
create_time timestamp(6),
approve_user_variable text  ,
task_type varchar(32)  ,
system varchar(64)  ,
submit_user_variable text  ,
variable  text,
task_create_user varchar(255),
task_level varchar(16) not null,
task_parent_id varchar(64)
)

;

-- ----------------------------
-- Table structure for wind_end_hist
-- ----------------------------
CREATE TABLE IF NOT EXISTS wind_end_hist (
id varchar(64)   NOT NULL primary  key ,
task_id varchar(64)   NOT NULL,
task_name varchar(64)   NOT NULL,
task_display_name varchar(255)  ,
process_id varchar(64)   NOT NULL,
order_id varchar(64)   NOT NULL,
process_name varchar(64)   NOT NULL,
process_display_name varchar(255)  ,
operate varchar(32)  ,
suggest varchar(1024)  ,
approve_time timestamp(6),
actor_id varchar(64)  ,
actor_name varchar(255)  ,
create_time timestamp(6),
approve_user_variable text  ,
task_type varchar(32)  ,
system varchar(64)  ,
submit_user_variable text  ,
variable  text,
task_create_user varchar(255),
task_level varchar(16) not null,
task_parent_id varchar(64)
)


;

-- ----------------------------
-- Table structure for wind_hist_order
-- ----------------------------
CREATE TABLE IF NOT EXISTS wind_hist_order (
id varchar(64)   NOT NULL primary  key ,
process_id varchar(64)   NOT NULL,
status varchar(32)   NOT NULL,
create_user varchar(64)   NOT NULL,
create_time timestamp(6) NOT NULL,
expire_time timestamp(6),
parent_id varchar(64)  ,
version integer NOT NULL,
variable text  ,
data varchar(4000)  ,
system varchar(64)  ,
finish_time timestamp(6),
business_id varchar(64) NOT NULL,
type varchar(32)
)


;

-- ----------------------------
-- Table structure for wind_order
-- ----------------------------
CREATE TABLE IF NOT EXISTS wind_order (
id varchar(64)   NOT NULL primary key ,
process_id varchar(64)   NOT NULL,
status varchar(32)   NOT NULL,
create_user varchar(64)   NOT NULL,
create_time timestamp(6) NOT NULL,
expire_time timestamp(6),
parent_id varchar(64)  ,
version integer NOT NULL,
variable text  ,
data varchar(4000)  ,
system varchar(64)  ,
finish_time timestamp(6),
business_id varchar(64) NOT NULL,
type varchar(32)
)


;

-- ----------------------------
-- Table structure for wind_process_config
-- ----------------------------
CREATE TABLE IF NOT EXISTS wind_process_config (
id varchar(64)   NOT NULL primary  key ,
process_id varchar(64)   NOT NULL,
config_name varchar(255)  ,
process_name varchar(255)   NOT NULL,
node_id varchar(64)   NOT NULL,
condition varchar(4000)  ,
node_config text  ,
approve_user text  ,
business_config varchar(4000)  ,
sort integer NOT NULL,
level integer NOT NULL,
create_time timestamp(6) NOT NULL,
logic varchar(32),
script text
)


;

-- ----------------------------
-- Table structure for wind_process_definition
-- ----------------------------
CREATE TABLE IF NOT EXISTS wind_process_definition (
id varchar(64)   NOT NULL primary  key ,
process_name varchar(255)   NOT NULL,
display_name varchar(255)  ,
status varchar(32)   NOT NULL,
version integer NOT NULL,
parent_id varchar(64)  ,
create_time timestamp(6) NOT NULL,
content longvarbinary NOT NULL,
system varchar(64),
module varchar(64)
)


;

-- ----------------------------
-- Table structure for wind_revoke_data
-- ----------------------------
CREATE TABLE IF NOT EXISTS wind_revoke_data (
id varchar(64)   NOT NULL primary  key ,
order_id varchar(64)   NOT NULL,
process_id varchar(64)   NOT NULL,
process_display_name varchar(255)  ,
task_name varchar(64)   NOT NULL,
task_display_name varchar(255)  ,
actor_id varchar(64)   NOT NULL,
operate varchar(32)   NOT NULL,
suggest varchar(1024)  ,
operate_time timestamp(6) NOT NULL,
wind_order text   NOT NULL,
wind_task text   NOT NULL,
wind_task_actor text   NOT NULL,
create_time timestamp(6) NOT NULL
)


;

-- ----------------------------
-- Table structure for wind_task
-- ----------------------------
CREATE TABLE IF NOT EXISTS wind_task (
id varchar(64)   NOT NULL primary  key ,
task_name varchar(64)   NOT NULL,
display_name varchar(255)  ,
task_type varchar(32)   NOT NULL,
create_time timestamp(6) NOT NULL,
expire_time timestamp(6),
approve_count integer NOT NULL,
status varchar(32)   NOT NULL,
order_id varchar(64)   NOT NULL,
process_id varchar(64)   NOT NULL,
variable text  ,
parent_id varchar(64)  ,
version integer NOT NULL,
position varchar(1024),
create_user varchar(255),
task_level varchar(32) NOT NULL
);

CREATE TABLE IF NOT EXISTS wind_process_module (
id varchar(64)   NOT NULL primary  key ,
module_name varchar(255) NOT NULL,
parent_id varchar(64),
system varchar(64)
)


;

-- ----------------------------
-- Table structure for wind_task_actor
-- ----------------------------
CREATE TABLE IF NOT EXISTS wind_task_actor (
task_id varchar(64)   NOT NULL,
actor_id varchar(64)   NOT NULL
)


;

CREATE INDEX IF NOT EXISTS wind_act_hist_approve_time ON wind_act_hist  (approve_time);
CREATE INDEX IF NOT EXISTS wind_act_hist_order_id ON wind_act_hist  (order_id);
CREATE INDEX IF NOT EXISTS wind_act_hist_process_id ON wind_act_hist  (process_id);
CREATE UNIQUE INDEX IF NOT EXISTS wind_act_hist_task_id ON wind_act_hist  (task_id);


CREATE INDEX IF NOT EXISTS wind_end_hist_approve_time_copy ON wind_end_hist  (approve_time);
CREATE INDEX IF NOT EXISTS wind_end_hist_order_id_copy ON wind_end_hist  (order_id);
CREATE INDEX IF NOT EXISTS wind_end_hist_process_id_copy ON wind_end_hist  (process_id);
CREATE UNIQUE INDEX IF NOT EXISTS wind_end_hist_task_id_copy ON wind_end_hist  (task_id);

CREATE INDEX IF NOT EXISTS wind_hist_order_create_time_copy ON wind_hist_order  (create_time);
CREATE INDEX IF NOT EXISTS wind_hist_order_create_user_copy ON wind_hist_order  (create_user);
CREATE INDEX IF NOT EXISTS wind_hist_order_business_id_copy ON wind_hist_order  (business_id);
CREATE UNIQUE INDEX IF NOT EXISTS wind_hist_order_id_copy ON wind_hist_order  (id);
CREATE INDEX IF NOT EXISTS wind_hist_order_process_id_copy ON wind_hist_order  (process_id);

CREATE INDEX IF NOT EXISTS wind_order_create_time ON wind_order  (create_time);
CREATE INDEX IF NOT EXISTS wind_order_create_user ON wind_order  (create_user);
CREATE INDEX IF NOT EXISTS wind_order_business_id ON wind_order  (business_id);
CREATE UNIQUE INDEX IF NOT EXISTS  wind_order_id ON wind_order  (id);
CREATE INDEX IF NOT EXISTS wind_order_process_id ON wind_order  (process_id);


CREATE INDEX IF NOT EXISTS config_create_time ON wind_process_config  (create_time);
CREATE UNIQUE INDEX IF NOT EXISTS config_id ON wind_process_config  (id);
CREATE INDEX IF NOT EXISTS config_process_id ON wind_process_config  (process_id);

CREATE INDEX IF NOT EXISTS process_create_time ON wind_process_definition  (create_time);
CREATE UNIQUE INDEX IF NOT EXISTS process_id ON wind_process_definition  (id);

CREATE INDEX IF NOT EXISTS wind_task_create_time ON wind_task  (create_time);
CREATE UNIQUE INDEX IF NOT EXISTS wind_task_id ON wind_task  (id);
CREATE INDEX IF NOT EXISTS wind_task_order_id ON wind_task  (order_id);
CREATE INDEX IF NOT EXISTS wind_task_process_id ON wind_task  (process_id);


CREATE INDEX IF NOT EXISTS wind_task_actor_task_id ON wind_task_actor  (task_id);
