package com.bcx.wind.workflow.db.util;

import com.bcx.wind.workflow.db.util.handler.ResultSetHandler;
import org.apache.ibatis.jdbc.SQL;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * 数据库持久层处理工具
 *
 * @author zhanglei
 */
public interface DbRunner {


    /**
     * 查询
     *
     * @param connection    数据库连接
     * @param sql            sql语句
     * @param handler       返回值处理拦截器
     * @param args           sql参数
     * @param <T>            返回类型
     * @throws SQLException   sql异常
     * @return                返回值
     */
    <T>T  query(Connection connection, String sql, ResultSetHandler handler,Object[] args) throws SQLException;



    /**
     * 更新
     *
     * @param connection   数据库连接
     * @param sql           sql语句
     * @param args          sql参数
     * @throws SQLException  sql异常
     * @return              更新结果值
     */
    int   saveOrUpdate(Connection connection,String sql,Object[] args) throws SQLException;


    /**
     * 更新
     *
     * @param connection     数据库连接
     * @param sql             sql语句
     * @return                执行结果
     * @throws SQLException   异常
     */
    int   saveOrUpdate(Connection connection,String sql) throws SQLException;
}
