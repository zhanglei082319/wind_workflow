package com.bcx.wind.workflow.exception;


/**
 * @author zhanglei
 */
public class WindException extends RuntimeException {

    private String msg;

    private int code;

    public WindException(String msg){
        super(msg);
        this.msg = msg;
        this.code = -1;
        this.printStackTrace();
    }

    public WindException(String msg,int code){
        this(msg);
        this.code = code;
        this.printStackTrace();
    }

    public WindException(Throwable e){
        super(e);
        this.msg = e.getMessage();
        this.code = -1;
        this.printStackTrace();
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
