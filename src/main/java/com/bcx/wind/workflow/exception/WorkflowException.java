package com.bcx.wind.workflow.exception;

/**
 * 底层异常
 *
 * @author zhanglei
 */
public class WorkflowException extends WindException {

    public WorkflowException(String msg) {
        super(msg);
    }
}
