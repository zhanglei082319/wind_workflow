package com.bcx.wind.workflow.interceptor;

import com.bcx.wind.workflow.pojo.Wind;

/**
 * 工作流自定义拦截器
 *
 * 在工作流自定义节点 拦截使用
 *
 * @author zhanglei
 */
public interface CustomInterceptor {


    /**
     * 执行
     *
     * @param wind   工作流执行实例
     * @param commandContext  工作流配置执行数据
     */
    void   execute(Wind wind,CommandContext commandContext);
}
