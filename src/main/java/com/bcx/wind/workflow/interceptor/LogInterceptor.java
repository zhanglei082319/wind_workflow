package com.bcx.wind.workflow.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 日志拦截器
 *
 * @author zhanglei
 */
public class LogInterceptor extends AbstractCommandInterceptor {

    private static Logger logger = LoggerFactory.getLogger(LogInterceptor.class);

    @Override
    public <T> T executor(Command<T> command) {
        if(!logger.isDebugEnabled()){
            return getNext().executor(command);
        }

        logger.debug("log start");
        logger.debug("workflow command "+command.getClass().getSimpleName()+" is start!");
        try{
            return getNext().executor(command);
        }finally {
            logger.debug("finish log");
            logger.debug("workflow command "+command.getClass().getSimpleName()+" is finish!");
        }
    }
}
