package com.bcx.wind.workflow.entity;

import com.bcx.wind.workflow.support.JsonHelper;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * 任务实例实体
 *
 * @author zhanglei
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WindTask implements Serializable {

    /**
     * 任务ID
     */
    private String id;

    /**
     * 任务名称 （节点名称）
     */
    private String taskName;

    /**
     * 任务显示名称 （节点显示名称）
     */
    private String displayName;

    /**
     * 任务类型  普通/会签/订阅
     */
    private String taskType;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     * 期望完成时间
     */
    private Timestamp expireTime;

    /**
     * 审批次数
     */
    private int approveCount;

    /**
     * 任务状态  运行/暂停
     */
    private String status;

    /**
     * 流程实例ID
     */
    private String orderId;

    /**
     * 流程实例ID
     */
    private String processId;

    /**
     * 任务变量
     */
    private String variable;

    /**
     * 父任务ID
     */
    private String parentId;

    /**
     * 版本 
     */
    private int version;

    /**
     * 任务审批地址
     */
    private String position;


    /**
     * 任务创建人
     */
    private String createUser;

    /**
     * 任务级别   main主任务（必须执行审批）, child 子任务（可不执行）由主任务衍生出来的任务，类似于订阅任务
     */
    private String taskLevel;

    public String getId() {
        return id;
    }

    public Object  gotVariable(String key){
        if(this.variable != null){
            Map<String,Object> args = JsonHelper.jsonToMap(this.variable);
            return args.get(key);
        }
        return null;
    }

    public WindTask  addVariable(String key,Object value){
        if(key == null){
            return this;
        }
        Map<String,Object> variable = variable();
        variable.put(key,value);
        this.setVariable(JsonHelper.toJson(variable));
        return this;
    }


    public WindTask  addVariable(Map<String,Object> args){
        if(args == null){
            return this;
        }
        Map<String,Object> variable = variable();
        variable.putAll(args);
        this.setVariable(JsonHelper.toJson(variable));
        return this;
    }

    public Map<String,Object> variable(){
        if(this.variable != null){
            return JsonHelper.jsonToMap(this.variable);
        }
        return new HashMap<>(4);
    }

    public WindTask setId(String id) {
        this.id = id;
        return this;
    }

    public String getTaskName() {
        return taskName;
    }

    public WindTask setTaskName(String taskName) {
        this.taskName = taskName;
        return this;
    }

    public String getDisplayName() {
        return displayName;
    }

    public WindTask setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public String getTaskType() {
        return taskType;
    }

    public WindTask setTaskType(String taskType) {
        this.taskType = taskType;
        return this;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public WindTask setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
        return this;
    }

    public Timestamp getExpireTime() {
        return expireTime;
    }

    public WindTask setExpireTime(Timestamp expireTime) {
        this.expireTime = expireTime;
        return this;
    }

    public int getApproveCount() {
        return approveCount;
    }

    public WindTask setApproveCount(int approveCount) {
        this.approveCount = approveCount;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public WindTask setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getOrderId() {
        return orderId;
    }

    public WindTask setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getProcessId() {
        return processId;
    }

    public WindTask setProcessId(String processId) {
        this.processId = processId;
        return this;
    }

    public String getVariable() {
        return variable;
    }

    public WindTask setVariable(String variable) {
        this.variable = variable;
        return this;
    }

    public String getParentId() {
        return parentId;
    }

    public WindTask setParentId(String parentId) {
        this.parentId = parentId;
        return this;
    }

    public int getVersion() {
        return version;
    }

    public WindTask setVersion(int version) {
        this.version = version;
        return this;
    }

    public String getPosition() {
        return position;
    }

    public WindTask setPosition(String position) {
        this.position = position;
        return this;
    }

    public String getCreateUser() {
        return createUser;
    }

    public WindTask setCreateUser(String createUser) {
        this.createUser = createUser;
        return this;
    }

    public String getTaskLevel() {
        return taskLevel;
    }

    public WindTask setTaskLevel(String taskLevel) {
        this.taskLevel = taskLevel;
        return this;
    }


    public WindTask clone()  {
        try {
            return (WindTask) super.clone();
        }catch (Exception e){
            return new WindTask()
                    .setCreateUser(this.createUser)
                    .setTaskLevel(this.taskLevel)
                    .setParentId(this.parentId)
                    .setId(this.id)
                    .setVersion(this.version)
                    .setTaskType(this.taskType)
                    .setTaskName(this.taskName)
                    .setProcessId(this.processId)
                    .setPosition(this.position)
                    .setOrderId(this.orderId)
                    .setStatus(this.status)
                    .setExpireTime(this.expireTime)
                    .setDisplayName(this.displayName)
                    .setVariable(this.variable)
                    .setApproveCount(this.approveCount)
                    .setCreateTime(this.createTime)
                    .setTaskLevel(this.taskLevel);
        }
    }
}
