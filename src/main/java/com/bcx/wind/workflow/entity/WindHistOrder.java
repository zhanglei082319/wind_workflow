package com.bcx.wind.workflow.entity;

import java.sql.Timestamp;

/**
 * 流程实例历史实体
 * 
 * @author zhanglei
 */
public class WindHistOrder {

    /**
     * 流程实例ID
     */
    private String id;

    /**
     * 流程定义ID
     */
    private String processId;

    /**
     * 流程状态   暂停，运行
     */
    private String status;

    /**
     * 流程创建人
     */
    private String createUser;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     * 期望完成时间
     */
    private Timestamp expireTime;

    /**
     * 父流程ID
     */
    private String parentId;

    /**
     * 版本
     */
    private int version;

    /**
     * 流程变量
     */
    private String variable;

    /**
     * 流程信息消息
     */
    private String data;

    /**
     * 所属模块
     */
    private String system;

    /**
     * 流程结束时间
     */
    private Timestamp finishTime;

    /**
     * 业务ID
     */
    private String  businessId;


    /**
     * 流程实例类型
     */
    private String  type;

    public String getId() {
        return id;
    }

    public WindHistOrder setId(String id) {
        this.id = id;
        return this;
    }

    public String getProcessId() {
        return processId;
    }

    public WindHistOrder setProcessId(String processId) {
        this.processId = processId;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public WindHistOrder setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getCreateUser() {
        return createUser;
    }

    public WindHistOrder setCreateUser(String createUser) {
        this.createUser = createUser;
        return this;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public WindHistOrder setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
        return this;
    }

    public Timestamp getExpireTime() {
        return expireTime;
    }

    public WindHistOrder setExpireTime(Timestamp expireTime) {
        this.expireTime = expireTime;
        return this;
    }

    public String getParentId() {
        return parentId;
    }

    public WindHistOrder setParentId(String parentId) {
        this.parentId = parentId;
        return this;
    }

    public int getVersion() {
        return version;
    }

    public WindHistOrder setVersion(int version) {
        this.version = version;
        return this;
    }

    public String getVariable() {
        return variable;
    }

    public WindHistOrder setVariable(String variable) {
        this.variable = variable;
        return this;
    }

    public String getData() {
        return data;
    }

    public WindHistOrder setData(String data) {
        this.data = data;
        return this;
    }

    public String getSystem() {
        return system;
    }

    public WindHistOrder setSystem(String system) {
        this.system = system;
        return this;
    }

    public Timestamp getFinishTime() {
        return finishTime;
    }

    public WindHistOrder setFinishTime(Timestamp finishTime) {
        this.finishTime = finishTime;
        return this;
    }

    public String getBusinessId() {
        return businessId;
    }

    public WindHistOrder setBusinessId(String businessId) {
        this.businessId = businessId;
        return this;
    }

    public String getType() {
        return type;
    }

    public WindHistOrder setType(String type) {
        this.type = type;
        return this;
    }
}
