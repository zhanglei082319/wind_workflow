package com.bcx.wind.workflow.entity;

import com.bcx.wind.workflow.core.flow.node.ProcessModel;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;

import static com.bcx.wind.workflow.message.ErrorCode.XML_ERROR;

/**
 * 流程过程定义实体
 *
 * @author zhanglei
 */
public class WindProcess {

    /**
     * 流程定义ID
     */
    private String id;

    /**
     * 流程定义名称
     */
    private String processName;

    /**
     * 显示名称
     */
    private String displayName;

    /**
     * 状态   发布/回收
     */
    private String status;

    /**
     * 版本   每更新一次，版本+1
     */
    private int version;

    /**
     * 父流程定义ID
     */
    private String parentId;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     * 流程定义内容
     */
    private byte[] content;

    /**
     * 所属系统
     */
    private String system;

    /**
     * 所属模块
     */
    private String module;

    /**
     * 模型内容字符串
     */
    private String contentStr;

    /**
     * 流程定义对象
     */
    @JsonIgnore
    private ProcessModel processModel;

    /**
     * 流程实例数量  用于查询流程实例时使用
     */
    @JsonIgnore
    private long taskNumber;

    public String getId() {
        return id;
    }

    public WindProcess setId(String id) {
        this.id = id;
        return this;
    }

    public String getProcessName() {
        return processName;
    }

    public WindProcess setProcessName(String processName) {
        this.processName = processName;
        return this;
    }

    public String getDisplayName() {
        return displayName;
    }

    public WindProcess setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public WindProcess setStatus(String status) {
        this.status = status;
        return this;
    }

    public int getVersion() {
        return version;
    }

    public WindProcess setVersion(int version) {
        this.version = version;
        return this;
    }

    public String getParentId() {
        return parentId;
    }

    public WindProcess setParentId(String parentId) {
        this.parentId = parentId;
        return this;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public WindProcess setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
        return this;
    }

    public byte[] getContent() {
        return content;
    }

    public WindProcess setContent(byte[] content) {
        this.content = content;
        return this;
    }

    public String getSystem() {
        return system;
    }

    public WindProcess setSystem(String system) {
        this.system = system;
        return this;
    }

    public String getModule() {
        return module;
    }

    public WindProcess setModule(String module) {
        this.module = module;
        return this;
    }

    public void processModel(ProcessModel model){
        this.processModel = model;
    }

    public ProcessModel processModel(){
        return this.processModel;
    }

    public ProcessModel getProcessModel() {
        return processModel;
    }

    public void setProcessModel(ProcessModel processModel) {
        this.processModel = processModel;
    }

    public long getTaskNumber() {
        return taskNumber;
    }

    public void setTaskNumber(long taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getContentStr() {
        if(this.contentStr != null){
            return this.contentStr;
        }
        try {
            this.contentStr = new String(this.content,"utf-8");
        } catch (UnsupportedEncodingException e) {
            WindError.error(XML_ERROR,e);
        }
        return contentStr;
    }

    public WindProcess setContentStr(String contentStr) {
        this.contentStr = contentStr;
        return this;
    }
}
