package com.bcx.wind.workflow.entity;

/**
 * 任务审批人实体
 *
 * @author zhanglei
 */
public class WindTaskActor {

    /**
     * 任务ID
     */
    private String taskId;

    /**
     * 审批人ID
     */
    private String actorId;

    public String getTaskId() {
        return taskId;
    }

    public WindTaskActor setTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getActorId() {
        return actorId;
    }

    public WindTaskActor setActorId(String actorId) {
        this.actorId = actorId;
        return this;
    }

}
