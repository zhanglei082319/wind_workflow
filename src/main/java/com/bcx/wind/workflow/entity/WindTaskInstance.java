package com.bcx.wind.workflow.entity;

import java.sql.Timestamp;

/**
 * 工作流任务实例实体
 *
 * @author zhanglei
 */
public class WindTaskInstance {

    /**
     * 流程定义ID
     */
    private String processId;

    /**
     * 流程定义名称
     */
    private String processName;

    /**
     * 流程定义显示清除
     */
    private String processDisplayName;

    /**
     * 流程实例ID
     */
    private String orderId;


    /**
     * 业务ID
     */
    private String businessId;

    /**
     * 流程实例创建人
     */
    private String orderCreateUser;

    /**
     * 流程实例请求内容
     */
    private String  orderData;

    /**
     * 流程实例创建时间
     */
    private Timestamp orderCreateTime;

    /**
     * 流程实例希望完成时间
     */
    private String orderExpireTime;

    /**
     * 流程实例变量
     */
    private String orderVariable;

    /**
     * 任务ID
     */
    private String taskId;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 任务显示名称
     */
    private String taskDisplayName;

    /**
     * 任务创建时间
     */
    private Timestamp taskCreateTime;

    /**
     * 任务地址
     */
    private String taskPosition;

    /**
     * 任务类型
     */
    private String taskType;

    /**
     * 任务创建人
     */
    private String taskCreateUser;

    /**
     * 任务期望完成时间
     */
    private String taskExpireTime;

    /**
     * 任务变量
     */
    private String taskVariable;

    /**
     * 任务审批人
     */
    private String taskActor;


    public String getProcessId() {
        return processId;
    }

    public WindTaskInstance setProcessId(String processId) {
        this.processId = processId;
        return this;
    }

    public String getProcessName() {
        return processName;
    }

    public WindTaskInstance setProcessName(String processName) {
        this.processName = processName;
        return this;
    }

    public String getProcessDisplayName() {
        return processDisplayName;
    }

    public WindTaskInstance setProcessDisplayName(String processDisplayName) {
        this.processDisplayName = processDisplayName;
        return this;
    }

    public String getOrderId() {
        return orderId;
    }

    public WindTaskInstance setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getOrderCreateUser() {
        return orderCreateUser;
    }

    public WindTaskInstance setOrderCreateUser(String orderCreateUser) {
        this.orderCreateUser = orderCreateUser;
        return this;
    }

    public String getOrderData() {
        return orderData;
    }

    public WindTaskInstance setOrderData(String orderData) {
        this.orderData = orderData;
        return this;
    }

    public Timestamp getOrderCreateTime() {
        return orderCreateTime;
    }

    public WindTaskInstance setOrderCreateTime(Timestamp orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
        return this;
    }

    public String getOrderExpireTime() {
        return orderExpireTime;
    }

    public WindTaskInstance setOrderExpireTime(String orderExpireTime) {
        this.orderExpireTime = orderExpireTime;
        return this;
    }

    public String getOrderVariable() {
        return orderVariable;
    }

    public WindTaskInstance setOrderVariable(String orderVariable) {
        this.orderVariable = orderVariable;
        return this;
    }

    public String getTaskId() {
        return taskId;
    }

    public WindTaskInstance setTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getTaskName() {
        return taskName;
    }

    public WindTaskInstance setTaskName(String taskName) {
        this.taskName = taskName;
        return this;
    }

    public String getTaskDisplayName() {
        return taskDisplayName;
    }

    public WindTaskInstance setTaskDisplayName(String taskDisplayName) {
        this.taskDisplayName = taskDisplayName;
        return this;
    }

    public Timestamp getTaskCreateTime() {
        return taskCreateTime;
    }

    public WindTaskInstance setTaskCreateTime(Timestamp taskCreateTime) {
        this.taskCreateTime = taskCreateTime;
        return this;
    }

    public String getTaskPosition() {
        return taskPosition;
    }

    public WindTaskInstance setTaskPosition(String taskPosition) {
        this.taskPosition = taskPosition;
        return this;
    }

    public String getTaskType() {
        return taskType;
    }

    public WindTaskInstance setTaskType(String taskType) {
        this.taskType = taskType;
        return this;
    }

    public String getTaskCreateUser() {
        return taskCreateUser;
    }

    public WindTaskInstance setTaskCreateUser(String taskCreateUser) {
        this.taskCreateUser = taskCreateUser;
        return this;
    }

    public String getTaskExpireTime() {
        return taskExpireTime;
    }

    public WindTaskInstance setTaskExpireTime(String taskExpireTime) {
        this.taskExpireTime = taskExpireTime;
        return this;
    }

    public String getTaskVariable() {
        return taskVariable;
    }

    public WindTaskInstance setTaskVariable(String taskVariable) {
        this.taskVariable = taskVariable;
        return this;
    }

    public String getTaskActor() {
        return taskActor;
    }

    public WindTaskInstance setTaskActor(String taskActor) {
        this.taskActor = taskActor;
        return this;
    }

    public String getBusinessId() {
        return businessId;
    }

    public WindTaskInstance setBusinessId(String businessId) {
        this.businessId = businessId;
        return this;
    }
}
