package com.bcx.wind.workflow.cache.imp;

import com.bcx.wind.workflow.cache.Cache;

/**
 * 定时情况缓存数据  并没有开启定时器来实现，而是在操作缓存数据时判断时间来实现。也称手动清除
 *
 * @author zhanglei
 */
public class ScheduleCache implements Cache {

    /**
     * 实际缓存
     */
    private Cache cache;

    /**
     * 定时情况缓存的时间
     */
    private  long  clearTime;

    /**
     * 上一次更新时间
     */
    private  long  lastUpdateTime;

    public ScheduleCache(Cache cache){
        this.cache = cache;
        //默认一小时清空一次
        this.clearTime = 1000*60*60;
        this.lastUpdateTime = System.currentTimeMillis();
    }

    public ScheduleCache(Cache cache,long clearTime){
        this.cache = cache;
        this.clearTime = clearTime;
        this.lastUpdateTime = System.currentTimeMillis();
    }

    private synchronized void clearCache(){
        long now = System.currentTimeMillis();
        if(now - this.lastUpdateTime > this.clearTime){
            this.lastUpdateTime = now;
            this.clear();
        }
    }

    @Override
    public Object put(Object key, Object value) {
        clearCache();
        return this.cache.put(key,value);
    }

    @Override
    public Object get(Object key) {
        clearCache();
        return this.cache.get(key);
    }

    @Override
    public boolean contain(Object key) {
        clearCache();
        return this.cache.contain(key);
    }

    @Override
    public int size() {
        clearCache();
        return this.cache.size();
    }

    @Override
    public Object removeKey(Object key) {
        clearCache();
        return this.cache.removeKey(key);
    }

    @Override
    public void clear() {
        this.cache.clear();
    }
}
