package com.bcx.wind.workflow.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * 默认缓存实现 基于HashMap
 *
 * @author zhanglei
 */
public class WindCache implements Cache {

    /**
     * 缓存数据中心容器
     */
    private Map<Object,Object> data = new HashMap<>();

    @Override
    public Object put(Object key, Object value) {
        return data.put(key,value);
    }

    @Override
    public Object get(Object key) {
        return data.get(key);
    }

    @Override
    public boolean contain(Object key) {
        return data.containsKey(key);
    }

    @Override
    public int size() {
        return data.size();
    }

    @Override
    public Object removeKey(Object key) {
        return data.remove(key);
    }

    @Override
    public void clear() {
        data.clear();
    }
}
