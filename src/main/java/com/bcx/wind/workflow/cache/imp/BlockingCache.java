package com.bcx.wind.workflow.cache.imp;

import com.bcx.wind.workflow.cache.Cache;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 阻塞实现缓存，相同的键操作给与阻塞，不同的键操作不阻塞
 *
 * @author zhanglei
 */
public class BlockingCache implements Cache {

    /**
     * 实际缓存实现
     */
    private Cache cache;

    /**
     * 键和显示锁的结合
     */
    private ConcurrentHashMap<Object,ReentrantLock> locks = new ConcurrentHashMap<>();

    public BlockingCache(Cache cache){
        this.cache = cache;
    }

    @Override
    public Object put(Object key, Object value) {
        ReentrantLock lock = getLock(key);
        try{
            lock.lock();
            return this.cache.put(key,value);
        }finally {
            unlock(key);
        }
    }

    @Override
    public Object get(Object key) {
        ReentrantLock lock = getLock(key);
        try{
            lock.lock();
            return this.cache.get(key);
        }finally {
            unlock(key);
        }
    }

    @Override
    public boolean contain(Object key) {
        ReentrantLock lock = getLock(key);
        try{
            lock.lock();
            return this.cache.contain(key);
        }finally {
            unlock(key);
        }
    }


    @Override
    public int size() {
        return this.cache.size();
    }

    @Override
    public Object removeKey(Object key) {
        ReentrantLock lock = getLock(key);
        try{
            lock.lock();
            return this.cache.removeKey(key);
        }finally {
            unlock(key);
        }
    }

    @Override
    public void clear() {
        this.cache.clear();
    }


    /**
     * 保证同一个键获取到的锁是同一把锁，不同的键获取到的锁是不同的锁
     *
     * @param key 键
     * @return    锁 ReentrantLock
     */
    private ReentrantLock getLock(Object key){
        ReentrantLock lock = new ReentrantLock();
        ReentrantLock preLock = this.locks.putIfAbsent(key,lock);
        return preLock == null ? lock : preLock;
    }


    /**
     * 通过键，是否对应的释放锁
     *
     * @param key  键
     */
    private void unlock(Object key){
        ReentrantLock lock = this.locks.get(key);
        if(lock != null && lock.isHeldByCurrentThread()){
            lock.unlock();
        }
    }
}
