package com.bcx.wind.workflow.cache;

/**
 * @author zhanglei
 */
public interface Cache {

    /**
     * 添加缓存信息
     *
     * @param key     键
     * @param value   值
     * @return        值
     */
    Object  put(Object key,Object value);


    /**
     * 通过键查询值
     *
     * @param key   键
     * @return      值
     */
    Object  get(Object key);


    /**
     * 判断是否包含键
     *
     * @param key  键
     * @return     boolean
     */
    boolean  contain(Object key);


    /**
     * 返回缓存大小
     *
     * @return   int
     */
    int  size();


    /**
     * 通过键 删除缓存
     *
     * @param key  键
     * @return     值
     */
    Object   removeKey(Object key);


    /**
     * 清除缓存
     */
    void clear();


}
