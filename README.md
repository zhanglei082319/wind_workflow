# wind_workflow

## 介绍
轻量级工作流引擎，封装审批流各个操作，简化业务开发流程

## 软件架构
###技术
jdk1.8  maven管理项目

###功能
提供基于工作流审批的创建，提交，退回，完结，撤销，撤回，转办，加签，减签，关闭任务，暂停流程，
恢复流程，自由提交，自由退回等。

流程组件： 开始 、结束、普通任务、会签任务、串签任务、订阅任务、决策路由、并且分支、或者分支、动态分支、
自定义节点、子流程等

支持多数据库： mysql、postgresql 、sqlServer 、oracle 等

无需编写持久层

支持mybatis spring jdbc


#### 安装教程

wind_workflow是一个辅助业务系统的流程引擎组件，需要依赖业务系统。
1. 将项目打包做为业务项目依赖
2. WindEngine engine = WindEngineBuilder.getInstance().buildDataSource(dataSource)
                   .buildDbType(WindDB.SPRING)
                   .buildEngine();

#### 使用说明

1. WindEngine engine = WindEngineBuilder.getInstance().buildDataSource(dataSource)
                      .buildDbType(WindDB.SPRING)
                      .buildEngine();

   核心服务  engine.windCoreService();
   资源配置服务 engine.repositoryService();
   历史服务 engine.historyService();
   管理服务  engine.managerService();
   运行时服务  engine.runtimeService();
   获取全局配置  engine.getConfiguration();

#### 参与贡献
   发起人、起草人

#### 特殊说明
本项目尚未完成，望有意人士积极参与


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)